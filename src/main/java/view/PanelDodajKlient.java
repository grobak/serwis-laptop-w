package view;

import components.ButtonsAdd;
import controllers.KlientFormValidator;
import interfaces.BackgroundTask;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import repositories.CustomerRepository;
import view.forms.KlientForm;

public class PanelDodajKlient extends JPanel implements BackgroundTask {

    private ButtonsAdd buttonPanel = new ButtonsAdd();
    private KlientForm form = new KlientForm();
    private KlientFormValidator validator;

    public PanelDodajKlient() {
        setView();

        buttonPanel.getBtnCofnij().addActionListener(e -> form.clear());
        buttonPanel.getBtnZapisz().addActionListener(e -> {
            validator = new KlientFormValidator(form);
            if (validator.formIsValid() && validator.nameIsAvailable()) {
                backgroundTask(() -> new CustomerRepository().add(form.generate()), form::clear);
            } else {
                JOptionPane.showMessageDialog(null, validator.getErrorMessage(), "Błąd", JOptionPane.ERROR_MESSAGE);
            }
        });

        checkFieldsRequirements();
        form.clear();

        form.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER && buttonPanel.getBtnZapisz().isEnabled()) {
                    buttonPanel.getBtnZapisz().doClick();
                }
            }
        });
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{600, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_addButtonPanel = new GridBagConstraints();
        gbc_addButtonPanel.insets = new Insets(0, 0, 5, 0);
        gbc_addButtonPanel.anchor = GridBagConstraints.NORTH;
        gbc_addButtonPanel.fill = GridBagConstraints.HORIZONTAL;
        gbc_addButtonPanel.gridx = 0;
        gbc_addButtonPanel.gridy = 0;
        add(buttonPanel, gbc_addButtonPanel);

        GridBagConstraints gbc_addKlientForm = new GridBagConstraints();
        gbc_addKlientForm.anchor = GridBagConstraints.NORTHWEST;
        gbc_addKlientForm.fill = GridBagConstraints.BOTH;
        gbc_addKlientForm.gridx = 0;
        gbc_addKlientForm.gridy = 1;
        add(form, gbc_addKlientForm);

        buttonPanel.getBtnZapisz().setEnabled(false);
    }

    private void checkFieldsRequirements() {
        DocumentListener pozwolenieNaZapis = new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                unlockAddButton();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                unlockAddButton();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                unlockAddButton();
            }
        };

        form.getTextNazwaKlienta().getDocument().addDocumentListener(pozwolenieNaZapis);
        form.getTextTelefon().getDocument().addDocumentListener(pozwolenieNaZapis);
    }

    public void unlockAddButton() {
        buttonPanel.getBtnZapisz().setEnabled(new KlientFormValidator(form).requiredFieldsAreValid());
    }
}

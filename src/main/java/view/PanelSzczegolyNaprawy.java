package view;

import components.ButtonPanel;
import dataproviders.LoggedUser;
import dataproviders.SendMail;
import dialogs.DialogWyszukajKlienta;
import dialogs.print.DialogDrukuj;
import dialogs.tests.TestsDialog;
import interfaces.BackgroundTask;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.mail.MessagingException;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import listeners.PanelChooser;
import models.Computer;
import models.Customer;
import models.RepairStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repositories.ComputerRepository;
import repositories.CustomerRepository;
import view.forms.ComputerForm;

public class PanelSzczegolyNaprawy extends JPanel implements BackgroundTask, PanelChooser {

    private Computer computer;
    private Computer cachedComputer;
    private Customer klient;

    private static final Logger LOG = LogManager.getLogger("src");

    private final ButtonPanel buttonPanel;
    private final ComputerForm computerForm;
    private final JPanel previousPanel;

    public PanelSzczegolyNaprawy(Computer sprze, JPanel previousPanel) {
        this.previousPanel = previousPanel;
        this.buttonPanel = new ButtonPanel();
        ComputerRepository computerRepository = new ComputerRepository();
        this.computer = computerRepository.getComputerByID(sprze.getId());
        try {
            cachedComputer = (Computer) computer.clone();
        } catch (CloneNotSupportedException e1) {
            LOG.error(e1, e1);
        }
        klient = new CustomerRepository().getById(computer.getIdKlienta());

        this.computerForm = new ComputerForm(computer);
        this.computerForm.setNumberOfCompletedTests(computerRepository.numberOfCompletedTests(computer));

        this.computerForm.getTextWlasciciel().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    showKlientDetails();
                }
            }
        });

        this.computerForm.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttonPanel.getBtnZapisz().doClick();
                }
            }
        });

        setView();
        ustawPola();

        if (LoggedUser.isAdmin()) {
            buttonPanel.getBtnUsu().setEnabled(true);
        }

        addModifyButtonsListeners();

        computerForm.getBtnWybierzKlienta().addActionListener(e -> {
            DialogWyszukajKlienta dialog = new DialogWyszukajKlienta();
            dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
            klient = dialog.closeDialog();
            if (klient != null) {
                computerForm.getTextWlasciciel().setText(klient.getName());
                computerForm.getSprzet().setIdKlienta(klient.getId());
            }
        });

        computerForm.getTextModel().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                enableSaveButton();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                enableSaveButton();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                enableSaveButton();
            }
        });
        addStatusButtonsListeners();

        computerForm.getTextBrak().addKeyListener(enter());
        computerForm.getTextDataPrzyjecia().addKeyListener(enter());
        computerForm.getTextModel().addKeyListener(enter());
        computerForm.getTextOpisNaprawy().addKeyListener(enter());
        computerForm.getTextOpisUsterki().addKeyListener(enter());
        computerForm.getTextRMA().addKeyListener(enter());
        computerForm.getTextUwagi().addKeyListener(enter());
        computerForm.getTextWypDod().addKeyListener(enter());
        computerForm.getRodzajNaprawy().getTxtRodzaj().addKeyListener(enter());
    }

    private KeyAdapter enter() {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttonPanel.getBtnZapisz().doClick();
                }
            }
        };
    }

    private void showKlientDetails() {
        setPanel("Szczegóły klienta", new PanelSzczegolyKlient(klient, this));
    }

    private void addModifyButtonsListeners() {
        buttonPanel.getBtnWr().addActionListener(e -> {
            setVisible(false);
            setPanel("Wyszukaj sprzęt", previousPanel);
        });

        buttonPanel.getBtnDrukuj().addActionListener(e -> {
            DialogDrukuj drukuj = new DialogDrukuj(computerForm.generateSprzet(), klient);
            drukuj.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            drukuj.setVisible(true);
        });

        buttonPanel.getBtnUsu().addActionListener(e -> {
            Object options[] = {"Tak", "Nie"};
            int reply = JOptionPane.showOptionDialog(null, "Czy na pewno chcesz usunąć sprzęt?", "Usuń sprzęt",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (reply == JOptionPane.YES_OPTION) {
                backgroundTask(() -> new ComputerRepository().delete(computer.getId()),
                        () -> buttonPanel.getBtnWr().doClick());
            }
        });

        buttonPanel.getBtnCofnij().addActionListener(e -> {
            Object options[] = {"Tak", "Nie"};
            int reply = JOptionPane.showOptionDialog(null, "Czy na pewno chcesz cofnąć zmiany?", "Cofnij zmiany",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (reply == JOptionPane.YES_OPTION) {
                ustawPola();
            }
        });

        buttonPanel.getBtnZapisz().addActionListener(e -> {

            if (sprzetWasUpdated()) {
                if (confirmUpdate() == JOptionPane.YES_OPTION) {
                    saveSprzetToDB();
                }
            } else {
                saveSprzetToDB();
            }
        });
    }

    private void addStatusButtonsListeners() {
        computerForm.getBtnDemonta().addActionListener(e -> changeStatus(RepairStatus.DEMONTAZ));

        computerForm.getBtnNaprawa().addActionListener(e -> changeStatus(RepairStatus.W_NAPRAWIE));

        computerForm.getBtnDoTestw().addActionListener(e -> changeStatus(RepairStatus.DO_TESTOW));

        computerForm.getBtnZakonczenieNaprawy().addActionListener(e -> changeStatus(RepairStatus.NAPRAWIONY));

        computerForm.getBtnWydanieSprztu().addActionListener(e -> changeStatus(RepairStatus.WYDANY));

        computerForm.getBtnTesty().addActionListener(e -> {
            TestsDialog dialog = new TestsDialog(computer);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
            computerForm.getKomponentStatus().refresh();
            computerForm.setNumberOfCompletedTests(new ComputerRepository().numberOfCompletedTests(computer));
        });
    }

    private void enableSaveButton() {
        buttonPanel.getBtnZapisz().setEnabled(computerForm.areRequiredFieldsCorrect());
    }

    public void ustawPola() {
        computerForm.getPanelWyposazenie().setWyposazenie(cachedComputer.getEquipment());
        computerForm.getTextWlasciciel().setText(cachedComputer.getOwner());
        computerForm.getTextModel().setText(cachedComputer.getModel());
        computerForm.getTextRMA().setText(cachedComputer.getRMA());
        computerForm.getTextDataPrzyjecia().setText(cachedComputer.getDataPrzyjecia().substring(0, 16));
        computerForm.getTextBrak().setText(cachedComputer.getEquipment().getBrak());
        computerForm.getTextWypDod().setText(cachedComputer.getEquipment().getWypDodatkowe());
        computerForm.getTextBrak().setText(cachedComputer.getEquipment().getBrak());
        computerForm.getTextWypDod().setText(cachedComputer.getEquipment().getWypDodatkowe());
        computerForm.getTextOpisUsterki().setText(cachedComputer.getOpisUsterki());
        computerForm.getTextOpisNaprawy().setText(cachedComputer.getOpisNaprawy());
        computerForm.getTextUwagi().setText(cachedComputer.getUwagi());
        String rodzaj = cachedComputer.getRodzajNaprawy();
        switch (rodzaj) {
            case "Naprawa":
                computerForm.getRodzajNaprawy().setRodzajNaprawy(0);
                break;
            case "Wymiana płyty":
                computerForm.getRodzajNaprawy().setRodzajNaprawy(1);
                break;
            case "Konserwacja":
                computerForm.getRodzajNaprawy().setRodzajNaprawy(2);
                break;
            case "Reklamacja":
                computerForm.getRodzajNaprawy().setRodzajNaprawy(3);
                break;
            default:
                computerForm.getRodzajNaprawy().setRodzajNaprawy(rodzaj);
                break;
        }
    }

    private void changeStatus(RepairStatus status) {
        if (sprzetWasUpdated()) {
            if (confirmUpdate() == JOptionPane.YES_OPTION) {
                setStatus(status);
            }
        } else {
            setStatus(status);
        }
    }

    private boolean sprzetWasUpdated() {
        Computer sprzetUpdate = new ComputerRepository().getComputerByID(computer.getId());
        return !computer.equals(sprzetUpdate);
    }

    private int confirmUpdate() {
        Object options[] = {"Tak", "Nie"};
        return JOptionPane.showOptionDialog(null, "Ktoś wprowadził zmiany w danych sprzętu. Czy chcesz je nadpisać?",
                "Nieaktualne dane", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
                options[1]);

    }

    private void setStatus(RepairStatus status) {
        backgroundTask(() -> {
            new ComputerRepository().setStatus(computer, status);
            computerForm.getKomponentStatus().refresh();
            saveSprzetToDB();
            if (status == RepairStatus.NAPRAWIONY && !klient.getEmail().isEmpty()
                    && confirmMail() == JOptionPane.YES_OPTION) {
                try {
                    new SendMail().sendRepairConfirmation(klient.getEmail(), computer.getRMA());
                } catch (MessagingException e) {
                    LOG.error(e, e);
                }

            }
            if (status == RepairStatus.WYDANY) {
                buttonPanel.getBtnDrukuj().doClick();
            }
        });
    }

    private int confirmMail() {
        Object options[] = {"Tak", "Nie"};
        return JOptionPane.showOptionDialog(null,
                "Klient posiada adres e-mail. Czy wysłać informację o zakończeniu naprawy?", "Powiadomienie klienta",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
    }

    private void saveSprzetToDB() {
        new ComputerRepository().update(computerForm.generateSprzet());
        computer = new ComputerRepository().getComputerByID(computer.getId());
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        setPreferredSize(new Dimension(600, 445));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 30, 60, 0, 0, 0, 0, 90, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 14, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_buttonsAdd = new GridBagConstraints();
        gbc_buttonsAdd.gridwidth = 7;
        gbc_buttonsAdd.insets = new Insets(0, 0, 5, 5);
        gbc_buttonsAdd.fill = GridBagConstraints.BOTH;
        gbc_buttonsAdd.gridx = 1;
        gbc_buttonsAdd.gridy = 0;
        add(buttonPanel, gbc_buttonsAdd);

        computerForm.getTextWlasciciel().setEditable(false);

        GridBagConstraints gbc_sprzetForm = new GridBagConstraints();
        gbc_sprzetForm.gridwidth = 7;
        gbc_sprzetForm.insets = new Insets(0, 0, 0, 5);
        gbc_sprzetForm.fill = GridBagConstraints.BOTH;
        gbc_sprzetForm.gridx = 1;
        gbc_sprzetForm.gridy = 1;
        add(computerForm, gbc_sprzetForm);

        buttonPanel.getBtnUsu().setEnabled(false);
    }

}

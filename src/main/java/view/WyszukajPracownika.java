package view;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import listeners.PanelChooser;
import models.Employee;
import repositories.EmployeeRepository;
import view.forms.AbstractPanelWyszukaj;

public class WyszukajPracownika extends AbstractPanelWyszukaj implements PanelChooser
{

	private List<Employee> pracownicy;

	public WyszukajPracownika()
	{
		super();
		columnNames = new Object[] { "Nazwa użytkownika", "Imię i nazwisko", "Stanowisko", "Telefon" };
		backgroundTask( () -> searchResultTable.setModel( generateTableModel() ) );
	}

	@Override
	public DefaultTableModel generateTableModel()
	{
		pracownicy = new EmployeeRepository().findByLoginOrName( topPanel.getTxtWyszukaj().getText() );

		DefaultTableModel model = new DefaultTableModel()
		{
			@Override
			public boolean isCellEditable( int row, int column )
			{
				return false;
			}
		};
		model.setColumnIdentifiers( columnNames );

		for( Employee aPracownicy : pracownicy )
		{
			model.insertRow( 0,
					new Object[] { aPracownicy.getLogin(), aPracownicy.getImie(),
							new EmployeeRepository().getPositionName( aPracownicy.getPositionId() ),
							aPracownicy.getTelefon() } );
		}

		return model;
	}

	@Override
	public void doubleClickAction()
	{
		Employee pracownik = pracownicy.get( pracownicy.size() - lastClickedRow - 1 );
		setPanel( "Szczegóły pracownika", new PanelSzczegolyPracownik( pracownik ) );
	}

}

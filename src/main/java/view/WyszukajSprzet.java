package view;

import java.util.List;
import javax.swing.table.DefaultTableModel;
import listeners.PanelChooser;
import models.Computer;
import repositories.ComputerRepository;
import view.forms.AbstractPanelWyszukaj;

public class WyszukajSprzet extends AbstractPanelWyszukaj implements PanelChooser {

    private List<Computer> sprzety;

    public WyszukajSprzet() {
        super();
        columnNames = new Object[]{
            COLUMNS.OWNER.getName(),
            COLUMNS.RMA.getName(),
            COLUMNS.MODEL.getName(),
            COLUMNS.DATE_ADDED.getName(),
            COLUMNS.LAST_STATUS.getName()
        };
        topPanel.getBtnWyszukaj().setEnabled(false);
        setUpTable();
    }

    @Override
    public DefaultTableModel generateTableModel() {
        sprzety = new ComputerRepository().find(topPanel.getTxtWyszukaj().getText());

        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        model.setColumnIdentifiers(columnNames);

        sprzety.forEach(
                computer -> model.insertRow(0, new Object[]{computer.getOwner(), computer.getRMA(), computer.getModel(), computer.getDataPrzyjecia(), computer.getStatusData()}));

        return model;
    }

    @Override
    public void doubleClickAction() {
        Computer sprzet = sprzety.get(sprzety.size() - lastClickedRow - 1);
        setPanel("Szczegóły sprzętu", new PanelSzczegolyNaprawy(sprzet, this));
    }

    @Override
    public void setUpTable() {
        backgroundTask(() -> {
            searchResultTable.setModel(generateTableModel());
            searchResultTable.getColumnModel().getColumn(COLUMNS.OWNER.getIndex()).setMinWidth(140);
            searchResultTable.getColumnModel().getColumn(COLUMNS.RMA.getIndex()).setMaxWidth(60);
            searchResultTable.getColumnModel().getColumn(COLUMNS.DATE_ADDED.getIndex()).setMaxWidth(100);
            searchResultTable.getColumnModel().getColumn(COLUMNS.DATE_ADDED.getIndex()).setMinWidth(100);
        }, () -> topPanel.getBtnWyszukaj().setEnabled(true));
    }

    private enum COLUMNS {

        OWNER(0, "Właściciel"), RMA(1, "RMA"), MODEL(2, "Model"),
        DATE_ADDED(3, "Data przyjęcia"), LAST_STATUS(4, "Ostatni status");

        private final int index;
        private final String name;

        COLUMNS(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return index;
        }

        public String getName() {
            return name;
        }
    }

}

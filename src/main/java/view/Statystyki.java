package view;

import dataproviders.LoggedUser;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeListener;
import models.ComboBoxModel;
import models.Employee;
import progressbar.ProgressController;
import repositories.EmployeeRepository;
import repositories.RepairRepository;

public class Statystyki extends JPanel implements ProgressController {

    private JLabel lblWydaneIlosc;
    private JLabel lblNaprawioneIlosc;
    private JLabel lblPrzetestowaneIlosc;
    private JLabel lblNaprawianeIlosc;
    private JLabel lblZdemontowaneIlosc;
    private JLabel lblPrzyjeteIlosc;
    private JLabel lblDoTestwIlosc;
    private JSpinner dataDo;
    private JSpinner dataOd;
    private JButton btnOdwie;
    private JComboBox<String> pracownicy;
    private List<Employee> pracownikArray;

    public Statystyki() {
        setView();
        setPracownicy();
        btnOdwie.addActionListener(e -> {
            startProgressBar();

            new SwingWorker() {

                @Override
                protected Object doInBackground() throws Exception {
                    lblPrzyjeteIlosc.setText(new RepairRepository().getNumberOfRepairs(1, getDataOd(), getDataDo(),
                            getLoginFromList()));
                    lblZdemontowaneIlosc.setText(new RepairRepository().getNumberOfRepairs(2, getDataOd(),
                            getDataDo(), getLoginFromList()));
                    lblNaprawianeIlosc.setText(new RepairRepository().getNumberOfRepairs(3, getDataOd(), getDataDo(),
                            getLoginFromList()));
                    lblDoTestwIlosc.setText(new RepairRepository().getNumberOfRepairs(4, getDataOd(), getDataDo(),
                            getLoginFromList()));
                    lblPrzetestowaneIlosc.setText(new RepairRepository().getNumberOfRepairs(5, getDataOd(),
                            getDataDo(), getLoginFromList()));
                    lblNaprawioneIlosc.setText(new RepairRepository().getNumberOfRepairs(6, getDataOd(), getDataDo(),
                            getLoginFromList()));
                    lblWydaneIlosc.setText(new RepairRepository().getNumberOfRepairs(7, getDataOd(), getDataDo(),
                            getLoginFromList()));
                    Thread.sleep(3000);
                    return null;
                }

                @Override
                protected void done() {
                    stopProgressBar();
                }

            }.execute();

        });

        btnOdwie.doClick();

        pracownicy.addItemListener(e -> btnOdwie.doClick());

        ChangeListener refresh = e -> btnOdwie.doClick();
        dataDo.addChangeListener(refresh);
        dataOd.addChangeListener(refresh);
    }

    private String getLoginFromList() {
        return pracownikArray.get(pracownicy.getSelectedIndex()).getLogin();
    }

    private String getDataOd() {
        return new SimpleDateFormat("yyyy-MM-dd").format(dataOd.getModel().getValue()) + " 00:00:00";
    }

    private String getDataDo() {
        return new SimpleDateFormat("yyyy-MM-dd").format(dataDo.getModel().getValue()) + " 23:59:59";
    }

    private void setPracownicy() {
        pracownikArray = new ArrayList<>();
        if (LoggedUser.isAdmin()) {
            pracownikArray = new EmployeeRepository().findByLoginOrName("");
            Employee wszyscy = new Employee();
            wszyscy.setLogin("Wszyscy");
            wszyscy.setImie("Wszyscy");
            pracownikArray.add(0, wszyscy);
        } else {
            pracownikArray.add(LoggedUser.getUser());
        }

        List<String> imiona = pracownikArray.stream()
                .map(Employee::getImie)
                .collect(Collectors.toList());
        pracownicy.setModel(new ComboBoxModel<>(imiona));
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        setPreferredSize(new Dimension(600, 445));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        JLabel lblDataOd = new JLabel("Data od: ");
        GridBagConstraints gbc_lblDataOd = new GridBagConstraints();
        gbc_lblDataOd.insets = new Insets(0, 0, 5, 5);
        gbc_lblDataOd.gridx = 0;
        gbc_lblDataOd.gridy = 0;
        add(lblDataOd, gbc_lblDataOd);

        dataOd = new JSpinner();
        dataOd.setModel(new SpinnerDateModel(new Date(), null, null, Calendar.DAY_OF_YEAR));
        GridBagConstraints gbc_spinner = new GridBagConstraints();
        gbc_spinner.insets = new Insets(0, 0, 5, 5);
        gbc_spinner.gridx = 1;
        gbc_spinner.gridy = 0;
        add(dataOd, gbc_spinner);

        JLabel lblDataDo = new JLabel("Data do: ");
        GridBagConstraints gbc_lblDataDo = new GridBagConstraints();
        gbc_lblDataDo.insets = new Insets(0, 0, 5, 5);
        gbc_lblDataDo.gridx = 3;
        gbc_lblDataDo.gridy = 0;
        add(lblDataDo, gbc_lblDataDo);

        dataDo = new JSpinner();
        dataDo.setModel(new SpinnerDateModel(new Date(), null, null, Calendar.DAY_OF_YEAR));
        GridBagConstraints gbc_spinner_1 = new GridBagConstraints();
        gbc_spinner_1.insets = new Insets(0, 0, 5, 5);
        gbc_spinner_1.gridx = 4;
        gbc_spinner_1.gridy = 0;
        add(dataDo, gbc_spinner_1);

        SimpleDateFormat shortf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.DATE_FIELD);

        dataOd.setEditor(new JSpinner.DateEditor(dataOd, shortf.toPattern()));
        dataDo.setEditor(new JSpinner.DateEditor(dataDo, shortf.toPattern()));

        btnOdwie = new JButton("Odśwież");

        GridBagConstraints gbc_btnOdwie = new GridBagConstraints();
        gbc_btnOdwie.insets = new Insets(0, 0, 5, 5);
        gbc_btnOdwie.gridx = 6;
        gbc_btnOdwie.gridy = 0;
        add(btnOdwie, gbc_btnOdwie);

        JLabel lblPracownik = new JLabel("Pracownik: ");
        GridBagConstraints gbc_lblPracownik = new GridBagConstraints();
        gbc_lblPracownik.insets = new Insets(0, 0, 5, 5);
        gbc_lblPracownik.anchor = GridBagConstraints.EAST;
        gbc_lblPracownik.gridx = 7;
        gbc_lblPracownik.gridy = 0;
        add(lblPracownik, gbc_lblPracownik);

        pracownicy = new JComboBox<>();
        pracownicy.setModel(new DefaultComboBoxModel<>(new String[]{"Wszyscy", "Marcin Kolenda", "Michał Usyk", "Mateusz Rosołowski", "Andrzej Gołuchowski", "Krystian Sacharuk", "Grzegorz Robak", "Paweł Stawski"}));
        GridBagConstraints gbc_pracownicy = new GridBagConstraints();
        gbc_pracownicy.insets = new Insets(0, 0, 5, 0);
        gbc_pracownicy.fill = GridBagConstraints.HORIZONTAL;
        gbc_pracownicy.gridx = 8;
        gbc_pracownicy.gridy = 0;
        add(pracownicy, gbc_pracownicy);

        JLabel lblPrzyjte = new JLabel("Przyjęte: ");
        GridBagConstraints gbc_lblPrzyjte = new GridBagConstraints();
        gbc_lblPrzyjte.insets = new Insets(0, 0, 5, 5);
        gbc_lblPrzyjte.gridx = 0;
        gbc_lblPrzyjte.gridy = 2;
        add(lblPrzyjte, gbc_lblPrzyjte);

        lblPrzyjeteIlosc = new JLabel("przyjete ilosc");
        GridBagConstraints gbc_lblPrzyjeteIlosc = new GridBagConstraints();
        gbc_lblPrzyjeteIlosc.insets = new Insets(0, 0, 5, 5);
        gbc_lblPrzyjeteIlosc.gridx = 1;
        gbc_lblPrzyjeteIlosc.gridy = 2;
        add(lblPrzyjeteIlosc, gbc_lblPrzyjeteIlosc);

        JLabel lblZdemontowane = new JLabel("Zdemontowane:");
        GridBagConstraints gbc_lblZdemontowane = new GridBagConstraints();
        gbc_lblZdemontowane.insets = new Insets(0, 0, 5, 5);
        gbc_lblZdemontowane.gridx = 0;
        gbc_lblZdemontowane.gridy = 3;
        add(lblZdemontowane, gbc_lblZdemontowane);

        lblZdemontowaneIlosc = new JLabel("zdemontowane ilosc");
        GridBagConstraints gbc_lblZdemontowaneIlosc = new GridBagConstraints();
        gbc_lblZdemontowaneIlosc.insets = new Insets(0, 0, 5, 5);
        gbc_lblZdemontowaneIlosc.gridx = 1;
        gbc_lblZdemontowaneIlosc.gridy = 3;
        add(lblZdemontowaneIlosc, gbc_lblZdemontowaneIlosc);

        JLabel lblNaprawiane = new JLabel("Naprawiane: ");
        GridBagConstraints gbc_lblNaprawiane = new GridBagConstraints();
        gbc_lblNaprawiane.insets = new Insets(0, 0, 5, 5);
        gbc_lblNaprawiane.gridx = 0;
        gbc_lblNaprawiane.gridy = 4;
        add(lblNaprawiane, gbc_lblNaprawiane);

        lblNaprawianeIlosc = new JLabel("Naprawiane ilosc");
        GridBagConstraints gbc_lblNaprawianeIlosc = new GridBagConstraints();
        gbc_lblNaprawianeIlosc.insets = new Insets(0, 0, 5, 5);
        gbc_lblNaprawianeIlosc.gridx = 1;
        gbc_lblNaprawianeIlosc.gridy = 4;
        add(lblNaprawianeIlosc, gbc_lblNaprawianeIlosc);

        JLabel lblDoTestw = new JLabel("Do testów:");
        GridBagConstraints gbc_lblDoTestw = new GridBagConstraints();
        gbc_lblDoTestw.insets = new Insets(0, 0, 5, 5);
        gbc_lblDoTestw.gridx = 0;
        gbc_lblDoTestw.gridy = 5;
        add(lblDoTestw, gbc_lblDoTestw);

        lblDoTestwIlosc = new JLabel("Do testow ilosc");
        GridBagConstraints gbc_lblDoTestwIlosc = new GridBagConstraints();
        gbc_lblDoTestwIlosc.insets = new Insets(0, 0, 5, 5);
        gbc_lblDoTestwIlosc.gridx = 1;
        gbc_lblDoTestwIlosc.gridy = 5;
        add(lblDoTestwIlosc, gbc_lblDoTestwIlosc);

        JLabel lblPrzetestowane = new JLabel("Przetestowane:");
        GridBagConstraints gbc_lblPrzetestowane = new GridBagConstraints();
        gbc_lblPrzetestowane.insets = new Insets(0, 0, 5, 5);
        gbc_lblPrzetestowane.gridx = 0;
        gbc_lblPrzetestowane.gridy = 6;
        add(lblPrzetestowane, gbc_lblPrzetestowane);

        lblPrzetestowaneIlosc = new JLabel("przetestowane ilosc");
        GridBagConstraints gbc_lblPrzetestowaneIlosc = new GridBagConstraints();
        gbc_lblPrzetestowaneIlosc.insets = new Insets(0, 0, 5, 5);
        gbc_lblPrzetestowaneIlosc.gridx = 1;
        gbc_lblPrzetestowaneIlosc.gridy = 6;
        add(lblPrzetestowaneIlosc, gbc_lblPrzetestowaneIlosc);

        JLabel lblNaprawione = new JLabel("Naprawione: ");
        GridBagConstraints gbc_lblNaprawione = new GridBagConstraints();
        gbc_lblNaprawione.insets = new Insets(0, 0, 5, 5);
        gbc_lblNaprawione.gridx = 0;
        gbc_lblNaprawione.gridy = 7;
        add(lblNaprawione, gbc_lblNaprawione);

        lblNaprawioneIlosc = new JLabel("naprawione");
        GridBagConstraints gbc_lblNaprawioneIlosc = new GridBagConstraints();
        gbc_lblNaprawioneIlosc.insets = new Insets(0, 0, 5, 5);
        gbc_lblNaprawioneIlosc.gridx = 1;
        gbc_lblNaprawioneIlosc.gridy = 7;
        add(lblNaprawioneIlosc, gbc_lblNaprawioneIlosc);

        JLabel lblWydane = new JLabel("Wydane: ");
        GridBagConstraints gbc_lblWydane = new GridBagConstraints();
        gbc_lblWydane.insets = new Insets(0, 0, 0, 5);
        gbc_lblWydane.gridx = 0;
        gbc_lblWydane.gridy = 8;
        add(lblWydane, gbc_lblWydane);

        lblWydaneIlosc = new JLabel("wydaneIlosc");
        GridBagConstraints gbc_lblWydaneIlosc = new GridBagConstraints();
        gbc_lblWydaneIlosc.insets = new Insets(0, 0, 0, 5);
        gbc_lblWydaneIlosc.gridx = 1;
        gbc_lblWydaneIlosc.gridy = 8;
        add(lblWydaneIlosc, gbc_lblWydaneIlosc);
    }

}

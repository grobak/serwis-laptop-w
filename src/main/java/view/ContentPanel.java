package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JPanel;

import components.Buttons;
import components.TopSearchPanel;

public class ContentPanel extends JPanel {
	private JPanel buttons = new JPanel();
	private JPanel content = new JPanel();
	
	public ContentPanel() {
		setLayout(new BorderLayout(0, 0));
		add(buttons, BorderLayout.NORTH);
		add(content, BorderLayout.CENTER);
		
	}

	public JPanel getButtons() {
		return buttons;
	}

	public void setButtons(JPanel buttons) {
		this.buttons = buttons;
		EventQueue.invokeLater(() -> {
			remove(buttons);
			add(buttons, BorderLayout.NORTH);
		});
	}
	public void setButtons(Buttons buttons) {
		this.buttons = buttons;
		EventQueue.invokeLater(() -> {
			remove(buttons);
			add(buttons, BorderLayout.NORTH);
		});
	}
	public void setButtons(TopSearchPanel buttons) {
		this.buttons = buttons;
		EventQueue.invokeLater(() -> {
			remove(buttons);
			add(buttons, BorderLayout.NORTH);
		});
	}

	public JPanel getContent() {
		return content;
	}

	public void setContent(JPanel content) {
		this.content = content;
		EventQueue.invokeLater(() -> {
			remove(content);
			add(content, BorderLayout.CENTER);
		});
	}

}

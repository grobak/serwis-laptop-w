package view;

import components.ButtonPanel;
import controllers.KlientFormValidator;
import dataproviders.LoggedUser;
import interfaces.BackgroundTask;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import listeners.PanelChooser;
import models.Customer;
import models.CustomerAddress.AddressBuilder;
import repositories.CustomerRepository;
import view.forms.KlientForm;

public class PanelSzczegolyKlient extends JPanel implements BackgroundTask, PanelChooser {

    private final Customer klient;
    private final KlientForm form;
    private final ButtonPanel buttonsModify;

    public PanelSzczegolyKlient(Customer klient, JPanel previousPanel) {
        this.buttonsModify = new ButtonPanel();
        this.form = new KlientForm();
        this.klient = klient;

        setView();
        checkDeletePrivilages();

        buttonsModify.getBtnWr().addActionListener(e -> {
            setVisible(false);
            backgroundTask(() -> setPanel("Wyszukaj sprzęt", previousPanel));
        });
        buttonsModify.getBtnDrukuj().setVisible(false);
        buttonsModify.getBtnUsu().addActionListener(e -> {
            if (confirmDelete() == JOptionPane.YES_OPTION) {
                backgroundTask(
                        () -> {
                            if (new CustomerRepository().hasHardware(klient)) {
                                JOptionPane.showMessageDialog(null, "Nie można usunąć klienta: Klient posiada komputery w bazie.", "Błąd",
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                new CustomerRepository().delete(klient);
                            }
                        },
                        () -> buttonsModify.getBtnWr().doClick());
            }
        });
        buttonsModify.getBtnCofnij().addActionListener(e -> resetKlientData());
        buttonsModify.getBtnZapisz().addActionListener(e -> backgroundTask(this::aktualizujKlienta));

        form.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttonsModify.getBtnZapisz().doClick();
                }
            }
        });

        resetKlientData();
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        setPreferredSize(new Dimension(600, 445));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_buttonsModify = new GridBagConstraints();
        gbc_buttonsModify.gridwidth = 5;
        gbc_buttonsModify.insets = new Insets(0, 0, 5, 5);
        gbc_buttonsModify.fill = GridBagConstraints.BOTH;
        gbc_buttonsModify.gridx = 1;
        gbc_buttonsModify.gridy = 0;
        add(buttonsModify, gbc_buttonsModify);

        GridBagConstraints gbc_klientForm = new GridBagConstraints();
        gbc_klientForm.gridwidth = 5;
        gbc_klientForm.insets = new Insets(0, 0, 5, 5);
        gbc_klientForm.fill = GridBagConstraints.BOTH;
        gbc_klientForm.gridx = 1;
        gbc_klientForm.gridy = 1;
        add(form, gbc_klientForm);

        buttonsModify.getBtnUsu().setEnabled(false);
    }

    private void checkDeletePrivilages() {
        if (LoggedUser.isAdmin()) {
            buttonsModify.getBtnUsu().setEnabled(true);
        }
    }

    private void resetKlientData() {
        form.setChckbxKlientFirmowy(klient.isFirma());
        form.setNazwaKlienta(klient.getName());
        form.setNIP(klient.getNIP());
        form.setMiasto(klient.getAddress().getCity());
        form.setUlica(klient.getAddress().getStreet());
        form.setKod(klient.getAddress().getZipcode());
        form.setTelefon(klient.getAddress().getPhone());
        form.setEmail(klient.getAddress().getEmail());
        form.setNotatki(klient.getNotes());
    }

    private void aktualizujKlienta() {
        Customer klientT = new Customer.CustomerBuilder(form.generate())
                .id(klient.getId())
                .address(new AddressBuilder(klient.getAddress())
                        .id(klient.getAddressID())
                        .build())
                .build();

        KlientFormValidator validator = new KlientFormValidator(form);
        if (validator.formIsValid()) {
            backgroundTask(() -> new CustomerRepository().update(klientT));
        } else {
            JOptionPane.showMessageDialog(null, validator.getErrorMessage(), "Błąd", JOptionPane.ERROR_MESSAGE);
        }
    }

    private int confirmDelete() {
        Object options[] = {"Tak", "Nie"};
        return JOptionPane.showOptionDialog(null, "Czy na pewno chcesz usunąć klienta?", "Usuń klienta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
    }

}

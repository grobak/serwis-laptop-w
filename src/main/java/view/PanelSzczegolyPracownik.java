package view;

import components.ButtonPanel;
import controllers.PracownikFormValidator;
import interfaces.BackgroundTask;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import listeners.PanelChooser;
import models.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repositories.EmployeeRepository;
import view.forms.PracownikForm;

public class PanelSzczegolyPracownik extends JPanel implements BackgroundTask, PanelChooser {

    private Employee pracownik = new Employee();
    private Employee pracownikUpdate = new Employee();
    private ButtonPanel buttonPanel = new ButtonPanel();
    private PracownikForm form = new PracownikForm();
    private PracownikFormValidator valid = new PracownikFormValidator();

    private static final Logger LOG = LogManager.getLogger("src");

    public PanelSzczegolyPracownik(Employee prac) {
        this.pracownik = prac;
        try {
            pracownikUpdate = (Employee) pracownik.clone();
        } catch (CloneNotSupportedException ex) {
            LOG.error(ex, ex);
        }

        setView();
        setPracownikData();

        buttonPanel.getBtnWr().addActionListener(e -> setPanel("Wyszukaj pracownika", new WyszukajPracownika()));
        buttonPanel.getBtnUsu().addActionListener(e -> {
            Object options[] = {"Tak", "Nie"};
            int reply = JOptionPane.showOptionDialog(null, "Czy na pewno chcesz usunąć pracownika?", "Usuń pracownika", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (reply == JOptionPane.YES_OPTION) {
                backgroundTask(() -> new EmployeeRepository().delete(pracownik.getLogin()),
                        () -> buttonPanel.getBtnWr().doClick());
            }
        });

        buttonPanel.getBtnCofnij().addActionListener(e -> setPracownikData());

        buttonPanel.getBtnZapisz().addActionListener(e -> {
            if (valid.isFormValidUpdate(form)) {
                updatePracownik();
            } else {
                JOptionPane.showMessageDialog(null, valid.getErrorMessage(),
                        "Błąd", JOptionPane.ERROR_MESSAGE);
            }
        });

    }

    private void updatePracownik() {
        readFieldsForUpdate();
        backgroundTask(
                () -> new EmployeeRepository().update(pracownik.getLogin(), pracownikUpdate));
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        setPreferredSize(new Dimension(600, 445));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.gridwidth = 9;
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        add(buttonPanel, gbc_panel);

        GridBagConstraints gbc_panelDanePracownika = new GridBagConstraints();
        gbc_panelDanePracownika.gridwidth = 7;
        gbc_panelDanePracownika.insets = new Insets(0, 0, 5, 5);
        gbc_panelDanePracownika.fill = GridBagConstraints.BOTH;
        gbc_panelDanePracownika.gridx = 1;
        gbc_panelDanePracownika.gridy = 1;
        add(form, gbc_panelDanePracownika);
        buttonPanel.getBtnDrukuj().setVisible(false);
    }

    private void setPracownikData() {
        form.setTextLogin(pracownik.getLogin());
        form.setTextName(pracownik.getImie());
        form.setTextPhone(pracownik.getTelefon());
        form.setComboJobPosition(new EmployeeRepository().getPositionName(pracownik.getPositionId()));
    }

    private void readFieldsForUpdate() {
        String login = form.getLogin();
        String imie = form.getName();
        String haslo;
        if (form.passwordsMatch()) {
            haslo = form.getPassword();
        } else {
            haslo = "";
        }
        String telefon = form.getPhone();
        if (login.isEmpty()) {
            pracownikUpdate.setLogin(pracownik.getLogin());
        } else {
            pracownikUpdate.setLogin(login);
        }
        if (imie.isEmpty()) {
            pracownikUpdate.setImie(pracownik.getImie());
        } else {
            pracownikUpdate.setImie(imie);
        }
        if (haslo.isEmpty()) {
            pracownikUpdate.setHaslo(pracownik.getHaslo());
        } else {
            pracownikUpdate.setHaslo(haslo);
        }
        if (telefon.isEmpty()) {
            pracownikUpdate.setTelefon(pracownik.getTelefon());
        } else {
            pracownikUpdate.setTelefon(telefon);
        }
        pracownikUpdate.setPositionId(form.getIdStanowiska());
    }
}

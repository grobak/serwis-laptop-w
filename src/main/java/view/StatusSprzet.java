package view;

import interfaces.BackgroundTask;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import listeners.PanelChooser;
import models.Computer;
import models.RepairStatus;
import repositories.ComputerRepository;
import view.forms.AbstractPanelWyszukaj;

public class StatusSprzet extends AbstractPanelWyszukaj implements BackgroundTask, PanelChooser {

    private RepairStatus status;
    private Computer sprzet = new Computer();
    private List<Computer> sprzety;

    public StatusSprzet(RepairStatus status) {
        super();
        this.status = status;
        columnNames = new Object[]{"Właściciel", "RMA", "Model", "Data przyjęcia", "Data statusu"};
        setUpTable();
    }

    @Override
    public DefaultTableModel generateTableModel() {
        sprzety = new ComputerRepository().listByStatus(status.getValue(), topPanel.getSearchInput());

        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        model.setColumnIdentifiers(columnNames);
        for (Computer aSprzet : sprzety) {
            String wlasciciel = aSprzet.getOwner();
            model.insertRow(0, new Object[]{wlasciciel, aSprzet.getRMA(), aSprzet.getModel(), aSprzet.getDataPrzyjecia(), aSprzet.getStatusData()});
        }
        return model;
    }

    @Override
    public void doubleClickAction() {
        sprzet = sprzety.get(sprzety.size() - lastClickedRow - 1);
        setPanel("Szczegóły sprzętu", new PanelSzczegolyNaprawy(sprzet, this));
    }

    @Override
    public void setUpTable() {
        backgroundTask(
                () -> {
                    searchResultTable.setModel(generateTableModel());
                    searchResultTable.getColumnModel().getColumn(0).setMinWidth(140);
                    searchResultTable.getColumnModel().getColumn(1).setMaxWidth(60);
                    searchResultTable.getColumnModel().getColumn(3).setMaxWidth(100);
                    searchResultTable.getColumnModel().getColumn(3).setMinWidth(100);
                },
                () -> topPanel.getBtnWyszukaj().setEnabled(true));
    }

}

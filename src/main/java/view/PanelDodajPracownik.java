package view;

import components.ButtonsAdd;
import controllers.PracownikFormValidator;
import interfaces.BackgroundTask;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repositories.EmployeeRepository;
import view.forms.PracownikForm;

public class PanelDodajPracownik extends JPanel implements BackgroundTask {

    private static final Logger LOG = LogManager.getLogger("src");
    private PracownikForm form = new PracownikForm();
    private ButtonsAdd buttonPanel = new ButtonsAdd();

    public PanelDodajPracownik() {
        setView();

        buttonPanel.getBtnZapisz().addActionListener(e -> {
            PracownikFormValidator validator = new PracownikFormValidator();
            if (validator.isFormValid(form)) {
                addPracownikToDB();
            } else {
                JOptionPane.showMessageDialog(null, validator.getErrorMessage(), "Błąd",
                        JOptionPane.ERROR_MESSAGE);
            }
        });

        buttonPanel.getBtnCofnij().addActionListener(e -> form.clear());

        form.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER && buttonPanel.getBtnZapisz().isEnabled()) {
                    buttonPanel.getBtnZapisz().doClick();
                }
            }
        });
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{600, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_addButtonPanel = new GridBagConstraints();
        gbc_addButtonPanel.insets = new Insets(0, 0, 5, 0);
        gbc_addButtonPanel.anchor = GridBagConstraints.NORTH;
        gbc_addButtonPanel.fill = GridBagConstraints.HORIZONTAL;
        gbc_addButtonPanel.gridx = 0;
        gbc_addButtonPanel.gridy = 0;
        add(buttonPanel, gbc_addButtonPanel);

        GridBagConstraints gbc_addPracownikForm = new GridBagConstraints();
        gbc_addPracownikForm.anchor = GridBagConstraints.NORTH;
        gbc_addPracownikForm.fill = GridBagConstraints.HORIZONTAL;
        gbc_addPracownikForm.gridx = 0;
        gbc_addPracownikForm.gridy = 1;
        add(form, gbc_addPracownikForm);

    }

    private void addPracownikToDB() {
        backgroundTask(
                () -> new EmployeeRepository().add(form.generate()));
    }

}

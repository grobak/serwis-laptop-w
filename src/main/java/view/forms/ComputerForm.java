package view.forms;

import components.JTextFieldWthPopup;
import components.KomponentRodzajNaprawy;
import components.KomponentStatus;
import components.KomponentWyposazenie;
import components.form.RepairStatusButtons;
import dataproviders.LoggedUser;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import models.Computer;
import models.Equipment;

public class ComputerForm extends JPanel {

    private Computer sprzet = new Computer();
    private int idKlientUpdate = 0;
    private JTextFieldWthPopup textWlasciciel;
    private JTextFieldWthPopup textModel;
    private JTextFieldWthPopup textRMA;
    private JTextFieldWthPopup textBrak;
    private JTextFieldWthPopup textDataPrzyjecia;
    private JTextFieldWthPopup textOpisUsterki;
    private JTextFieldWthPopup textOpisNaprawy;
    private JTextFieldWthPopup textUwagi;
    private JTextFieldWthPopup textWypDod;
    private JButton btnWybierzKlienta;

    private KomponentRodzajNaprawy rodzajNaprawy = new KomponentRodzajNaprawy();
    private KomponentWyposazenie panelWyposazenie;
    private KomponentStatus komponentStatus;

    private RepairStatusButtons repairStatusButtons;

    public ComputerForm() {
        setView();
        ustawDostep();
        clear();
    }

    public ComputerForm(Computer sprzet) {
        this.sprzet = sprzet;
        setView();
        ustawDostep();
    }

    private void ustawDostep() {
        if (LoggedUser.isAdmin()) {
            allowToAdd();
        }
    }

    public Equipment generateSprzetWyposazenie() {
        return new Equipment(sprzet.getEquipment().getId(), panelWyposazenie, getTextWypDod().getText(), getTextBrak().getText());
    }

    public Computer generateSprzet() {
        Computer genSprzet = new Computer(sprzet.getId(), sprzet.getIdKlienta(), textModel.getText(), getTextDataPrzyjecia().getText(), getTextRMA().getText(), generateSprzetWyposazenie(), getTextOpisNaprawy().getText(), getTextOpisUsterki().getText(), getTextUwagi().getText());
        genSprzet.setRodzajNaprawy(rodzajNaprawy.getRodzajNaprawy());
        return genSprzet;
    }

    public void clear() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        EventQueue.invokeLater(() -> {
            textUwagi.setText("");
            textBrak.setText("");
            textWypDod.setText("");
            textModel.setText("");
            textRMA.setText("");
            textDataPrzyjecia.setText(dateFormat.format(cal.getTime()));
            textWlasciciel.setText("");
            textOpisNaprawy.setText("");
            textOpisUsterki.setText("");
            panelWyposazenie.wyczysc();
            rodzajNaprawy.setRodzajNaprawy(0);
        });
    }

    public boolean areRequiredFieldsCorrect() {
        return textModel.getText().trim().length() > 0 && textRMA.getText().trim().length() > 1 && textWlasciciel.getText().trim().length() > 0;
    }

    public void allowToAdd() {
        btnWybierzKlienta.setEnabled(true);
        textRMA.setEditable(true);
        textModel.setEditable(true);
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{20, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        Component horizontalStrut_13 = Box.createHorizontalStrut(20);
        GridBagConstraints gbc_horizontalStrut_13 = new GridBagConstraints();
        gbc_horizontalStrut_13.insets = new Insets(0, 0, 5, 5);
        gbc_horizontalStrut_13.gridx = 2;
        gbc_horizontalStrut_13.gridy = 0;
        add(horizontalStrut_13, gbc_horizontalStrut_13);

        JLabel lblDataPrzyjecia = new JLabel("Data przyjęcia: ");
        GridBagConstraints gbc_lblDataPrzyjecia = new GridBagConstraints();
        gbc_lblDataPrzyjecia.anchor = GridBagConstraints.EAST;
        gbc_lblDataPrzyjecia.insets = new Insets(0, 0, 5, 5);
        gbc_lblDataPrzyjecia.gridx = 3;
        gbc_lblDataPrzyjecia.gridy = 0;
        add(lblDataPrzyjecia, gbc_lblDataPrzyjecia);

        textDataPrzyjecia = new JTextFieldWthPopup();
        GridBagConstraints gbc_textDataPrzyjecia = new GridBagConstraints();
        gbc_textDataPrzyjecia.insets = new Insets(0, 0, 5, 0);
        gbc_textDataPrzyjecia.fill = GridBagConstraints.HORIZONTAL;
        gbc_textDataPrzyjecia.gridx = 4;
        gbc_textDataPrzyjecia.gridy = 0;
        add(textDataPrzyjecia, gbc_textDataPrzyjecia);
        textDataPrzyjecia.setColumns(10);
        textDataPrzyjecia.setEnabled(false);
        textDataPrzyjecia.setDisabledTextColor(Color.BLACK);

        panelWyposazenie = new KomponentWyposazenie();
        GridBagConstraints gbc_wyposazenie = new GridBagConstraints();
        gbc_wyposazenie.gridwidth = 2;
        gbc_wyposazenie.gridheight = 3;
        gbc_wyposazenie.insets = new Insets(0, 0, 5, 0);
        gbc_wyposazenie.fill = GridBagConstraints.BOTH;
        gbc_wyposazenie.gridx = 3;
        gbc_wyposazenie.gridy = 1;
        add(panelWyposazenie, gbc_wyposazenie);

        JPanel panelDaneLaptopa = new JPanel();
        GridBagConstraints gbc_panelDaneLaptopa = new GridBagConstraints();
        gbc_panelDaneLaptopa.gridheight = 4;
        gbc_panelDaneLaptopa.gridwidth = 2;
        gbc_panelDaneLaptopa.insets = new Insets(0, 0, 5, 5);
        gbc_panelDaneLaptopa.fill = GridBagConstraints.BOTH;
        gbc_panelDaneLaptopa.gridx = 0;
        gbc_panelDaneLaptopa.gridy = 0;
        add(panelDaneLaptopa, gbc_panelDaneLaptopa);
        GridBagLayout gbl_panelDaneLaptopa = new GridBagLayout();
        gbl_panelDaneLaptopa.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panelDaneLaptopa.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
        gbl_panelDaneLaptopa.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_panelDaneLaptopa.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelDaneLaptopa.setLayout(gbl_panelDaneLaptopa);

        JLabel lblWlasciciel = new JLabel("Właściciel: ");
        lblWlasciciel.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_lblWlasciciel = new GridBagConstraints();
        gbc_lblWlasciciel.anchor = GridBagConstraints.EAST;
        gbc_lblWlasciciel.insets = new Insets(0, 0, 5, 5);
        gbc_lblWlasciciel.gridx = 0;
        gbc_lblWlasciciel.gridy = 0;
        panelDaneLaptopa.add(lblWlasciciel, gbc_lblWlasciciel);

        textWlasciciel = new JTextFieldWthPopup();
        GridBagConstraints gbc_textWlasciciel = new GridBagConstraints();
        gbc_textWlasciciel.fill = GridBagConstraints.HORIZONTAL;
        gbc_textWlasciciel.insets = new Insets(0, 0, 5, 5);
        gbc_textWlasciciel.gridx = 1;
        gbc_textWlasciciel.gridy = 0;
        panelDaneLaptopa.add(textWlasciciel, gbc_textWlasciciel);
        textWlasciciel.setColumns(10);
        textWlasciciel.setToolTipText("Pole wymagane");

        btnWybierzKlienta = new JButton("Wybierz");
        GridBagConstraints gbc_btnWybierz = new GridBagConstraints();
        gbc_btnWybierz.insets = new Insets(0, 0, 5, 0);
        gbc_btnWybierz.gridx = 2;
        gbc_btnWybierz.gridy = 0;
        panelDaneLaptopa.add(btnWybierzKlienta, gbc_btnWybierz);
        btnWybierzKlienta.setToolTipText("Wybierz klienta");

        JLabel lblModel = new JLabel("Model: ");
        lblModel.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_lblModel = new GridBagConstraints();
        gbc_lblModel.insets = new Insets(0, 0, 5, 5);
        gbc_lblModel.anchor = GridBagConstraints.EAST;
        gbc_lblModel.gridx = 0;
        gbc_lblModel.gridy = 1;
        panelDaneLaptopa.add(lblModel, gbc_lblModel);

        textModel = new JTextFieldWthPopup();
        GridBagConstraints gbc_textModel = new GridBagConstraints();
        gbc_textModel.fill = GridBagConstraints.HORIZONTAL;
        gbc_textModel.insets = new Insets(0, 0, 5, 5);
        gbc_textModel.gridx = 1;
        gbc_textModel.gridy = 1;
        panelDaneLaptopa.add(textModel, gbc_textModel);
        textModel.setColumns(10);
        textModel.setToolTipText("Pole wymagane");

        JLabel lblRma = new JLabel("RMA: ");
        lblRma.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_lblRma = new GridBagConstraints();
        gbc_lblRma.anchor = GridBagConstraints.EAST;
        gbc_lblRma.insets = new Insets(0, 0, 5, 5);
        gbc_lblRma.gridx = 0;
        gbc_lblRma.gridy = 2;
        panelDaneLaptopa.add(lblRma, gbc_lblRma);

        textRMA = new JTextFieldWthPopup();
        GridBagConstraints gbc_textRMA = new GridBagConstraints();
        gbc_textRMA.fill = GridBagConstraints.HORIZONTAL;
        gbc_textRMA.insets = new Insets(0, 0, 5, 5);
        gbc_textRMA.gridx = 1;
        gbc_textRMA.gridy = 2;
        panelDaneLaptopa.add(textRMA, gbc_textRMA);
        textRMA.setColumns(10);
        textRMA.setToolTipText("Pole wymagane. Minimum 2 znaki");

        JLabel lblBrak = new JLabel("Brak: ");
        GridBagConstraints gbc_lblBrak = new GridBagConstraints();
        gbc_lblBrak.anchor = GridBagConstraints.EAST;
        gbc_lblBrak.insets = new Insets(0, 0, 5, 5);
        gbc_lblBrak.gridx = 0;
        gbc_lblBrak.gridy = 3;
        panelDaneLaptopa.add(lblBrak, gbc_lblBrak);

        textBrak = new JTextFieldWthPopup();
        GridBagConstraints gbc_textBrak = new GridBagConstraints();
        gbc_textBrak.gridwidth = 2;
        gbc_textBrak.fill = GridBagConstraints.HORIZONTAL;
        gbc_textBrak.insets = new Insets(0, 0, 5, 0);
        gbc_textBrak.gridx = 1;
        gbc_textBrak.gridy = 3;
        panelDaneLaptopa.add(textBrak, gbc_textBrak);
        textBrak.setColumns(10);

        JLabel lblWypDod = new JLabel("<html>Wyposażenie <br>dodatkowe:</html>");
        lblWypDod.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc_lblWypDod = new GridBagConstraints();
        gbc_lblWypDod.insets = new Insets(0, 0, 0, 5);
        gbc_lblWypDod.gridx = 0;
        gbc_lblWypDod.gridy = 4;
        panelDaneLaptopa.add(lblWypDod, gbc_lblWypDod);

        textWypDod = new JTextFieldWthPopup();
        GridBagConstraints gbc_textWypDod = new GridBagConstraints();
        gbc_textWypDod.gridwidth = 2;
        gbc_textWypDod.fill = GridBagConstraints.HORIZONTAL;
        gbc_textWypDod.gridx = 1;
        gbc_textWypDod.gridy = 4;
        panelDaneLaptopa.add(textWypDod, gbc_textWypDod);
        textWypDod.setColumns(10);

        JLabel lblRodzajNaprawy = new JLabel("Rodzaj naprawy:");
        GridBagConstraints gbc_lblRodzajNaprawy = new GridBagConstraints();
        gbc_lblRodzajNaprawy.insets = new Insets(0, 0, 5, 5);
        gbc_lblRodzajNaprawy.gridx = 0;
        gbc_lblRodzajNaprawy.gridy = 4;
        add(lblRodzajNaprawy, gbc_lblRodzajNaprawy);

        GridBagConstraints gbc_komponentRodzajNaprawy = new GridBagConstraints();
        gbc_komponentRodzajNaprawy.gridwidth = 4;
        gbc_komponentRodzajNaprawy.insets = new Insets(0, 0, 5, 0);
        gbc_komponentRodzajNaprawy.fill = GridBagConstraints.BOTH;
        gbc_komponentRodzajNaprawy.gridx = 1;
        gbc_komponentRodzajNaprawy.gridy = 4;
        add(rodzajNaprawy, gbc_komponentRodzajNaprawy);

        JLabel lblOpisUsterki = new JLabel("Opis usterki: ");
        GridBagConstraints gbc_lblOpisUsterki = new GridBagConstraints();
        gbc_lblOpisUsterki.anchor = GridBagConstraints.EAST;
        gbc_lblOpisUsterki.insets = new Insets(0, 0, 5, 5);
        gbc_lblOpisUsterki.gridx = 0;
        gbc_lblOpisUsterki.gridy = 5;
        add(lblOpisUsterki, gbc_lblOpisUsterki);

        textOpisUsterki = new JTextFieldWthPopup();
        GridBagConstraints gbc_textOpisUsterki = new GridBagConstraints();
        gbc_textOpisUsterki.gridwidth = 3;
        gbc_textOpisUsterki.insets = new Insets(0, 0, 5, 5);
        gbc_textOpisUsterki.fill = GridBagConstraints.HORIZONTAL;
        gbc_textOpisUsterki.gridx = 1;
        gbc_textOpisUsterki.gridy = 5;
        add(textOpisUsterki, gbc_textOpisUsterki);
        textOpisUsterki.setColumns(10);

        JLabel lblOpisNaprawy = new JLabel("Opis naprawy: ");
        GridBagConstraints gbc_lblOpisNaprawy = new GridBagConstraints();
        gbc_lblOpisNaprawy.anchor = GridBagConstraints.EAST;
        gbc_lblOpisNaprawy.insets = new Insets(0, 0, 5, 5);
        gbc_lblOpisNaprawy.gridx = 0;
        gbc_lblOpisNaprawy.gridy = 6;
        add(lblOpisNaprawy, gbc_lblOpisNaprawy);

        textOpisNaprawy = new JTextFieldWthPopup();
        GridBagConstraints gbc_textOpisNaprawy = new GridBagConstraints();
        gbc_textOpisNaprawy.gridwidth = 3;
        gbc_textOpisNaprawy.insets = new Insets(0, 0, 5, 5);
        gbc_textOpisNaprawy.fill = GridBagConstraints.HORIZONTAL;
        gbc_textOpisNaprawy.gridx = 1;
        gbc_textOpisNaprawy.gridy = 6;
        add(textOpisNaprawy, gbc_textOpisNaprawy);
        textOpisNaprawy.setColumns(10);

        JLabel lblUwagi = new JLabel("Uwagi: ");
        GridBagConstraints gbc_lblUwagi = new GridBagConstraints();
        gbc_lblUwagi.anchor = GridBagConstraints.EAST;
        gbc_lblUwagi.insets = new Insets(0, 0, 5, 5);
        gbc_lblUwagi.gridx = 0;
        gbc_lblUwagi.gridy = 7;
        add(lblUwagi, gbc_lblUwagi);

        textUwagi = new JTextFieldWthPopup();
        GridBagConstraints gbc_textUwagi = new GridBagConstraints();
        gbc_textUwagi.gridwidth = 3;
        gbc_textUwagi.insets = new Insets(0, 0, 5, 5);
        gbc_textUwagi.fill = GridBagConstraints.HORIZONTAL;
        gbc_textUwagi.gridx = 1;
        gbc_textUwagi.gridy = 7;
        add(textUwagi, gbc_textUwagi);
        textUwagi.setColumns(10);

        komponentStatus = new KomponentStatus(sprzet);
        GridBagConstraints gbc_komponentStatus = new GridBagConstraints();
        gbc_komponentStatus.gridheight = 2;
        gbc_komponentStatus.gridwidth = 4;
        gbc_komponentStatus.insets = new Insets(0, 0, 0, 5);
        gbc_komponentStatus.fill = GridBagConstraints.BOTH;
        gbc_komponentStatus.gridx = 0;
        gbc_komponentStatus.gridy = 8;
        add(komponentStatus, gbc_komponentStatus);

        repairStatusButtons = new RepairStatusButtons();
        GridBagConstraints gbc_repairStatusButtons = new GridBagConstraints();
        gbc_repairStatusButtons.gridheight = 2;
        gbc_repairStatusButtons.fill = GridBagConstraints.BOTH;
        gbc_repairStatusButtons.gridx = 4;
        gbc_repairStatusButtons.gridy = 8;
        add(repairStatusButtons, gbc_repairStatusButtons);

        textWlasciciel.setEditable(false);
        btnWybierzKlienta.setEnabled(false);
        textRMA.setEditable(false);
        textModel.setEditable(false);

    }

    public JTextFieldWthPopup getTextOpisUsterki() {
        return textOpisUsterki;
    }

    public JTextFieldWthPopup getTextOpisNaprawy() {
        return textOpisNaprawy;
    }

    public JTextFieldWthPopup getTextUwagi() {
        return textUwagi;
    }

    public JTextFieldWthPopup getTextWlasciciel() {
        return textWlasciciel;
    }

    public JTextFieldWthPopup getTextModel() {
        return textModel;
    }

    public JTextFieldWthPopup getTextRMA() {
        return textRMA;
    }

    public JTextFieldWthPopup getTextBrak() {
        return textBrak;
    }

    public JTextFieldWthPopup getTextDataPrzyjecia() {
        return textDataPrzyjecia;
    }

    public KomponentWyposazenie getPanelWyposazenie() {
        return panelWyposazenie;
    }

    public KomponentStatus getKomponentStatus() {
        return komponentStatus;
    }

    public JButton getBtnDemonta() {
        return repairStatusButtons.getBtnDemonta();
    }

    public JButton getBtnTesty() {
        return repairStatusButtons.getBtnTesty();
    }

    public JButton getBtnNaprawa() {
        return repairStatusButtons.getBtnNaprawa();
    }

    public JButton getBtnZakonczenieNaprawy() {
        return repairStatusButtons.getBtnZakonczenieNaprawy();
    }

    public JButton getBtnWydanieSprztu() {
        return repairStatusButtons.getBtnWydanieSprztu();
    }

    public KomponentRodzajNaprawy getRodzajNaprawy() {
        return rodzajNaprawy;
    }

    public JButton getBtnWybierzKlienta() {
        return btnWybierzKlienta;
    }

    public JButton getBtnDoTestw() {
        return repairStatusButtons.getBtnDoTestw();
    }

    public JTextFieldWthPopup getTextWypDod() {
        return textWypDod;
    }

    public RepairStatusButtons getRepairStatusButtons() {
        return repairStatusButtons;
    }

    public void setNumberOfCompletedTests(int number) {
        EventQueue.invokeLater(() -> repairStatusButtons.getBtnTesty().setText("Testy [" + number + "/ 16]"));
    }

    public Computer getSprzet() {
        return sprzet;
    }

}

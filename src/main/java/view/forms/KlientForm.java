package view.forms;

import components.ClipboardPopUpMenu;
import components.JTextFieldWthPopup;
import interfaces.Form;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.regex.Pattern;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import models.Customer;
import models.CustomerAddress;
import models.CustomerAddress.AddressBuilder;

public class KlientForm extends JPanel implements Form<Customer> {

    private JTextFieldWthPopup textNazwaKlienta;
    private JTextFieldWthPopup textMiasto;
    private JTextFieldWthPopup textUlica;
    private JTextFieldWthPopup textKod;
    private JTextFieldWthPopup textTelefon;
    private JTextFieldWthPopup textEmail;
    private JTextFieldWthPopup textNIP;
    private JTextArea textNotatki;
    private JCheckBox chckbxKlientFirmowy;

    public KlientForm() {
        setView();

        chckbxKlientFirmowy.addItemListener(e -> {
            if (chckbxKlientFirmowy.isSelected()) {
                textNIP.setEnabled(true);
                AbstractDocument doc = ((AbstractDocument) textNIP.getDocument());
                doc.setDocumentFilter(new DocumentFilter() {

                    @Override
                    public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
                        if (Pattern.matches("[0-9-]+", text)) {
                            super.replace(fb, offset, length, text, attrs);
                            return;
                        }
                        Toolkit.getDefaultToolkit().beep();
                    }

                });
                textNIP.setDocument(doc);
            } else {
                textNIP.setEnabled(false);
                textNIP.setText("");
            }
        });

    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, 1.0};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        JPanel panelDaneKlienta = new JPanel();
        GridBagConstraints gbc_panelDaneKlienta = new GridBagConstraints();
        gbc_panelDaneKlienta.gridheight = 2;
        gbc_panelDaneKlienta.insets = new Insets(0, 0, 5, 5);
        gbc_panelDaneKlienta.fill = GridBagConstraints.BOTH;
        gbc_panelDaneKlienta.gridx = 0;
        gbc_panelDaneKlienta.gridy = 0;
        add(panelDaneKlienta, gbc_panelDaneKlienta);
        GridBagLayout gbl_panelDaneKlienta = new GridBagLayout();
        gbl_panelDaneKlienta.columnWidths = new int[]{100, 86, 0};
        gbl_panelDaneKlienta.rowHeights = new int[]{20, 0, 0};
        gbl_panelDaneKlienta.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panelDaneKlienta.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        panelDaneKlienta.setLayout(gbl_panelDaneKlienta);

        JLabel lblNazwaKlienta = new JLabel("Nazwa klienta: ");
        lblNazwaKlienta.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_lblNazwaKlienta = new GridBagConstraints();
        gbc_lblNazwaKlienta.anchor = GridBagConstraints.EAST;
        gbc_lblNazwaKlienta.insets = new Insets(0, 0, 5, 5);
        gbc_lblNazwaKlienta.gridx = 0;
        gbc_lblNazwaKlienta.gridy = 0;
        panelDaneKlienta.add(lblNazwaKlienta, gbc_lblNazwaKlienta);

        textNazwaKlienta = new JTextFieldWthPopup();
        GridBagConstraints gbc_textDane = new GridBagConstraints();
        gbc_textDane.insets = new Insets(0, 0, 5, 0);
        gbc_textDane.fill = GridBagConstraints.HORIZONTAL;
        gbc_textDane.anchor = GridBagConstraints.NORTH;
        gbc_textDane.gridx = 1;
        gbc_textDane.gridy = 0;
        panelDaneKlienta.add(textNazwaKlienta, gbc_textDane);
        textNazwaKlienta.setColumns(10);
        textNazwaKlienta.setToolTipText("Pole wymagane. Musi zawierać minimum 3 znaki.");

        JPanel panelFirma = new JPanel();
        GridBagConstraints gbc_panelFirma = new GridBagConstraints();
        gbc_panelFirma.gridheight = 2;
        gbc_panelFirma.insets = new Insets(0, 0, 5, 0);
        gbc_panelFirma.fill = GridBagConstraints.BOTH;
        gbc_panelFirma.gridx = 1;
        gbc_panelFirma.gridy = 0;
        add(panelFirma, gbc_panelFirma);
        GridBagLayout gbl_panelFirma = new GridBagLayout();
        gbl_panelFirma.columnWidths = new int[]{0, 0, 0};
        gbl_panelFirma.rowHeights = new int[]{0, 0, 0};
        gbl_panelFirma.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        gbl_panelFirma.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        panelFirma.setLayout(gbl_panelFirma);

        JLabel lblKlientFirmowy = new JLabel("Klient firmowy?");
        GridBagConstraints gbc_lblKlientFirmowy = new GridBagConstraints();
        gbc_lblKlientFirmowy.anchor = GridBagConstraints.EAST;
        gbc_lblKlientFirmowy.insets = new Insets(0, 0, 5, 5);
        gbc_lblKlientFirmowy.gridx = 0;
        gbc_lblKlientFirmowy.gridy = 0;
        panelFirma.add(lblKlientFirmowy, gbc_lblKlientFirmowy);

        chckbxKlientFirmowy = new JCheckBox();
        GridBagConstraints gbc_chckbxKlientFirmowy = new GridBagConstraints();
        gbc_chckbxKlientFirmowy.insets = new Insets(0, 0, 5, 0);
        gbc_chckbxKlientFirmowy.gridx = 1;
        gbc_chckbxKlientFirmowy.gridy = 0;
        panelFirma.add(chckbxKlientFirmowy, gbc_chckbxKlientFirmowy);

        JLabel lblNip = new JLabel("NIP: ");
        GridBagConstraints gbc_lblNip = new GridBagConstraints();
        gbc_lblNip.anchor = GridBagConstraints.EAST;
        gbc_lblNip.insets = new Insets(0, 0, 0, 5);
        gbc_lblNip.gridx = 0;
        gbc_lblNip.gridy = 1;
        panelFirma.add(lblNip, gbc_lblNip);

        textNIP = new JTextFieldWthPopup();
        GridBagConstraints gbc_textNIP = new GridBagConstraints();
        gbc_textNIP.fill = GridBagConstraints.HORIZONTAL;
        gbc_textNIP.gridx = 1;
        gbc_textNIP.gridy = 1;
        panelFirma.add(textNIP, gbc_textNIP);

        textNIP.setEnabled(false);
        textNIP.setColumns(10);

        Component verticalStrut = Box.createVerticalStrut(20);
        GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
        gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
        gbc_verticalStrut.gridx = 0;
        gbc_verticalStrut.gridy = 2;
        add(verticalStrut, gbc_verticalStrut);

        JPanel panelKontakt = new JPanel();
        GridBagConstraints gbc_panelKontakt = new GridBagConstraints();
        gbc_panelKontakt.gridheight = 2;
        gbc_panelKontakt.insets = new Insets(0, 0, 5, 5);
        gbc_panelKontakt.fill = GridBagConstraints.BOTH;
        gbc_panelKontakt.gridx = 0;
        gbc_panelKontakt.gridy = 3;
        add(panelKontakt, gbc_panelKontakt);
        GridBagLayout gbl_panelKontakt = new GridBagLayout();
        gbl_panelKontakt.columnWidths = new int[]{100, 0, 0};
        gbl_panelKontakt.rowHeights = new int[]{0, 0, 0};
        gbl_panelKontakt.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panelKontakt.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        panelKontakt.setLayout(gbl_panelKontakt);

        JLabel lblNrTelefonu = new JLabel("Nr telefonu: ");
        lblNrTelefonu.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_lblNrTelefonu = new GridBagConstraints();
        gbc_lblNrTelefonu.anchor = GridBagConstraints.EAST;
        gbc_lblNrTelefonu.insets = new Insets(0, 0, 5, 5);
        gbc_lblNrTelefonu.gridx = 0;
        gbc_lblNrTelefonu.gridy = 0;
        panelKontakt.add(lblNrTelefonu, gbc_lblNrTelefonu);

        textTelefon = new JTextFieldWthPopup();
        GridBagConstraints gbc_textTelefon = new GridBagConstraints();
        gbc_textTelefon.fill = GridBagConstraints.HORIZONTAL;
        gbc_textTelefon.insets = new Insets(0, 0, 5, 0);
        gbc_textTelefon.gridx = 1;
        gbc_textTelefon.gridy = 0;
        panelKontakt.add(textTelefon, gbc_textTelefon);
        textTelefon.setColumns(10);
        textTelefon.setToolTipText("Pole wymagane. Może zawierać wyłącznie cyfry, odstępy i znaki \"-\"");

        JLabel lblEmail = new JLabel("Email: ");
        GridBagConstraints gbc_lblEmail = new GridBagConstraints();
        gbc_lblEmail.anchor = GridBagConstraints.EAST;
        gbc_lblEmail.insets = new Insets(0, 0, 0, 5);
        gbc_lblEmail.gridx = 0;
        gbc_lblEmail.gridy = 1;
        panelKontakt.add(lblEmail, gbc_lblEmail);

        textEmail = new JTextFieldWthPopup();
        GridBagConstraints gbc_textEmail = new GridBagConstraints();
        gbc_textEmail.fill = GridBagConstraints.HORIZONTAL;
        gbc_textEmail.gridx = 1;
        gbc_textEmail.gridy = 1;
        panelKontakt.add(textEmail, gbc_textEmail);
        textEmail.setColumns(10);

        JPanel panelAdres = new JPanel();
        GridBagConstraints gbc_panelAdres = new GridBagConstraints();
        gbc_panelAdres.gridheight = 3;
        gbc_panelAdres.insets = new Insets(0, 0, 5, 0);
        gbc_panelAdres.fill = GridBagConstraints.BOTH;
        gbc_panelAdres.gridx = 1;
        gbc_panelAdres.gridy = 3;
        add(panelAdres, gbc_panelAdres);
        GridBagLayout gbl_panelAdres = new GridBagLayout();
        gbl_panelAdres.columnWidths = new int[]{0, 0, 0};
        gbl_panelAdres.rowHeights = new int[]{0, 0, 0, 0};
        gbl_panelAdres.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        gbl_panelAdres.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelAdres.setLayout(gbl_panelAdres);

        JLabel lblMiejscowo = new JLabel("Miejscowość: ");
        GridBagConstraints gbc_lblMiejscowo = new GridBagConstraints();
        gbc_lblMiejscowo.anchor = GridBagConstraints.EAST;
        gbc_lblMiejscowo.insets = new Insets(0, 0, 5, 5);
        gbc_lblMiejscowo.gridx = 0;
        gbc_lblMiejscowo.gridy = 0;
        panelAdres.add(lblMiejscowo, gbc_lblMiejscowo);

        textMiasto = new JTextFieldWthPopup();
        GridBagConstraints gbc_textMiasto = new GridBagConstraints();
        gbc_textMiasto.insets = new Insets(0, 0, 5, 0);
        gbc_textMiasto.fill = GridBagConstraints.HORIZONTAL;
        gbc_textMiasto.gridx = 1;
        gbc_textMiasto.gridy = 0;
        panelAdres.add(textMiasto, gbc_textMiasto);
        textMiasto.setColumns(10);

        JLabel lblUlicaINr = new JLabel("Ulica i nr domu: ");
        GridBagConstraints gbc_lblUlicaINr = new GridBagConstraints();
        gbc_lblUlicaINr.anchor = GridBagConstraints.EAST;
        gbc_lblUlicaINr.insets = new Insets(0, 0, 5, 5);
        gbc_lblUlicaINr.gridx = 0;
        gbc_lblUlicaINr.gridy = 1;
        panelAdres.add(lblUlicaINr, gbc_lblUlicaINr);

        textUlica = new JTextFieldWthPopup();
        GridBagConstraints gbc_textUlica = new GridBagConstraints();
        gbc_textUlica.fill = GridBagConstraints.HORIZONTAL;
        gbc_textUlica.insets = new Insets(0, 0, 5, 0);
        gbc_textUlica.gridx = 1;
        gbc_textUlica.gridy = 1;
        panelAdres.add(textUlica, gbc_textUlica);
        textUlica.setColumns(10);

        JLabel lblKodPocztowy = new JLabel("Kod pocztowy: ");
        GridBagConstraints gbc_lblKodPocztowy = new GridBagConstraints();
        gbc_lblKodPocztowy.anchor = GridBagConstraints.EAST;
        gbc_lblKodPocztowy.insets = new Insets(0, 0, 0, 5);
        gbc_lblKodPocztowy.gridx = 0;
        gbc_lblKodPocztowy.gridy = 2;
        panelAdres.add(lblKodPocztowy, gbc_lblKodPocztowy);

        textKod = new JTextFieldWthPopup();
        GridBagConstraints gbc_textKod = new GridBagConstraints();
        gbc_textKod.fill = GridBagConstraints.HORIZONTAL;
        gbc_textKod.gridx = 1;
        gbc_textKod.gridy = 2;
        panelAdres.add(textKod, gbc_textKod);
        textKod.setColumns(10);

        Component verticalStrut_1 = Box.createVerticalStrut(20);
        GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
        gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
        gbc_verticalStrut_1.gridx = 0;
        gbc_verticalStrut_1.gridy = 6;
        add(verticalStrut_1, gbc_verticalStrut_1);

        JPanel panelNotatek = new JPanel();
        GridBagConstraints gbc_panelNotatek = new GridBagConstraints();
        gbc_panelNotatek.gridwidth = 2;
        gbc_panelNotatek.fill = GridBagConstraints.BOTH;
        gbc_panelNotatek.gridx = 0;
        gbc_panelNotatek.gridy = 7;
        add(panelNotatek, gbc_panelNotatek);
        GridBagLayout gbl_panelNotatek = new GridBagLayout();
        gbl_panelNotatek.columnWidths = new int[]{100, 0, 0};
        gbl_panelNotatek.rowHeights = new int[]{60, 0};
        gbl_panelNotatek.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panelNotatek.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        panelNotatek.setLayout(gbl_panelNotatek);

        JLabel lblNotatki = new JLabel("Notatki:  ");
        GridBagConstraints gbc_lblNotatki = new GridBagConstraints();
        gbc_lblNotatki.anchor = GridBagConstraints.EAST;
        gbc_lblNotatki.insets = new Insets(0, 0, 0, 5);
        gbc_lblNotatki.gridx = 0;
        gbc_lblNotatki.gridy = 0;
        panelNotatek.add(lblNotatki, gbc_lblNotatki);

        textNotatki = new JTextArea();
        GridBagConstraints gbc_textNotatki = new GridBagConstraints();
        gbc_textNotatki.fill = GridBagConstraints.BOTH;
        gbc_textNotatki.gridx = 1;
        gbc_textNotatki.gridy = 0;
        panelNotatek.add(textNotatki, gbc_textNotatki);
        textNotatki.setFont(new Font("Tahoma", Font.PLAIN, 11));
        textNotatki.setBorder(new LineBorder(SystemColor.controlShadow));

        textNotatki.addMouseListener(new PopupListener());

    }

    public void clear() {
        setNazwaKlienta("");
        setEmail("");
        setKod("");
        setMiasto("");
        setNIP("");
        setTelefon("");
        setUlica("");
        setNotatki("");
        setChckbxKlientFirmowy(false);
    }

    public CustomerAddress generateKlientAdres() {
        return new AddressBuilder().city(getMiasto()).street(getUlica()).zipcode(getKod()).phone(getTelefon()).email(getEmail()).build();
    }

    public String getNazwaKlienta() {
        return textNazwaKlienta.getText();
    }

    public void setNazwaKlienta(String s) {
        textNazwaKlienta.setText(s);
    }

    public String getMiasto() {
        return textMiasto.getText();
    }

    public void setMiasto(String s) {
        textMiasto.setText(s);
    }

    public String getUlica() {
        return textUlica.getText();
    }

    public void setUlica(String s) {
        textUlica.setText(s);
    }

    public String getKod() {
        return textKod.getText();
    }

    public void setKod(String s) {
        textKod.setText(s);
    }

    public String getTelefon() {
        return textTelefon.getText();
    }

    public void setTelefon(String s) {
        textTelefon.setText(s);
    }

    public String getEmail() {
        return textEmail.getText();
    }

    public void setEmail(String s) {
        textEmail.setText(s);
    }

    public String getNIP() {
        return textNIP.getText();
    }

    public void setNIP(String s) {
        textNIP.setText(s);
    }

    public String getNotatki() {
        return textNotatki.getText();
    }

    public void setNotatki(String s) {
        textNotatki.setText(s);
    }

    public boolean isKlientFirmowy() {
        return chckbxKlientFirmowy.isSelected();
    }

    public void setChckbxKlientFirmowy(boolean b) {
        this.chckbxKlientFirmowy.setSelected(b);
    }

    public JTextFieldWthPopup getTextNazwaKlienta() {
        return textNazwaKlienta;
    }

    public JTextFieldWthPopup getTextMiasto() {
        return textMiasto;
    }

    public JTextFieldWthPopup getTextUlica() {
        return textUlica;
    }

    public JTextFieldWthPopup getTextKod() {
        return textKod;
    }

    public JTextFieldWthPopup getTextTelefon() {
        return textTelefon;
    }

    public JTextFieldWthPopup getTextEmail() {
        return textEmail;
    }

    public JTextFieldWthPopup getTextNIP() {
        return textNIP;
    }

    public JTextArea getTextNotatki() {
        return textNotatki;
    }

    public JCheckBox getChckbxKlientFirmowy() {
        return chckbxKlientFirmowy;
    }

    private class PopupListener extends MouseAdapter {

        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                new ClipboardPopUpMenu((JTextComponent) e.getSource()).show(e.getComponent(),
                        e.getX(), e.getY());
            }
        }
    }

    @Override
    public Customer generate() {
        return new Customer.CustomerBuilder()
                .isCompany(isKlientFirmowy())
                .name(getNazwaKlienta())
                .nip(getNIP())
                .address(generateKlientAdres())
                .notes(getNotatki()).build();

    }

}

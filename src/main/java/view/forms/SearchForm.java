package view.forms;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import models.ForcedListSelectionModel;

public class SearchForm extends JPanel {

    public JTable searchResultTable = new JTable();

    @SuppressWarnings("PMD.VariableNamingConventions")
    public SearchForm() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 0;
        add(scrollPane, gbc_scrollPane);

        scrollPane.setViewportView(searchResultTable);
        searchResultTable.setSelectionModel(new ForcedListSelectionModel());
    }

    public JTable getSearchResultTable() {
        return searchResultTable;
    }

}

package view.forms;

import components.JTextFieldWthPopup;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import models.Employee;

public class PracownikForm extends JPanel {

    private JTextFieldWthPopup textLogin = new JTextFieldWthPopup();
    private JTextFieldWthPopup textName = new JTextFieldWthPopup();
    private JTextFieldWthPopup textPhone = new JTextFieldWthPopup();
    private JPasswordField textPassword = new JPasswordField();
    private JPasswordField textPasswordConfirm = new JPasswordField();
    private JComboBox<String> comboJobPosition = new JComboBox<>();

    @SuppressWarnings("PMD.VariableNamingConventions")
    public PracownikForm() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 30, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        Component horizontalStrut = Box.createHorizontalStrut(20);
        GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
        gbc_horizontalStrut.anchor = GridBagConstraints.NORTH;
        gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
        gbc_horizontalStrut.gridx = 2;
        gbc_horizontalStrut.gridy = 0;
        add(horizontalStrut, gbc_horizontalStrut);

        JPanel passwordArea = new JPanel();
        GridBagConstraints gbc_passwordArea = new GridBagConstraints();
        gbc_passwordArea.anchor = GridBagConstraints.NORTH;
        gbc_passwordArea.gridwidth = 2;
        gbc_passwordArea.insets = new Insets(0, 0, 5, 0);
        gbc_passwordArea.fill = GridBagConstraints.HORIZONTAL;
        gbc_passwordArea.gridx = 3;
        gbc_passwordArea.gridy = 1;
        add(passwordArea, gbc_passwordArea);
        GridBagLayout gbl_passwordArea = new GridBagLayout();
        gbl_passwordArea.columnWidths = new int[]{53, 38, 0};
        gbl_passwordArea.rowHeights = new int[]{14, 0, 0};
        gbl_passwordArea.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_passwordArea.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        passwordArea.setLayout(gbl_passwordArea);

        JLabel lblHaso = new JLabel("Hasło: ");
        GridBagConstraints gbc_lblHaso = new GridBagConstraints();
        gbc_lblHaso.insets = new Insets(0, 0, 5, 5);
        gbc_lblHaso.anchor = GridBagConstraints.WEST;
        gbc_lblHaso.gridx = 0;
        gbc_lblHaso.gridy = 0;
        passwordArea.add(lblHaso, gbc_lblHaso);
        lblHaso.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_textPassword = new GridBagConstraints();
        gbc_textPassword.fill = GridBagConstraints.HORIZONTAL;
        gbc_textPassword.insets = new Insets(0, 0, 5, 0);
        gbc_textPassword.gridx = 1;
        gbc_textPassword.gridy = 0;
        passwordArea.add(textPassword, gbc_textPassword);
        textPassword.setColumns(10);

        JLabel lblPowtrzHaso = new JLabel("Powtórz hasło: ");
        GridBagConstraints gbc_lblPowtrzHaso = new GridBagConstraints();
        gbc_lblPowtrzHaso.insets = new Insets(0, 0, 0, 5);
        gbc_lblPowtrzHaso.gridx = 0;
        gbc_lblPowtrzHaso.gridy = 1;
        passwordArea.add(lblPowtrzHaso, gbc_lblPowtrzHaso);
        lblPowtrzHaso.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_textPasswordConfirm = new GridBagConstraints();
        gbc_textPasswordConfirm.fill = GridBagConstraints.HORIZONTAL;
        gbc_textPasswordConfirm.gridx = 1;
        gbc_textPasswordConfirm.gridy = 1;
        passwordArea.add(textPasswordConfirm, gbc_textPasswordConfirm);
        textPasswordConfirm.setColumns(10);

        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.anchor = GridBagConstraints.NORTH;
        gbc_panel.gridwidth = 2;
        gbc_panel.insets = new Insets(0, 0, 5, 5);
        gbc_panel.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 1;
        add(panel, gbc_panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        JLabel lblNazwaUytkownika = new JLabel("Nazwa użytkownika: ");
        GridBagConstraints gbc_lblNazwaUytkownika = new GridBagConstraints();
        gbc_lblNazwaUytkownika.insets = new Insets(0, 0, 5, 5);
        gbc_lblNazwaUytkownika.gridx = 0;
        gbc_lblNazwaUytkownika.gridy = 0;
        panel.add(lblNazwaUytkownika, gbc_lblNazwaUytkownika);
        lblNazwaUytkownika.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_textLogin = new GridBagConstraints();
        gbc_textLogin.insets = new Insets(0, 0, 5, 0);
        gbc_textLogin.fill = GridBagConstraints.HORIZONTAL;
        gbc_textLogin.gridx = 1;
        gbc_textLogin.gridy = 0;
        panel.add(textLogin, gbc_textLogin);
        textLogin.setColumns(10);

        JLabel lblImiINazwisko = new JLabel("Imię i nazwisko: ");
        GridBagConstraints gbc_lblImiINazwisko = new GridBagConstraints();
        gbc_lblImiINazwisko.anchor = GridBagConstraints.WEST;
        gbc_lblImiINazwisko.insets = new Insets(0, 0, 5, 5);
        gbc_lblImiINazwisko.gridx = 0;
        gbc_lblImiINazwisko.gridy = 1;
        panel.add(lblImiINazwisko, gbc_lblImiINazwisko);
        lblImiINazwisko.setFont(new Font("Tahoma", Font.BOLD, 11));
        GridBagConstraints gbc_textName = new GridBagConstraints();
        gbc_textName.insets = new Insets(0, 0, 5, 0);
        gbc_textName.fill = GridBagConstraints.HORIZONTAL;
        gbc_textName.gridx = 1;
        gbc_textName.gridy = 1;
        panel.add(textName, gbc_textName);
        textName.setColumns(10);

        JLabel lblTelefon = new JLabel("Telefon: ");
        GridBagConstraints gbc_lblTelefon = new GridBagConstraints();
        gbc_lblTelefon.anchor = GridBagConstraints.WEST;
        gbc_lblTelefon.insets = new Insets(0, 0, 0, 5);
        gbc_lblTelefon.gridx = 0;
        gbc_lblTelefon.gridy = 2;
        panel.add(lblTelefon, gbc_lblTelefon);
        GridBagConstraints gbc_textPhone = new GridBagConstraints();
        gbc_textPhone.fill = GridBagConstraints.HORIZONTAL;
        gbc_textPhone.gridx = 1;
        gbc_textPhone.gridy = 2;
        panel.add(textPhone, gbc_textPhone);
        textPhone.setColumns(10);

        Component verticalStrut = Box.createVerticalStrut(20);
        GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
        gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
        gbc_verticalStrut.gridx = 2;
        gbc_verticalStrut.gridy = 2;
        add(verticalStrut, gbc_verticalStrut);

        JLabel lblStanowisko = new JLabel("Stanowisko: ");
        GridBagConstraints gbc_lblStanowisko = new GridBagConstraints();
        gbc_lblStanowisko.anchor = GridBagConstraints.WEST;
        gbc_lblStanowisko.insets = new Insets(0, 0, 0, 5);
        gbc_lblStanowisko.gridx = 0;
        gbc_lblStanowisko.gridy = 3;
        add(lblStanowisko, gbc_lblStanowisko);

        String[] stan = {"Administrator", "Technik"};
        comboJobPosition.setModel(new DefaultComboBoxModel<>(stan));
        GridBagConstraints gbc_comboJobPosition = new GridBagConstraints();
        gbc_comboJobPosition.insets = new Insets(0, 0, 0, 5);
        gbc_comboJobPosition.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboJobPosition.gridx = 1;
        gbc_comboJobPosition.gridy = 3;
        add(comboJobPosition, gbc_comboJobPosition);

        textLogin.requestFocusInWindow();
    }

    public void clear() {
        EventQueue.invokeLater(() -> {
            textLogin.setText("");
            textName.setText("");
            textPhone.setText("");
            textPassword.setText("");
            textPasswordConfirm.setText("");
            comboJobPosition.setSelectedIndex(0);
        });
    }

    public boolean requiredFieldsAreFilled() {
        if (String.valueOf(textPassword.getPassword()).isEmpty()) {
            return false;
        }
        if (String.valueOf(textPasswordConfirm.getPassword()).isEmpty()) {
            return false;
        }
        if (textLogin.getText().isEmpty()) {
            return false;
        }
        if (textName.getText().isEmpty()) {
            return false;
        }
        return passwordsMatch();
    }

    public boolean passwordsMatch() {
        return String.valueOf(textPassword.getPassword()).equals(String.valueOf(textPasswordConfirm.getPassword()));
    }

    public Employee generate() {
        if (requiredFieldsAreFilled()) {
            return new Employee(textLogin.getText(), String.valueOf(textPassword.getPassword()),
                    textName.getText(), textPhone.getText(), comboJobPosition.getSelectedIndex() + 1);
        } else {
            return new Employee();
        }
    }

    public void setTextLogin(String textLogin) {
        this.textLogin.setText(textLogin);
    }

    public void setTextName(String textName) {
        this.textName.setText(textName);
    }

    public void setPassword(String password) {
        this.textPassword.setText(password);
    }

    public void setPasswordConfirm(String password) {
        this.textPasswordConfirm.setText(password);
    }

    public void setTextPhone(String textPhone) {
        this.textPhone.setText(textPhone);
    }

    public void setComboJobPosition(String comboJobPosition) {
        this.comboJobPosition.setSelectedItem(comboJobPosition);
    }

    public String getLogin() {
        return textLogin.getText();
    }

    public String getName() {
        return textName.getText();
    }

    public String getPhone() {
        return textPhone.getText();
    }

    public String getPassword() {
        return String.valueOf(textPassword.getPassword());
    }

    public String getPasswordConfirm() {
        return String.valueOf(textPasswordConfirm.getPassword());
    }

    public int getIdStanowiska() {
        return comboJobPosition.getSelectedIndex() + 1;
    }
}

package view.forms;

import components.TopSearchPanel;
import interfaces.BackgroundTask;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import models.ForcedListSelectionModel;

public abstract class AbstractPanelWyszukaj extends JPanel implements BackgroundTask {

    public JTable searchResultTable = new JTable();
    public TopSearchPanel topPanel = new TopSearchPanel();
    public int lastClickedRow;
    public Object[] columnNames;

    public AbstractPanelWyszukaj() {
        initView();
        topPanel.getTxtWyszukaj().requestFocusInWindow();

        topPanel.getBtnWyszukaj().addActionListener(e -> {
            topPanel.getBtnWyszukaj().setEnabled(false);
            setUpTable();
        });

        topPanel.getTxtWyszukaj().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    topPanel.getBtnWyszukaj().doClick();
                }
            }
        });

        searchResultTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                lastClickedRow = table.rowAtPoint(p);
                if (me.getButton() == MouseEvent.BUTTON1 && me.getClickCount() == 2) {
                    doubleClickAction();
                }
            }
        });
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void initView() {
        setPreferredSize(new Dimension(600, 445));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        topPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        add(topPanel, gbc_panel);

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 1;
        add(scrollPane, gbc_scrollPane);

        scrollPane.setViewportView(searchResultTable);
        searchResultTable.setSelectionModel(new ForcedListSelectionModel());
        topPanel.getTxtWyszukaj().requestFocusInWindow();
    }

    public abstract DefaultTableModel generateTableModel();

    public abstract void doubleClickAction();

    public void setUpTable() {
        backgroundTask(() -> searchResultTable.setModel(generateTableModel()), () -> topPanel.getBtnWyszukaj().setEnabled(true));
    }

}

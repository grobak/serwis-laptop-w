package view;

import components.ButtonsAdd;
import controllers.DodajSprzetController;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import view.forms.ComputerForm;

public class PanelDodajSprzet extends JPanel {

    private ButtonsAdd buttonPanel = new ButtonsAdd();
    private ComputerForm form = new ComputerForm();

    public PanelDodajSprzet(DodajSprzetController controller) {
        initView();
        buttonPanel.getBtnCofnij().addActionListener(e -> form.clear());

        buttonPanel.getBtnZapisz().addActionListener(e -> controller.saveSprzet());

        form.getBtnWybierzKlienta().addActionListener(e -> controller.chooseClient());

        DocumentListener fieldChangeListener = new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                enableSaveButton();
            }

            public void removeUpdate(DocumentEvent e) {
                enableSaveButton();
            }

            public void insertUpdate(DocumentEvent e) {
                enableSaveButton();
            }
        };

        form.getTextModel().getDocument().addDocumentListener(fieldChangeListener);
        form.getTextRMA().getDocument().addDocumentListener(fieldChangeListener);
        form.getTextWlasciciel().getDocument().addDocumentListener(fieldChangeListener);

    }

    public PanelDodajSprzet() {
        this(new DodajSprzetController());
    }

    private void enableSaveButton() {
        buttonPanel.getBtnZapisz().setEnabled(form.areRequiredFieldsCorrect());
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void initView() {
        setPreferredSize(new Dimension(600, 445));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{30, 60, 0, 0, 0, 0, 90, 0};
        gridBagLayout.rowHeights = new int[]{0, 14, 0};
        gridBagLayout.columnWeights = new double[]{1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
            Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        buttonPanel.getBtnZapisz().setEnabled(false);
        form.getKomponentStatus().setVisible(false);
        form.getRepairStatusButtons().setVisible(false);

        GridBagConstraints gbc_buttonsAdd = new GridBagConstraints();
        gbc_buttonsAdd.gridwidth = 7;
        gbc_buttonsAdd.insets = new Insets(0, 0, 5, 0);
        gbc_buttonsAdd.fill = GridBagConstraints.BOTH;
        gbc_buttonsAdd.gridx = 0;
        gbc_buttonsAdd.gridy = 0;
        add(buttonPanel, gbc_buttonsAdd);

        form.getTextWlasciciel().setEditable(false);

        GridBagConstraints gbc_sprzetForm = new GridBagConstraints();
        gbc_sprzetForm.gridwidth = 7;
        gbc_sprzetForm.fill = GridBagConstraints.BOTH;
        gbc_sprzetForm.gridx = 0;
        gbc_sprzetForm.gridy = 1;
        add(form, gbc_sprzetForm);

        form.allowToAdd();
    }

    public void setNazwaKlienta(String nazwaKlienta) {
        form.getTextWlasciciel().setText(nazwaKlienta);
    }

    public void clearForm() {
        form.clear();
    }

}

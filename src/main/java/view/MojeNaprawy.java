package view;

import interfaces.BackgroundTask;
import listeners.PanelChooser;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import models.Computer;
import repositories.ComputerRepository;
import view.forms.AbstractPanelWyszukaj;

public class MojeNaprawy extends AbstractPanelWyszukaj implements PanelChooser, BackgroundTask {

    private Computer sprzet = new Computer();
    private List<Computer> sprzety;

    public MojeNaprawy() {
        super();
        columnNames = new Object[]{"Właściciel", "RMA", "Model", "Data przyjęcia", "Ostatni status"};
        setUpTable();
    }

    @Override
    public DefaultTableModel generateTableModel() {
        sprzety = new ComputerRepository().myComputers(topPanel.getSearchInput());

        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        model.setColumnIdentifiers(columnNames);

        for (Computer aSprzet : sprzety) {
            model.insertRow(0, new Object[]{aSprzet.getOwner(), aSprzet.getRMA(), aSprzet.getModel(), aSprzet.getDataPrzyjecia(), aSprzet.getStatusData()});
        }

        return model;
    }

    @Override
    public void doubleClickAction() {
        startProgressBar();

        backgroundTask(() -> {
            sprzet = sprzety.get(sprzety.size() - lastClickedRow - 1);
            setPanel("Szczegóły sprzętu", new PanelSzczegolyNaprawy(sprzet, this));
        },
                this::stopProgressBar
        );
    }

    @Override
    public void setUpTable() {
        backgroundTask(() -> {
            searchResultTable.setModel(generateTableModel());
            searchResultTable.getColumnModel().getColumn(0).setMinWidth(140);
            searchResultTable.getColumnModel().getColumn(1).setMaxWidth(60);
            searchResultTable.getColumnModel().getColumn(3).setMaxWidth(100);
            searchResultTable.getColumnModel().getColumn(3).setMinWidth(100);
        },
            () -> topPanel.getBtnWyszukaj().setEnabled(true));
    }
}

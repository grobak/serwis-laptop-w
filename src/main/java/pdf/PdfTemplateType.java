package pdf;

public enum PdfTemplateType {

    EKSPERTYZA("ekspertyza.html"), KARTA("karta.html"), PRZYJECIE("przyjecie.html");

    private final String filename;

    PdfTemplateType(String name) {
        this.filename = name;
    }

    public String getFilename() {
        return this.filename;
    }
}

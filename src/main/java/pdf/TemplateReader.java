package pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class TemplateReader {
	private File templateFile;
	private String templateFilename;
	private InputStream tempStream = getClass().getResourceAsStream("res/"+templateFilename);
	
	public TemplateReader(String templateFilename){
		this.templateFilename = templateFilename;
		templateFile = new File(templateFilename);
		tempStream = getClass().getResourceAsStream("res/"+templateFilename);
	}
	
	public void getDefaultTemplate(){
		tempStream = getClass().getResourceAsStream("res/"+templateFilename);
	}
	
	public Scanner read() throws FileNotFoundException{
		if(templateFile.exists())
			return new Scanner(templateFile, StandardCharsets.UTF_8.name());
		else
			return new Scanner(tempStream, StandardCharsets.UTF_8.name());
	}
	
	public Scanner readFromFile() throws FileNotFoundException{
		return new Scanner(templateFile, StandardCharsets.UTF_8.name());
	}
	public Scanner readFromResource(){
		return new Scanner(tempStream, StandardCharsets.UTF_8.name());
	}

	public File getTemplateFile() {
		return templateFile;
	}

	public void setTemplateFile(File templateFile) {
		this.templateFile = templateFile;
	}

	public String getTemplateFilename() {
		return templateFilename;
	}

	public void setTemplateFilename(String templateFilename) {
		this.templateFilename = templateFilename;
	}
}

package pdf;

import controllers.PlaceholderReplacer;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import javax.swing.JOptionPane;
import models.Computer;
import models.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repositories.ComputerRepository;

public class PlikPDF {

    private File templateFile;
    private final File resultPDF = new File("temp.pdf");
    private final File resultHTML = new File("temp.html");
    private Computer sprzet;
    private Customer klient;
    private String warranty;
    private Scanner readBuffer;
    private InputStream tempStream;

    private static final Logger LOG = LogManager.getLogger("src");

    public PlikPDF(PdfTemplateType templateFilename, Computer sprzet, Customer klient) {
        this(templateFilename, sprzet, klient, "brak");
    }

    public PlikPDF(PdfTemplateType pdfTemplateType, Computer sprzet, Customer klient, String warranty) {
        this.sprzet = sprzet;
        this.klient = klient;
        this.warranty = warranty;
        String templateFilename = pdfTemplateType.getFilename();
        templateFile = new File(templateFilename);

        getTemplateFromJAR(templateFilename);

        if (isTempPdfAccessible()) {
            generatePDFfromTemplate();
        } else {
            JOptionPane.showMessageDialog(null, "Plik wydruku jest zablokowany. Zamknij program w którym jest otwarty.", "Błąd", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void getTemplateFromJAR(String templateFilename) {
        tempStream = getClass().getResourceAsStream(templateFilename);
    }

    private boolean isTempPdfAccessible() {
        File temp = new File("temp.pdf");
        return !temp.exists() || temp.renameTo(temp);
    }

    private void generatePDFfromTemplate() {
        try (PrintWriter writeBuffer = new PrintWriter(resultHTML, StandardCharsets.UTF_8.name())) {
            if (templateFile.exists()) {
                readBuffer = new Scanner(templateFile, StandardCharsets.UTF_8.name());
            } else {
                readBuffer = new Scanner(tempStream, StandardCharsets.UTF_8.name());
            }
            replacePlaceholders(readBuffer, writeBuffer);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            LOG.error(e, e);
        } finally {
            readBuffer.close();
        }
        convertToPDF();
        try {
            tempStream.close();
        } catch (IOException e) {
            LOG.error(e, e);
        }

        resultHTML.deleteOnExit();
        resultPDF.deleteOnExit();
    }

    private void replacePlaceholders(Scanner input, PrintWriter output) {
        sprzet.setStan(new ComputerRepository().getComputerStatus(sprzet));
        PlaceholderReplacer placeholderReplacer = new PlaceholderReplacer(sprzet, klient);
        if (!"brak".equalsIgnoreCase(warranty)) {
            placeholderReplacer.setWarranty(warranty);
        }
        String line;

        while (input.hasNextLine()) {
            line = input.nextLine();
            line = placeholderReplacer.replace(line);

            output.println(line);
        }

    }

    private void convertToPDF() {
        try {
            Process convert = Runtime.getRuntime().exec(".\\wkhtmltopdf\\bin\\wkhtmltopdf.exe temp.html temp.pdf");
            convert.waitFor();
        } catch (InterruptedException | IOException e) {
            LOG.error(e, e);
        }
    }

    /**
     * This method sends pdf file to default system printer
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    public void print() throws IOException, IllegalArgumentException {
        if (Desktop.getDesktop().isSupported(Desktop.Action.PRINT)) {
            Desktop.getDesktop().print(resultPDF);
        }
    }

    /**
     * This method opens PDF file in default pdf reader
     *
     * @throws IOException
     * @throws IllegalArgumentException
     */
    public void open() throws IOException, IllegalArgumentException {
        if (Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            Desktop.getDesktop().open(resultPDF);
        }
    }

}

package dataproviders;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConnectionValidator
{
	private static final Logger LOG = LogManager.getLogger("src");
	
	public boolean isServerUp(String hostname, int port)
	{
		Socket socket = new Socket();
		boolean reachable = false;
		try
		{
			socket.setSoTimeout( 500 );
			socket.connect( new InetSocketAddress( hostname, port ), 500 );
			reachable = true;
		}
		catch( IOException e )
		{
			reachable = false;
			LOG.error( e, e );
		}
		finally
		{
			if( socket.isConnected() )
				reachable = true;
			try
			{
				socket.close();
			}
			catch( IOException e )
			{
				LOG.error( e, e );
			}

		}
		return reachable;
	}

}

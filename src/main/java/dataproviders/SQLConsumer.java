package dataproviders;

import java.sql.SQLException;
import java.util.function.Consumer;

@FunctionalInterface
public interface SQLConsumer<T> extends Consumer<T>{

	default void accept(T t){
		try
		{
			acceptWithThrow(t);
		}
		catch( SQLException e )
		{
			org.apache.logging.log4j.LogManager.getLogger( "src" ).error( e, e );
			throw new RuntimeException(e);
		}
	}
	
	void acceptWithThrow( T t ) throws SQLException;
}

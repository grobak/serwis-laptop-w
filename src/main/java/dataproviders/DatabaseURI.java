package dataproviders;

public class DatabaseURI {
	private String adres;
	private String port;
	private String nazwa;
	private String smtp;
	private String email;
	private String haslo;
	private String lastLoginAttempt;
	
	public DatabaseURI(String adres, String port, String nazwa, String smtp, String email, String haslo){
		this.adres=adres;
		this.port=port;
		this.nazwa=nazwa;
		this.smtp=smtp;
		this.email=email;
		this.haslo=haslo;
	}
	
	public DatabaseURI(String adres, String port, String nazwa){
		this.adres=adres;
		this.port=port;
		this.nazwa=nazwa;
	}
	
	public String getDBurl() {
		return "jdbc:mysql://" + adres + ":" + port + "/"
				+ nazwa + "?characterEncoding=UTF-8&useUnicode=true&autoReconnect=true";
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adresBazy) {
		this.adres = adresBazy;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String portBazy) {
		this.port = portBazy;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwaBazy) {
		this.nazwa = nazwaBazy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String loginBazy) {
		this.email = loginBazy;
	}

	public String getHaslo() {
		return haslo;
	}

	public void setHaslo(String hasloBazy) {
		this.haslo = hasloBazy;
	}

	public String getLastLoginAttempt() {
		return lastLoginAttempt;
	}

	public void setLastLoginAttempt(String lastLoginAttempt) {
		this.lastLoginAttempt = lastLoginAttempt;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}
}

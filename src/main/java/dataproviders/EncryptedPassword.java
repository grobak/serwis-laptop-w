package dataproviders;

public class EncryptedPassword
{
	private final String hash;
	private final String salt;
	
	public EncryptedPassword(String hash, String salt)
	{
		super();
		this.hash = hash;
		this.salt = salt;
	}

	public String getHash()
	{
		return hash;
	}

	public String getSalt()
	{
		return salt;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( hash == null ) ? 0 : hash.hashCode() );
		result = prime * result + ( ( salt == null ) ? 0 : salt.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj )
	{
		if( this == obj )
			return true;
		if( obj == null )
			return false;
		if( getClass() != obj.getClass() )
			return false;
		EncryptedPassword other = ( EncryptedPassword ) obj;
		if( hash == null )
		{
			if( other.hash != null )
				return false;
		}
		else if( !hash.equals( other.hash ) )
			return false;
		if( salt == null )
		{
			if( other.salt != null )
				return false;
		}
		else if( !salt.equals( other.salt ) )
			return false;
		return true;
	}
	
	
	
}

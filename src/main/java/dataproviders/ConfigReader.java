package dataproviders;

import static dataproviders.ConfigProperties.dbHostname;
import static dataproviders.ConfigProperties.dbName;
import static dataproviders.ConfigProperties.dbPort;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import models.DatabaseConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigReader {

    private static final String FILENAME = "config.ini";
    private final File config;
    private static final Logger LOG = LogManager.getLogger("src");

    public ConfigReader() {
        config = new File(FILENAME);
        if (!config.exists()) {
            try {
                config.createNewFile();
                LOG.debug("Created new config.ini file");
                createDefaultConfig();
            } catch (IOException e) {
                LOG.error(e, e);
            }
        }
    }

    public DatabaseURI getDatabaseInfo() {
        return new DatabaseURI(getSetting(dbHostname), getSetting(dbPort), getSetting(dbName));
    }

    private void createDefaultConfig() {
        update(new DatabaseConfiguration());
    }

    private Map<ConfigProperties, String> odczyt() {
        Scanner in;
        HashMap<ConfigProperties, String> properties = new HashMap<>();
        try {
            in = new Scanner(config);

            while (in.hasNextLine()) {
                String line = in.nextLine();

                for (ConfigProperties prop : ConfigProperties.values()) {
                    if (line.startsWith(prop.getKeyString())) {
                        properties.put(prop, line.replace(prop.getKeyString(), ""));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            LOG.error(e, e);
            createDefaultConfig();
        }

        return properties;
    }

    public String getSetting(ConfigProperties propertyName) {
        return odczyt().get(propertyName);
    }

    public String getFilename() {
        return FILENAME;
    }

    public void saveLastLoggedUser(String username) {
        try {
            Set<Entry<ConfigProperties, String>> configValues = odczyt().entrySet();
            PrintWriter zapis = new PrintWriter(FILENAME);

            for (Entry<ConfigProperties, String> property : configValues) {
                if (!property.getKey().equals(ConfigProperties.lastLoggedUsername)) {
                    zapis.println(property.getKey().getKeyString() + property.getValue());
                } else {
                    zapis.println(property.getKey().getKeyString() + username);
                }
            }
            zapis.close();
        } catch (FileNotFoundException e) {
            LOG.error(e, e);
        }

    }

    public void update(DatabaseConfiguration dbConfig) {
        try {
            PrintWriter zapis = new PrintWriter(FILENAME);
            zapis.println(ConfigProperties.dbHostname.getKeyString() + dbConfig.getDbHostname());
            zapis.println(ConfigProperties.dbPort.getKeyString() + dbConfig.getPort());
            zapis.println(ConfigProperties.dbName.getKeyString() + dbConfig.getDbName());
            zapis.println(ConfigProperties.smtpHostname.getKeyString() + dbConfig.getSmtpHostname());
            zapis.println(ConfigProperties.email.getKeyString() + dbConfig.getEmail());
            zapis.println(ConfigProperties.emailPassword.getKeyString() + dbConfig.getEmailPassword());
            zapis.println(ConfigProperties.lastLoggedUsername.getKeyString() + dbConfig.getLastLoggedUsername());
            zapis.close();
        } catch (FileNotFoundException e) {
            LOG.error(e, e);
        }

    }

}

package dataproviders;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import models.Employee;
import repositories.EmployeeStatementFactory;
import repositories.SQLStatements;

public enum Database
{
	INSTANCE;
	private Connection connection;

	private static final Logger LOG = LogManager.getLogger( "src" );
	private final ConfigReader settings;	

	Database()
	{
		settings = new ConfigReader();
	}

	public void close()
	{
		try
		{
			if( connection != null )
				connection.close();
		}
		catch( SQLException e )
		{
			LOG.error( e, e );
		}
	}

	private String getSalt( String username )
	{
		String salt = "";
		try( PreparedStatement preStmt = getConnection().prepareStatement( SQLStatements.GET_PRACOWNIK_SALT ) )
		{
			preStmt.setString( 1, username );
			try( ResultSet result = preStmt.executeQuery() )
			{
				while( result.next() )
					salt = result.getString( "salt" );
			}
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return salt;
	}

	public boolean login( String login, String password )
	{
		String salt = getSalt( login );
		EncryptedPassword encryptedPassword;
		try
		{
			encryptedPassword = new PasswordGenerator().encrypt( password, salt );
		}	
		catch( NoSuchAlgorithmException e )
		{
			showError( e );
			return false;
		}
		Employee user = new Employee();

		try( PreparedStatement preStmt = new EmployeeStatementFactory().get( login ); 
			 ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
			{
				user.setPositionId( result.getInt( "Id_stanowiska" ) );
				user.setLogin( result.getString( "Login" ) );
				user.setHaslo( result.getString( "Haslo" ) );
				user.setImie( result.getString( "Imie" ) );
				user.setTelefon( result.getString( "Telefon" ) );
			}
		}
		catch( SQLException e )
		{
			showError( e );
			return false;
		}
		
		if( encryptedPassword.getHash().equals( user.getHaslo() ) )
		{
			new LoggedUser( user );
			return true;			
		}
		
		return false;
	}

	private boolean isConnectionLost() throws SQLException
	{
		return connection == null || !connection.isValid( 50 );
	}

	public boolean connectToDB() throws SQLException
	{
		connection = DriverManager.getConnection( settings.getDatabaseInfo().getDBurl(), "root", "" );
		return connection.isValid( 5 );
	}
	
	

	public DatabaseURI getDatabaseInfo()
	{
		return settings.getDatabaseInfo();
	}

	public String readLastUser()
	{
		return settings.getSetting(ConfigProperties.lastLoggedUsername);
	}

	public void saveLastLoggedUser( String username )
	{
		settings.saveLastLoggedUser( username );
	}

	public void rollback()
	{
		try
		{
			connection.rollback();
		}
		catch( SQLException e1 )
		{
			LOG.error( e1, e1 );
		}
	}
	
	public void setAutoCommit( boolean flag) throws SQLException
	{
		getConnection().setAutoCommit( flag );
	}
	
	public void commit() throws SQLException
	{
		getConnection().commit();
	}

	void connectIfNecessary() throws SQLException
	{
		if( isConnectionLost() )
			connectToDB();
	}

	public Connection getConnection() throws SQLException
	{
		connectIfNecessary();
		return connection;
	}

	private void showError( Exception e )
	{
		JOptionPane.showMessageDialog( null, e, "Błąd", JOptionPane.ERROR_MESSAGE );
		LOG.error( e, e );
	}
	
}
package dataproviders;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PasswordGenerator {
	
	private String encryptPassword(String password, String inputSalt) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(inputSalt.getBytes());
		byte[] bytes = md.digest(password.getBytes());
		StringBuilder sb = new StringBuilder();
		for(byte aByte : bytes)
		{
			sb.append( Integer.toString( (aByte & 0xff) + 0x100, 16 ).substring( 1 ) );
		}
		
		return sb.toString();
	}

	private String getSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt.toString();
	}
	
	public EncryptedPassword encrypt(String password) throws NoSuchAlgorithmException
	{
		return encrypt(password, getSalt());
	}
	
	public EncryptedPassword encrypt(String password, String salt) throws NoSuchAlgorithmException
	{
		return new EncryptedPassword(encryptPassword( password, salt ), salt );
	}
	
}

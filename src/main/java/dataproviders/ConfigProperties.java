package dataproviders;

public enum ConfigProperties{
	dbHostname("Adres = "), 
	dbPort("Port = "), 
	dbName("Nazwa = "), 
	smtpHostname("SMTP = "), 
	email("Email = "), 
	emailPassword("Haslo = "),
	lastLoggedUsername("Last login attempt by = "),
	hash("hash = ");

	
	private final String value;
	
	ConfigProperties( String value ) {
		this.value = value; 
	}

	public String getKeyString() {
		return value;
	}		
	
}

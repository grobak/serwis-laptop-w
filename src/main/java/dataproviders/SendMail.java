package dataproviders;

import static dataproviders.ConfigProperties.email;
import static dataproviders.ConfigProperties.emailPassword;
import static dataproviders.ConfigProperties.smtpHostname;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

public class SendMail {
	private ConfigReader settings = new ConfigReader();
    private final String SMTP_HOST_NAME = settings.getSetting(smtpHostname);
    private final String SMTP_AUTH_USER = settings.getSetting(email);
    private final String SMTP_AUTH_PWD  = settings.getSetting(emailPassword); //"WaxhLjKW";    
    
    public void sendRepairConfirmation(String recipientEmail, String rma) throws MessagingException {
        if (!configIsNull())
        {
            String messageContent = "<html><h3>Witamy.</h3>Naprawa komputera o nr " + rma + " została zakończona.<br/><br/>"
                    + "Pozdrawiamy, PC CENTER Serwis.<br/><br/>"
                    + "<p style=\"font-style:italic; font-size:smaller;\">Wiadomość wygenerowana automatycznie. "
                    + "Prosimy na nią nie odpowiadać.</p></html>";

            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", SMTP_HOST_NAME);
            props.put("mail.smtp.auth", "true");

            Authenticator auth = new SMTPAuthenticator();
            Session mailSession = Session.getDefaultInstance(props, auth);
            Transport transport = mailSession.getTransport();

            MimeMessage message = new MimeMessage(mailSession);
            message.setSubject("Zakończnie naprawy");
            message.setContent(messageContent, "text/html; charset=utf-8");
            message.setFrom(new InternetAddress("status@pccenter.pl"));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(recipientEmail));

            transport.connect();
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            transport.close();
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Ustawienia email nie są wypełnione poprawnie", "Błąd", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private boolean configIsNull(){
    	return SMTP_HOST_NAME.isEmpty() || SMTP_AUTH_USER.isEmpty() || SMTP_AUTH_PWD.isEmpty();
    }

    private class SMTPAuthenticator extends Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
           String username = SMTP_AUTH_USER;
           String password = SMTP_AUTH_PWD;
           return new PasswordAuthentication(username, password);
        }
    }
 
}

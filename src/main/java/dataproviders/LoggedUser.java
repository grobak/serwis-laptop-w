package dataproviders;

import models.Employee;

public final class LoggedUser {
	private static Employee user;
	
	LoggedUser(Employee pracownik){
		user= new Employee(pracownik.getLogin(), pracownik.getHaslo(), pracownik.getImie(), pracownik.getTelefon(), pracownik.getPositionId());
	}
	
	public static boolean isAdmin(){
		return user.isAdmin();
	}
	
	public static int getStanowisko(){
		return user.getPositionId();
	}
	
	public static String getImie(){
		return user.getImie();
	}
	
	public static String getLogin(){
		return user.getLogin();
	}
	
	public static Employee getUser(){
		return user;
	}
}

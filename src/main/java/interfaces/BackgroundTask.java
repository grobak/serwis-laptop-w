/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import javax.swing.SwingWorker;
import progressbar.ProgressController;

/**
 *
 * @author Grzesiek
 */
public interface BackgroundTask extends ProgressController {

    default void backgroundTask(Runnable doInBackground) {
        backgroundTask(doInBackground, null);
    }

    default void backgroundTask(Runnable backgroundDelegate, Runnable doWhenDone) {

        new SwingWorker() {
            @Override
            protected Void doInBackground() throws Exception {
                startProgressBar();
                backgroundDelegate.run();
                return null;
            }

            @Override
            protected void done() {               
                if (doWhenDone != null) {
                    doWhenDone.run();
                }
                stopProgressBar();
            }
        }.execute();
    }

}

package interfaces;

public interface Form<T> {
	void clear();
	T generate();
}

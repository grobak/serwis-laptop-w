package components;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class ButtonsAdd extends JPanel {

    private JButton btnCofnij = new JButton("Cofnij");
    private JButton btnZapisz = new JButton("Zapisz");

    @SuppressWarnings("PMD.VariableNamingConventions")
    public ButtonsAdd() {
        setBorder(new LineBorder(new Color(0, 0, 0)));
        //setMaximumSize(new Dimension(450, 40));

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{64, 70, 82, 0};
        gridBagLayout.rowHeights = new int[]{35, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.anchor = GridBagConstraints.EAST;
        gbc_panel_1.fill = GridBagConstraints.VERTICAL;
        gbc_panel_1.gridx = 2;
        gbc_panel_1.gridy = 0;
        JPanel panelModify = new JPanel();
        add(panelModify, gbc_panel_1);
        panelModify.add(btnCofnij);

        Component horizontalStrut = Box.createHorizontalStrut(20);
        panelModify.add(horizontalStrut);
        panelModify.add(btnZapisz);

    }

    public JButton getBtnCofnij() {
        return btnCofnij;
    }

    public void setBtnCofnij(JButton btnCofnij) {
        this.btnCofnij = btnCofnij;
    }

    public JButton getBtnZapisz() {
        return btnZapisz;
    }

    public void setBtnZapisz(JButton btnZapisz) {
        this.btnZapisz = btnZapisz;
    }

}

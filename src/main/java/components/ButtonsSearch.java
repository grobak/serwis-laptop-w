package components;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

public class ButtonsSearch extends Buttons {

    private final JTextFieldWthPopup txtWyszukaj = new JTextFieldWthPopup();
    private final JButton btnWyszukaj = new JButton("Wyszukaj");

    @SuppressWarnings("PMD.VariableNamingConventions")
    public ButtonsSearch() {
        setBorder(new LineBorder(new Color(0, 0, 0)));
        FlowLayout flowLayout = (FlowLayout) getLayout();
        flowLayout.setAlignment(FlowLayout.RIGHT);

        add(txtWyszukaj);
        txtWyszukaj.setColumns(10);

        Component horizontalStrut_2 = Box.createHorizontalStrut(20);
        add(horizontalStrut_2);

        add(btnWyszukaj);
    }

    public JTextFieldWthPopup getTxtWyszukaj() {
        return txtWyszukaj;
    }

    public JButton getBtnWyszukaj() {
        return btnWyszukaj;
    }

    public String getSearchInput() {
        return txtWyszukaj.getText().trim();
    }
}

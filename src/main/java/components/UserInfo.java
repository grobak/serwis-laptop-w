package components;

import dataproviders.LoggedUser;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import repositories.EmployeeRepository;

public class UserInfo extends JPanel {

    private final JButton btnLogout;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public UserInfo() {
        setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        setMinimumSize(new Dimension(320, 30));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{58, 1, 58, 1, 71, 0};
        gridBagLayout.rowHeights = new int[]{20, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        JLabel lblUytkownik = new JLabel("Użytkownik:");
        GridBagConstraints gbc_lblUytkownik = new GridBagConstraints();
        gbc_lblUytkownik.insets = new Insets(0, 0, 0, 5);
        gbc_lblUytkownik.gridx = 0;
        gbc_lblUytkownik.gridy = 0;
        add(lblUytkownik, gbc_lblUytkownik);

        JLabel userName = new JLabel("nazwaUsera");
        userName.setText(LoggedUser.getImie());

        GridBagConstraints gbc_userName = new GridBagConstraints();
        gbc_userName.insets = new Insets(0, 0, 0, 5);
        gbc_userName.gridx = 1;
        gbc_userName.gridy = 0;
        add(userName, gbc_userName);

        JLabel lblStanowisko = new JLabel("Stanowisko:");
        GridBagConstraints gbc_lblStanowisko = new GridBagConstraints();
        gbc_lblStanowisko.insets = new Insets(0, 0, 0, 5);
        gbc_lblStanowisko.gridx = 2;
        gbc_lblStanowisko.gridy = 0;
        add(lblStanowisko, gbc_lblStanowisko);

        JLabel privileges = new JLabel("Uprawnienia");
        privileges.setText(new EmployeeRepository().getPositionName(LoggedUser.getStanowisko()));
        GridBagConstraints gbc_privileges = new GridBagConstraints();
        gbc_privileges.insets = new Insets(0, 0, 0, 5);
        gbc_privileges.gridx = 3;
        gbc_privileges.gridy = 0;
        add(privileges, gbc_privileges);

        btnLogout = new JButton("Wyloguj");
        GridBagConstraints gbc_btnWyloguj = new GridBagConstraints();
        gbc_btnWyloguj.gridx = 4;
        gbc_btnWyloguj.gridy = 0;
        add(btnLogout, gbc_btnWyloguj);
    }
  
    public void addLogoutListener(ActionListener listener) {
        btnLogout.addActionListener(listener);
    }

}

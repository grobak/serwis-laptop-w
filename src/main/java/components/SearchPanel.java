package components;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import models.ForcedListSelectionModel;

public class SearchPanel extends JPanel {

    private JTable searchResultTable;
    private TopSearchPanel topSearchPanel;

    public SearchPanel() {
        setView();
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        searchResultTable = new JTable();
        topSearchPanel = new TopSearchPanel();

        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        add(topSearchPanel, gbc_panel);

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 1;
        add(scrollPane, gbc_scrollPane);

        scrollPane.setViewportView(searchResultTable);
        searchResultTable.setSelectionModel(new ForcedListSelectionModel());
    }

    public JTable getSearchResultTable() {
        return searchResultTable;
    }

// --Commented out by Inspection START (12-04-2015 23:25):
//	public TopSearchPanel getTopSearchPanel() {
//		return topSearchPanel;
//	}
// --Commented out by Inspection STOP (12-04-2015 23:25)
// --Commented out by Inspection START (12-04-2015 23:25):
//	public void setTopSearchPanel(TopSearchPanel topSearchPanel) {
//		this.topSearchPanel = topSearchPanel;
//	}
// --Commented out by Inspection STOP (12-04-2015 23:25)
    public JTextFieldWthPopup getTxtWyszukaj() {
        return topSearchPanel.getTxtWyszukaj();
    }

    public JButton getBtnWyszukaj() {
        return topSearchPanel.getBtnWyszukaj();
    }

}

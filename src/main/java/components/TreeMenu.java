package components;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import models.RepairStatus;
import view.ContentPanel;
import view.MojeNaprawy;
import view.PanelDodajPracownik;
import view.PanelDodajSprzet;
import view.StatusSprzet;
import view.Statystyki;
import view.WyszukajPracownika;
import view.WyszukajSprzet;
import view.forms.KlientForm;
import view.forms.SearchForm;
import controllers.DodajKlient;
import controllers.WyszukajKlient;
import dataproviders.LoggedUser;

public class TreeMenu extends JTree{

	public TreeMenu() {
		setPreferredSize(new Dimension(150, 440));
		setMinimumSize(getPreferredSize());

		setRootVisible(false);

		DefaultTreeCellRenderer renderer = 	new DefaultTreeCellRenderer();
		renderer.setOpenIcon(null);
		renderer.setLeafIcon(null);
		setCellRenderer(renderer);

		setJTreeModel();

		expandRows();
	}

	private void expandRows(){
		for(int i=0;i<getRowCount();i++)
			expandRow(i);
	}

	private void setJTreeModel(){
		DefaultTreeModel model = new DefaultTreeModel(
				new DefaultMutableTreeNode("Serwis") {
					{
						DefaultMutableTreeNode node_1;
						node_1 = new DefaultMutableTreeNode("Klienci");
						node_1.add(new DefaultMutableTreeNode("Dodaj klienta"));
						node_1.add(new DefaultMutableTreeNode("Wyszukaj klienta"));
						this.add(node_1);
						node_1 = new DefaultMutableTreeNode("Magazyn");
						node_1.add(new DefaultMutableTreeNode("Dodaj sprzęt"));
						node_1.add(new DefaultMutableTreeNode("Wyszukaj sprzęt"));
						this.add(node_1);
						node_1 = new DefaultMutableTreeNode("Laptopy");
						node_1.add(new DefaultMutableTreeNode("Przyjęte"));
						node_1.add(new DefaultMutableTreeNode("Zdemontowane"));
						node_1.add(new DefaultMutableTreeNode("W trakcie napraw"));
						node_1.add(new DefaultMutableTreeNode("Przekazane do testów"));
						node_1.add(new DefaultMutableTreeNode("Testowane"));
						node_1.add(new DefaultMutableTreeNode("Naprawione"));
						node_1.add(new DefaultMutableTreeNode("Wydane"));
						this.add(node_1);
						node_1 = new DefaultMutableTreeNode("Naprawy");
						node_1.add(new DefaultMutableTreeNode("Moje naprawy"));
						this.add(node_1);
						if(LoggedUser.isAdmin()){
							node_1 = new DefaultMutableTreeNode("Pracownicy");
							node_1.add(new DefaultMutableTreeNode("Dodaj pracownika"));
							node_1.add(new DefaultMutableTreeNode("Wyszukaj pracownika"));
							this.add(node_1);
						}
						node_1 = new DefaultMutableTreeNode("Statystyki");
						node_1.add(new DefaultMutableTreeNode("Wykonane czynności"));
						//node_1.add(new DefaultMutableTreeNode("Test"));
						this.add(node_1);
					}
				}
		);

		setModel(model);
	}

	public String getSelectedMenuName(){
		return getLastSelectedPathComponent().toString();
	}

	public JPanel getSelectedPanel()
	{
		ContentPanel content = new ContentPanel();
		switch(getSelectedMenuName()){
			case "Dodaj klienta":
				content.setContent(new KlientForm());
				new DodajKlient(content);
				return content;
			case "Wyszukaj klienta":
				content.setContent(new SearchForm());
				new WyszukajKlient(content);
				return content;
			case "Dodaj sprzęt":
				return new PanelDodajSprzet();
			case "Wyszukaj sprzęt":
				return new WyszukajSprzet();
			case "Przyjęte":
				return new StatusSprzet(RepairStatus.PRZYJETY);
			case "Zdemontowane":
				return new StatusSprzet(RepairStatus.DEMONTAZ);
			case "W trakcie napraw":
				return new StatusSprzet(RepairStatus.W_NAPRAWIE);
			case "Przekazane do testów":
				return new StatusSprzet(RepairStatus.DO_TESTOW);
			case "Testowane":
				return new StatusSprzet(RepairStatus.PODCZAS_TESTOW);
			case "Naprawione":
				return new StatusSprzet(RepairStatus.NAPRAWIONY);
			case "Wydane":
				return new StatusSprzet(RepairStatus.WYDANY);
			case "Moje naprawy":
				return new MojeNaprawy();
			case "Dodaj pracownika":
				return new PanelDodajPracownik();
			case "Wyszukaj pracownika":
				return new WyszukajPracownika();
			case "Wykonane czynności":
				return new Statystyki();
			default:
				return new JPanel();
		}
	}
}
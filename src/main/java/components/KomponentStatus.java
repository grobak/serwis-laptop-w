package components;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import models.Computer;
import models.ForcedListSelectionModel;
import models.Naprawa;
import repositories.RepairRepository;

public class KomponentStatus extends JPanel {
	private JTable table = new JTable();
	private Computer sprzet;
	private List<String> statusNames = new ArrayList<>();

	public KomponentStatus(Computer sprzet) {
		this.sprzet = sprzet;
		setLayout(new GridLayout(1, 0, 0, 0));

		statusNames = new RepairRepository().getRepairStatusNames();

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane);

		scrollPane.setViewportView(table);		
		table.setSelectionModel(new ForcedListSelectionModel());
		if(sprzet.isInitialized())
			table.setModel(getTableModel());
	}

	private DefaultTableModel getTableModel(){
		List<Naprawa> statusy = new RepairRepository().getRepairs( sprzet.getId() );
		DefaultTableModel model = new DefaultTableModel(){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};	
		Object[] nazwyKolumn = {"Kto", "Status", "Data"}; 
		model.setColumnIdentifiers(nazwyKolumn);
		for(Naprawa status : statusy)
		{
			model.insertRow( 0, new Object[]{status.getKto(), statusNames.get( status.getIdStatus() ), status.getData().substring( 0, 16 )} );
		}

		return model;
	}

	public void refresh(){
		table.setModel(getTableModel());
	}
}

package components;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class JTextFieldWthPopup extends JTextField {

	public JTextFieldWthPopup() {
		super();
		setComponentPopupMenu(new ClipboardPopUpMenu(this));
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				requestFocus();					
			}
			
			@Override
			public void mouseExited(MouseEvent e) {		
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
	}

	public JTextFieldWthPopup(Document arg0, String arg1, int arg2) {
		super(arg0, arg1, arg2);
		setComponentPopupMenu(new ClipboardPopUpMenu(this));
	}

	public JTextFieldWthPopup(int arg0) {
		super(arg0);
		setComponentPopupMenu(new ClipboardPopUpMenu(this));
	}

	public JTextFieldWthPopup(String arg0, int arg1) {
		super(arg0, arg1);
		setComponentPopupMenu(new ClipboardPopUpMenu(this));
	}

	public JTextFieldWthPopup(String arg0) {
		super(arg0);
		setComponentPopupMenu(new ClipboardPopUpMenu(this));
	}
	

}

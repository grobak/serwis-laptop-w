package components;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TestComponent extends JPanel implements Cloneable {

    private int idLaptopa;
    private final KomponentStan hdd;
    private final KomponentStan dvd;
    private final KomponentStan bateria;
    private final KomponentStan zasilacz;
    private final KomponentStan ladowanie;
    private final KomponentStan bluetooth;
    private final KomponentStan lan;
    private final KomponentStan wifi;
    private final KomponentStan glosniki;
    private final KomponentStan sluchawki;
    private final KomponentStan mikrofon;
    private final KomponentStan klawiatura;
    private final KomponentStan kamera;
    private final KomponentStan touchpad;
    private final KomponentStan usb;
    private final KomponentStan monitorZew;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public TestComponent() {
        this.idLaptopa = 0;
        setPreferredSize(new Dimension(468, 261));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{56, 150, 0, 66, 0, 0};
        gridBagLayout.rowHeights = new int[]{26, 25, 14, 14, 14, 14, 14, 14, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        JLabel lblHdd = new JLabel("HDD: ");
        GridBagConstraints gbc_lblHdd = new GridBagConstraints();
        gbc_lblHdd.insets = new Insets(0, 0, 5, 5);
        gbc_lblHdd.gridx = 0;
        gbc_lblHdd.gridy = 0;
        add(lblHdd, gbc_lblHdd);

        hdd = new KomponentStan();
        GridBagConstraints gbc_komponentStan = new GridBagConstraints();
        gbc_komponentStan.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan.fill = GridBagConstraints.BOTH;
        gbc_komponentStan.gridx = 1;
        gbc_komponentStan.gridy = 0;
        add(hdd, gbc_komponentStan);

        JLabel lblGoniki = new JLabel("Głośniki:");
        GridBagConstraints gbc_lblGoniki = new GridBagConstraints();
        gbc_lblGoniki.insets = new Insets(0, 0, 5, 5);
        gbc_lblGoniki.gridx = 3;
        gbc_lblGoniki.gridy = 0;
        add(lblGoniki, gbc_lblGoniki);

        glosniki = new KomponentStan();
        GridBagConstraints gbc_komponentStan_8 = new GridBagConstraints();
        gbc_komponentStan_8.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_8.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_8.gridx = 4;
        gbc_komponentStan_8.gridy = 0;
        add(glosniki, gbc_komponentStan_8);

        JLabel lblDvd = new JLabel("DVD: ");
        GridBagConstraints gbc_lblDvd = new GridBagConstraints();
        gbc_lblDvd.insets = new Insets(0, 0, 5, 5);
        gbc_lblDvd.gridx = 0;
        gbc_lblDvd.gridy = 1;
        add(lblDvd, gbc_lblDvd);

        dvd = new KomponentStan();
        GridBagConstraints gbc_komponentStan_1 = new GridBagConstraints();
        gbc_komponentStan_1.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan_1.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_1.gridx = 1;
        gbc_komponentStan_1.gridy = 1;
        add(dvd, gbc_komponentStan_1);

        JLabel lblSuchawki = new JLabel("Słuchawki:");
        GridBagConstraints gbc_lblSuchawki = new GridBagConstraints();
        gbc_lblSuchawki.insets = new Insets(0, 0, 5, 5);
        gbc_lblSuchawki.gridx = 3;
        gbc_lblSuchawki.gridy = 1;
        add(lblSuchawki, gbc_lblSuchawki);

        sluchawki = new KomponentStan();
        GridBagConstraints gbc_komponentStan_9 = new GridBagConstraints();
        gbc_komponentStan_9.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_9.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_9.gridx = 4;
        gbc_komponentStan_9.gridy = 1;
        add(sluchawki, gbc_komponentStan_9);

        JLabel lblBateria = new JLabel("Bateria:");
        GridBagConstraints gbc_lblBateria = new GridBagConstraints();
        gbc_lblBateria.insets = new Insets(0, 0, 5, 5);
        gbc_lblBateria.gridx = 0;
        gbc_lblBateria.gridy = 2;
        add(lblBateria, gbc_lblBateria);

        bateria = new KomponentStan();
        GridBagConstraints gbc_komponentStan_2 = new GridBagConstraints();
        gbc_komponentStan_2.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan_2.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_2.gridx = 1;
        gbc_komponentStan_2.gridy = 2;
        add(bateria, gbc_komponentStan_2);

        JLabel lblMikrofon = new JLabel("Mikrofon:");
        GridBagConstraints gbc_lblMikrofon = new GridBagConstraints();
        gbc_lblMikrofon.insets = new Insets(0, 0, 5, 5);
        gbc_lblMikrofon.gridx = 3;
        gbc_lblMikrofon.gridy = 2;
        add(lblMikrofon, gbc_lblMikrofon);

        mikrofon = new KomponentStan();
        GridBagConstraints gbc_komponentStan_10 = new GridBagConstraints();
        gbc_komponentStan_10.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_10.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_10.gridx = 4;
        gbc_komponentStan_10.gridy = 2;
        add(mikrofon, gbc_komponentStan_10);

        JLabel lblZasilacz = new JLabel("Zasilacz:");
        GridBagConstraints gbc_lblZasilacz = new GridBagConstraints();
        gbc_lblZasilacz.insets = new Insets(0, 0, 5, 5);
        gbc_lblZasilacz.gridx = 0;
        gbc_lblZasilacz.gridy = 3;
        add(lblZasilacz, gbc_lblZasilacz);

        zasilacz = new KomponentStan();
        GridBagConstraints gbc_komponentStan_3 = new GridBagConstraints();
        gbc_komponentStan_3.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan_3.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_3.gridx = 1;
        gbc_komponentStan_3.gridy = 3;
        add(zasilacz, gbc_komponentStan_3);

        JLabel lblKlawiatura = new JLabel("Klawiatura:");
        GridBagConstraints gbc_lblKlawiatura = new GridBagConstraints();
        gbc_lblKlawiatura.insets = new Insets(0, 0, 5, 5);
        gbc_lblKlawiatura.gridx = 3;
        gbc_lblKlawiatura.gridy = 3;
        add(lblKlawiatura, gbc_lblKlawiatura);

        klawiatura = new KomponentStan();
        GridBagConstraints gbc_komponentStan_11 = new GridBagConstraints();
        gbc_komponentStan_11.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_11.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_11.gridx = 4;
        gbc_komponentStan_11.gridy = 3;
        add(klawiatura, gbc_komponentStan_11);

        JLabel lbladowanie = new JLabel("Ładowanie:");
        GridBagConstraints gbc_lbladowanie = new GridBagConstraints();
        gbc_lbladowanie.insets = new Insets(0, 0, 5, 5);
        gbc_lbladowanie.gridx = 0;
        gbc_lbladowanie.gridy = 4;
        add(lbladowanie, gbc_lbladowanie);

        ladowanie = new KomponentStan();
        GridBagConstraints gbc_komponentStan_4 = new GridBagConstraints();
        gbc_komponentStan_4.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan_4.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_4.gridx = 1;
        gbc_komponentStan_4.gridy = 4;
        add(ladowanie, gbc_komponentStan_4);

        JLabel lblKamera = new JLabel("Kamera:");
        GridBagConstraints gbc_lblKamera = new GridBagConstraints();
        gbc_lblKamera.insets = new Insets(0, 0, 5, 5);
        gbc_lblKamera.gridx = 3;
        gbc_lblKamera.gridy = 4;
        add(lblKamera, gbc_lblKamera);

        kamera = new KomponentStan();
        GridBagConstraints gbc_komponentStan_12 = new GridBagConstraints();
        gbc_komponentStan_12.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_12.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_12.gridx = 4;
        gbc_komponentStan_12.gridy = 4;
        add(kamera, gbc_komponentStan_12);

        JLabel lblBluetooth = new JLabel("Bluetooth:");
        GridBagConstraints gbc_lblBluetooth = new GridBagConstraints();
        gbc_lblBluetooth.insets = new Insets(0, 0, 5, 5);
        gbc_lblBluetooth.gridx = 0;
        gbc_lblBluetooth.gridy = 5;
        add(lblBluetooth, gbc_lblBluetooth);

        bluetooth = new KomponentStan();
        GridBagConstraints gbc_komponentStan_5 = new GridBagConstraints();
        gbc_komponentStan_5.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan_5.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_5.gridx = 1;
        gbc_komponentStan_5.gridy = 5;
        add(bluetooth, gbc_komponentStan_5);

        JLabel lblTouchpad = new JLabel("Touchpad:");
        GridBagConstraints gbc_lblTouchpad = new GridBagConstraints();
        gbc_lblTouchpad.insets = new Insets(0, 0, 5, 5);
        gbc_lblTouchpad.gridx = 3;
        gbc_lblTouchpad.gridy = 5;
        add(lblTouchpad, gbc_lblTouchpad);

        touchpad = new KomponentStan();
        GridBagConstraints gbc_komponentStan_13 = new GridBagConstraints();
        gbc_komponentStan_13.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_13.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_13.gridx = 4;
        gbc_komponentStan_13.gridy = 5;
        add(touchpad, gbc_komponentStan_13);

        JLabel lblLan = new JLabel("LAN:");
        GridBagConstraints gbc_lblLan = new GridBagConstraints();
        gbc_lblLan.insets = new Insets(0, 0, 5, 5);
        gbc_lblLan.gridx = 0;
        gbc_lblLan.gridy = 6;
        add(lblLan, gbc_lblLan);

        lan = new KomponentStan();
        GridBagConstraints gbc_komponentStan_6 = new GridBagConstraints();
        gbc_komponentStan_6.insets = new Insets(0, 0, 5, 5);
        gbc_komponentStan_6.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_6.gridx = 1;
        gbc_komponentStan_6.gridy = 6;
        add(lan, gbc_komponentStan_6);

        JLabel label = new JLabel("USB:");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.gridx = 3;
        gbc_label.gridy = 6;
        add(label, gbc_label);

        usb = new KomponentStan();
        GridBagConstraints gbc_komponentStan_14 = new GridBagConstraints();
        gbc_komponentStan_14.insets = new Insets(0, 0, 5, 0);
        gbc_komponentStan_14.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_14.gridx = 4;
        gbc_komponentStan_14.gridy = 6;
        add(usb, gbc_komponentStan_14);

        JLabel lblWifi = new JLabel("WiFi:");
        GridBagConstraints gbc_lblWifi = new GridBagConstraints();
        gbc_lblWifi.insets = new Insets(0, 0, 0, 5);
        gbc_lblWifi.gridx = 0;
        gbc_lblWifi.gridy = 7;
        add(lblWifi, gbc_lblWifi);

        wifi = new KomponentStan();
        GridBagConstraints gbc_komponentStan_7 = new GridBagConstraints();
        gbc_komponentStan_7.insets = new Insets(0, 0, 0, 5);
        gbc_komponentStan_7.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_7.gridx = 1;
        gbc_komponentStan_7.gridy = 7;
        add(wifi, gbc_komponentStan_7);

        JLabel lblMonitorZew = new JLabel("Monitor zew.:");
        GridBagConstraints gbc_lblMonitorZew = new GridBagConstraints();
        gbc_lblMonitorZew.insets = new Insets(0, 0, 0, 5);
        gbc_lblMonitorZew.gridx = 3;
        gbc_lblMonitorZew.gridy = 7;
        add(lblMonitorZew, gbc_lblMonitorZew);

        monitorZew = new KomponentStan();
        GridBagConstraints gbc_komponentStan_15 = new GridBagConstraints();
        gbc_komponentStan_15.fill = GridBagConstraints.BOTH;
        gbc_komponentStan_15.gridx = 4;
        gbc_komponentStan_15.gridy = 7;
        add(monitorZew, gbc_komponentStan_15);

    }

    public void setIdLaptopa(int idLaptopa) {
        this.idLaptopa = idLaptopa;
    }

    public KomponentStan getHdd() {
        return hdd;
    }

    public KomponentStan getDvd() {
        return dvd;
    }

    public KomponentStan getBateria() {
        return bateria;
    }

    public KomponentStan getZasilacz() {
        return zasilacz;
    }

    public KomponentStan getLadowanie() {
        return ladowanie;
    }

    public KomponentStan getBluetooth() {
        return bluetooth;
    }

    public KomponentStan getLan() {
        return lan;
    }

    public KomponentStan getWifi() {
        return wifi;
    }

    public KomponentStan getGlosniki() {
        return glosniki;
    }

    public KomponentStan getSluchawki() {
        return sluchawki;
    }

    public KomponentStan getMikrofon() {
        return mikrofon;
    }

    public KomponentStan getKlawiatura() {
        return klawiatura;
    }

    public KomponentStan getKamera() {
        return kamera;
    }

    public KomponentStan getTouchpad() {
        return touchpad;
    }

    public KomponentStan getUsb() {
        return usb;
    }

    public KomponentStan getMonitorZew() {
        return monitorZew;
    }

    @Override
    public String toString() {
        return "KomponentTesty [idLaptopa=" + idLaptopa + ", hdd=" + hdd
                + ", dvd=" + dvd + ", bateria=" + bateria + ", zasilacz="
                + zasilacz + ", ladowanie=" + ladowanie + ", bluetooth="
                + bluetooth + ", lan=" + lan + ", wifi=" + wifi + ", glosniki="
                + glosniki + ", sluchawki=" + sluchawki + ", mikrofon="
                + mikrofon + ", klawiatura=" + klawiatura + ", kamera="
                + kamera + ", touchpad=" + touchpad + ", usb=" + usb
                + ", monitorZew=" + monitorZew + "]";
    }

    public boolean equals(TestComponent testy) {
        return this.getHdd().getSelected() == testy.getHdd().getSelected()
                && this.getDvd().getSelected() == testy.getDvd().getSelected()
                && this.getBateria().getSelected() == testy.getBateria().getSelected()
                && this.getZasilacz().getSelected() == testy.getZasilacz().getSelected()
                && this.getLadowanie().getSelected() == testy.getLadowanie().getSelected()
                && this.getBluetooth().getSelected() == testy.getBluetooth().getSelected()
                && this.getGlosniki().getSelected() == testy.getGlosniki().getSelected()
                && this.getSluchawki().getSelected() == testy.getSluchawki().getSelected()
                && this.getMikrofon().getSelected() == testy.getMikrofon().getSelected()
                && this.getKlawiatura().getSelected() == testy.getKlawiatura().getSelected()
                && this.getKamera().getSelected() == testy.getKamera().getSelected()
                && this.getTouchpad().getSelected() == testy.getTouchpad().getSelected()
                && this.getUsb().getSelected() == testy.getUsb().getSelected()
                && this.getLan().getSelected() == testy.getLan().getSelected()
                && this.getWifi().getSelected() == testy.getWifi().getSelected()
                && this.getMonitorZew().getSelected() == testy.getMonitorZew().getSelected();
    }

}

package components;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class KomponentRodzajNaprawy extends JPanel {

    private final JTextFieldWthPopup txtRodzaj;
    private final JComboBox<String> comboRodzajNaprawy;
    private final JCheckBox chckbxInny;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public KomponentRodzajNaprawy() {
        setMinimumSize(new Dimension(100, 20));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        comboRodzajNaprawy = new JComboBox<>();
        comboRodzajNaprawy.setModel(new DefaultComboBoxModel<>(new String[]{"Naprawa", "Wymiana płyty", "Konserwacja", "Reklamacja"}));
        GridBagConstraints gbc_comboBox = new GridBagConstraints();
        gbc_comboBox.insets = new Insets(0, 0, 0, 5);
        gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox.gridx = 0;
        gbc_comboBox.gridy = 0;
        add(comboRodzajNaprawy, gbc_comboBox);

        chckbxInny = new JCheckBox("Inny:");
        chckbxInny.setHorizontalTextPosition(SwingConstants.LEFT);
        GridBagConstraints gbc_checkBox = new GridBagConstraints();
        gbc_checkBox.insets = new Insets(0, 0, 0, 5);
        gbc_checkBox.gridx = 2;
        gbc_checkBox.gridy = 0;
        add(chckbxInny, gbc_checkBox);

        txtRodzaj = new JTextFieldWthPopup();
        txtRodzaj.setEnabled(false);
        txtRodzaj.setText("Określ inny rodzaj");
        txtRodzaj.setColumns(10);
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.gridx = 3;
        gbc_textField.gridy = 0;
        add(txtRodzaj, gbc_textField);

        chckbxInny.addItemListener(e -> allowAnotherRepairDescription(chckbxInny.isSelected()));
    }

    private void allowAnotherRepairDescription(boolean flag) {
        txtRodzaj.setEnabled(flag);
        txtRodzaj.setText(flag ? "" : "Określ inny rodzaj");
        comboRodzajNaprawy.setEnabled(!flag);
    }

    public String getRodzajNaprawy() {
        if (comboRodzajNaprawy.isEnabled()) {
            return comboRodzajNaprawy.getSelectedItem().toString();
        } else {
            return txtRodzaj.getText();
        }
    }

    public void setRodzajNaprawy(int nrCombo) {
        chckbxInny.setSelected(false);
        comboRodzajNaprawy.setSelectedIndex(nrCombo);
    }

    public void setRodzajNaprawy(String rodzaj) {
        chckbxInny.setSelected(true);
        txtRodzaj.setText(rodzaj);
    }

    public JTextFieldWthPopup getTxtRodzaj() {
        return txtRodzaj;
    }
}

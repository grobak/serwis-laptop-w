package components;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import models.Equipment;

public class KomponentWyposazenie extends JPanel {

    private final JCheckBox chckbxHdd;
    private final JCheckBox chckbxBateria;
    private final JCheckBox chckbxTorba;
    private final JCheckBox chckbxCddvd;
    private final JCheckBox chckbxZasilacz;
    private final JCheckBox chckbxKarton;
    private final JCheckBox chckbxPrzewdAc;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public KomponentWyposazenie() {
        Border border = BorderFactory.createTitledBorder(new LineBorder(new Color(0, 0, 0)), "Wyposażenie:");
        setBorder(border);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        chckbxHdd = new JCheckBox("HDD");
        GridBagConstraints gbc_chckbxHdd = new GridBagConstraints();
        gbc_chckbxHdd.anchor = GridBagConstraints.WEST;
        gbc_chckbxHdd.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxHdd.fill = GridBagConstraints.VERTICAL;
        gbc_chckbxHdd.gridx = 0;
        gbc_chckbxHdd.gridy = 0;
        add(chckbxHdd, gbc_chckbxHdd);

        chckbxBateria = new JCheckBox("Bateria");
        GridBagConstraints gbc_chckbxBateria = new GridBagConstraints();
        gbc_chckbxBateria.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxBateria.anchor = GridBagConstraints.WEST;
        gbc_chckbxBateria.gridx = 1;
        gbc_chckbxBateria.gridy = 0;
        add(chckbxBateria, gbc_chckbxBateria);

        chckbxTorba = new JCheckBox("Torba");
        GridBagConstraints gbc_chckbxTorba = new GridBagConstraints();
        gbc_chckbxTorba.anchor = GridBagConstraints.WEST;
        gbc_chckbxTorba.insets = new Insets(0, 0, 5, 0);
        gbc_chckbxTorba.gridx = 2;
        gbc_chckbxTorba.gridy = 0;
        add(chckbxTorba, gbc_chckbxTorba);

        chckbxCddvd = new JCheckBox("CD/DVD");
        GridBagConstraints gbc_chckbxCddvd = new GridBagConstraints();
        gbc_chckbxCddvd.anchor = GridBagConstraints.WEST;
        gbc_chckbxCddvd.fill = GridBagConstraints.VERTICAL;
        gbc_chckbxCddvd.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxCddvd.gridx = 0;
        gbc_chckbxCddvd.gridy = 1;
        add(chckbxCddvd, gbc_chckbxCddvd);

        chckbxZasilacz = new JCheckBox("Zasilacz");
        GridBagConstraints gbc_chckbxZasilacz = new GridBagConstraints();
        gbc_chckbxZasilacz.anchor = GridBagConstraints.WEST;
        gbc_chckbxZasilacz.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxZasilacz.gridx = 1;
        gbc_chckbxZasilacz.gridy = 1;
        add(chckbxZasilacz, gbc_chckbxZasilacz);

        chckbxKarton = new JCheckBox("Karton");
        GridBagConstraints gbc_chckbxKarton = new GridBagConstraints();
        gbc_chckbxKarton.anchor = GridBagConstraints.WEST;
        gbc_chckbxKarton.insets = new Insets(0, 0, 5, 0);
        gbc_chckbxKarton.gridx = 2;
        gbc_chckbxKarton.gridy = 1;
        add(chckbxKarton, gbc_chckbxKarton);

        chckbxPrzewdAc = new JCheckBox("Przew\u00F3d AC");
        GridBagConstraints gbc_chckbxPrzewdAc = new GridBagConstraints();
        gbc_chckbxPrzewdAc.anchor = GridBagConstraints.WEST;
        gbc_chckbxPrzewdAc.insets = new Insets(0, 0, 0, 5);
        gbc_chckbxPrzewdAc.gridx = 1;
        gbc_chckbxPrzewdAc.gridy = 2;
        add(chckbxPrzewdAc, gbc_chckbxPrzewdAc);

    }

    public boolean getChckbxHdd() {
        return chckbxHdd.isSelected();
    }

    public void setChckbxHdd(boolean b) {
        chckbxHdd.setSelected(b);
    }

    public boolean getChckbxBateria() {
        return chckbxBateria.isSelected();
    }

    public void setChckbxBateria(boolean b) {
        chckbxBateria.setSelected(b);
    }

    public boolean getChckbxTorba() {
        return chckbxTorba.isSelected();
    }

    public void setChckbxTorba(boolean b) {
        chckbxTorba.setSelected(b);
    }

    public boolean getChckbxCddvd() {
        return chckbxCddvd.isSelected();
    }

    public void setChckbxCddvd(boolean b) {
        chckbxCddvd.setSelected(b);
    }

    public boolean getChckbxZasilacz() {
        return chckbxZasilacz.isSelected();
    }

    public void setChckbxZasilacz(boolean b) {
        chckbxZasilacz.setSelected(b);
    }

    public boolean getChckbxKarton() {
        return chckbxKarton.isSelected();
    }

    public void setChckbxKarton(boolean b) {
        chckbxKarton.setSelected(b);
    }

    public boolean getChckbxPrzewdAc() {
        return chckbxPrzewdAc.isSelected();
    }

    public void setChckbxPrzewdAc(boolean b) {
        chckbxPrzewdAc.setSelected(b);
    }

    public void wyczysc() {
        setChckbxPrzewdAc(false);
        setChckbxKarton(false);
        setChckbxZasilacz(false);
        setChckbxCddvd(false);
        setChckbxTorba(false);
        setChckbxBateria(false);
        setChckbxHdd(false);
    }

    public void setWyposazenie(Equipment wyposaz) {
        setChckbxHdd(wyposaz.isHDD());
        setChckbxCddvd(wyposaz.isDVD());
        setChckbxBateria(wyposaz.isBateria());
        setChckbxZasilacz(wyposaz.isZasilacz());
        setChckbxPrzewdAc(wyposaz.isKabelAC());
        setChckbxTorba(wyposaz.isTorba());
        setChckbxKarton(wyposaz.isKarton());
    }

    public void setEnabled(boolean flag) {
        chckbxBateria.setEnabled(flag);
        chckbxCddvd.setEnabled(flag);
        chckbxHdd.setEnabled(flag);
        chckbxKarton.setEnabled(flag);
        chckbxPrzewdAc.setEnabled(flag);
        chckbxTorba.setEnabled(flag);
        chckbxZasilacz.setEnabled(flag);
    }

}

package components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.LineBorder;

public class KomponentStan extends JPanel {

    private JRadioButton radioOk = new JRadioButton("OK");
    private JRadioButton radioUszk = new JRadioButton("Uszk.");
    private JRadioButton radioBrak = new JRadioButton("Brak");
    private ButtonGroup grupa = new ButtonGroup();
    private boolean modified = false;

    public KomponentStan() {
        setView();

        radioOk.addItemListener(e -> {
            setBorder(new LineBorder(new Color(0, 0, 0)));
            modified = true;
        });

        radioUszk.addItemListener(e -> {
            setBorder(new LineBorder(new Color(255, 0, 0), 2));
            modified = true;
        });

        radioBrak.addItemListener(e -> {
            setBorder(new LineBorder(new Color(0, 0, 0)));
            modified = true;
        });

    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setView() {
        setBorder(new LineBorder(new Color(255, 255, 255), 2));
        setPreferredSize(new Dimension(150, 25));
        setMinimumSize(getPreferredSize());
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{20, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_RadioOk = new GridBagConstraints();
        gbc_RadioOk.insets = new Insets(0, 0, 0, 5);
        gbc_RadioOk.gridx = 0;
        gbc_RadioOk.gridy = 0;
        add(radioOk, gbc_RadioOk);

        GridBagConstraints gbc_RadioUszk = new GridBagConstraints();
        gbc_RadioUszk.insets = new Insets(0, 0, 0, 5);
        gbc_RadioUszk.gridx = 1;
        gbc_RadioUszk.gridy = 0;
        add(radioUszk, gbc_RadioUszk);

        GridBagConstraints gbc_RadioBrak = new GridBagConstraints();
        gbc_RadioBrak.gridx = 2;
        gbc_RadioBrak.gridy = 0;
        add(radioBrak, gbc_RadioBrak);

        grupa.add(radioOk);
        grupa.add(radioUszk);
        grupa.add(radioBrak);
    }

    public JRadioButton getRadioOk() {
        return radioOk;
    }

    public JRadioButton getRadioUszk() {
        return radioUszk;
    }

    public JRadioButton getRadioBrak() {
        return radioBrak;
    }

    public void select(int radioNr) {
        switch (radioNr) {
            case 1:
                radioOk.setSelected(true);
                break;
            case 2:
                radioUszk.setSelected(true);
                break;
            case 3:
                radioBrak.setSelected(true);
                break;
            default:
                grupa.clearSelection();
        }
    }

    public int getSelected() {
        if (getRadioOk().isSelected()) {
            return 1;
        }
        if (getRadioUszk().isSelected()) {
            return 2;
        }
        if (getRadioBrak().isSelected()) {
            return 3;
        } else {
            return 4;
        }
    }

    @Override
    public String toString() {
        return "KomponentStan [radio= " + getSelected() + " ]";
    }

    public boolean isModified() {
        return modified;
    }
}

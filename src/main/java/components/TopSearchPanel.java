package components;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class TopSearchPanel extends JPanel {

    private final JTextFieldWthPopup txtWyszukaj;
    private final JButton btnWyszukaj;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public TopSearchPanel() {
        setBorder(new LineBorder(new Color(0, 0, 0)));
        FlowLayout flowLayout = (FlowLayout) getLayout();
        flowLayout.setAlignment(FlowLayout.RIGHT);

        txtWyszukaj = new JTextFieldWthPopup();
        add(txtWyszukaj);
        txtWyszukaj.setColumns(10);

        Component horizontalStrut_2 = Box.createHorizontalStrut(20);
        add(horizontalStrut_2);

        btnWyszukaj = new JButton("Wyszukaj");
        add(btnWyszukaj);
    }

    public JTextFieldWthPopup getTxtWyszukaj() {
        return txtWyszukaj;
    }

    public JButton getBtnWyszukaj() {
        return btnWyszukaj;
    }

    public String getSearchInput() {
        return txtWyszukaj.getText();
    }
}

package components;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class ButtonPanel extends JPanel {

    private final JButton btnWr = new JButton("Wróć");
    private final JButton btnUsu = new JButton("Usuń");
    private final JButton btnCofnij = new JButton("Cofnij");
    private final JButton btnZapisz = new JButton("Zapisz");
    private final JButton btnDrukuj = new JButton("Drukuj...");

    @SuppressWarnings("PMD.VariableNamingConventions")
    public ButtonPanel() {
        setBorder(new LineBorder(new Color(0, 0, 0)));
        //setMaximumSize(new Dimension(450, 40));

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{64, 70, 82, 0};
        gridBagLayout.rowHeights = new int[]{35, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.anchor = GridBagConstraints.NORTHWEST;
        gbc_panel.insets = new Insets(0, 0, 0, 5);
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        JPanel panelBack = new JPanel();
        add(panelBack, gbc_panel);
        panelBack.add(btnWr);

        GridBagConstraints gbc_btnDrukuj = new GridBagConstraints();
        gbc_btnDrukuj.insets = new Insets(0, 0, 0, 5);
        gbc_btnDrukuj.gridx = 1;
        gbc_btnDrukuj.gridy = 0;
        add(btnDrukuj, gbc_btnDrukuj);

        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.anchor = GridBagConstraints.EAST;
        gbc_panel_1.fill = GridBagConstraints.VERTICAL;
        gbc_panel_1.gridx = 2;
        gbc_panel_1.gridy = 0;
        JPanel panelModify = new JPanel();
        add(panelModify, gbc_panel_1);
        panelModify.add(btnUsu);
        panelModify.add(btnCofnij);
        panelModify.add(btnZapisz);
    }

    public JButton getBtnWr() {
        return btnWr;
    }

    public JButton getBtnUsu() {
        return btnUsu;
    }

    public JButton getBtnCofnij() {
        return btnCofnij;
    }

    public JButton getBtnZapisz() {
        return btnZapisz;
    }

    public JButton getBtnDrukuj() {
        return btnDrukuj;
    }
}

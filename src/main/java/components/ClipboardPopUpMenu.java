package components;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.JTextComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClipboardPopUpMenu extends JPopupMenu {

    private final JTextComponent text;
    private String content;
    private static final Logger LOG = LogManager.getLogger("src");

    public ClipboardPopUpMenu(JTextComponent text) {
        this.text = text;
        content = getClipboardContents();
        if (content == null) {
            content = "";
        }

        JMenuItem mntmWytnij = new JMenuItem("Wytnij");
        mntmWytnij.addActionListener(e -> cut());
        add(mntmWytnij);

        JMenuItem mntmKopiuj = new JMenuItem("Kopiuj");
        mntmKopiuj.addActionListener(e -> copy());
        add(mntmKopiuj);

        JMenuItem mntmWklej = new JMenuItem("Wklej");
        mntmWklej.addActionListener(e -> paste());
        add(mntmWklej);

        addSeparator();

        JMenuItem mntmWybierz = new JMenuItem("Zaznacz wszystko");
        mntmWybierz.addActionListener(e -> text.selectAll());
        add(mntmWybierz);

        this.addPopupMenuListener(new PopupMenuListener() {

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

                mntmWytnij.setEnabled(isTextSelected());
                mntmKopiuj.setEnabled(isTextSelected());
                mntmWklej.setEnabled(!content.isEmpty());
                mntmWybierz.setEnabled(text.getText().length() > 0);

            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

    }

    private void cut() {
        if (text.getSelectedText() != null) {
            setClipboardContents(text.getSelectedText());
        }
        int selectionStart = text.getSelectionStart();
        int selectionEnd = text.getSelectionEnd();
        String textStart = text.getText().substring(0, selectionStart);
        String textEnd = text.getText().substring(selectionEnd, text.getText().length());
        String textFull = textStart + textEnd;
        text.setText(textFull);
    }

    private void copy() {
        if (text.getSelectedText() != null) {
            setClipboardContents(text.getSelectedText());
        }
    }

    private void paste() {
        if (text.getSelectedText() == null) {
            int start = text.getCaretPosition();
            String textStart = text.getText().substring(0, start);
            String textEnd = text.getText().substring(start, text.getText().length());
            String textFull = textStart + getClipboardContents() + textEnd;
            text.setText(textFull);
        } else {
            text.setText(text.getText().replace(text.getSelectedText(), getClipboardContents()));
        }
    }

    private String getClipboardContents() {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable contents = clipboard.getContents(null);
        if (contents != null && contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            try {
                return (String) contents.getTransferData(DataFlavor.stringFlavor);
            } catch (Exception e) {
                LOG.error(e, e);
            }
        }
        return null;
    }

    private void setClipboardContents(String contents) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection strSel = new StringSelection(contents);
        clipboard.setContents(strSel, null);
    }

    private boolean isTextSelected() {
        return text.getSelectedText() != null;
    }
}

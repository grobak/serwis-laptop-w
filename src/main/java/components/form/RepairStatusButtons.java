package components.form;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JPanel;

public class RepairStatusButtons extends JPanel {

    private final JButton btnDemonta;
    private final JButton btnTesty;
    private final JButton btnNaprawa;
    private final JButton btnZakonczenieNaprawy;
    private final JButton btnWydanieSprztu;
    private final JButton btnDoTestw;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public RepairStatusButtons() {

        GridBagLayout gbl_repairStatusButtons = new GridBagLayout();
        gbl_repairStatusButtons.columnWidths = new int[]{0, 0};
        gbl_repairStatusButtons.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_repairStatusButtons.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_repairStatusButtons.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
        setLayout(gbl_repairStatusButtons);

        btnDemonta = new JButton("Demontaż");
        GridBagConstraints gbc_btnDemonta = new GridBagConstraints();
        gbc_btnDemonta.insets = new Insets(0, 0, 5, 0);
        gbc_btnDemonta.fill = GridBagConstraints.BOTH;
        gbc_btnDemonta.gridx = 0;
        gbc_btnDemonta.gridy = 0;
        add(btnDemonta, gbc_btnDemonta);

        btnNaprawa = new JButton("Naprawa");
        GridBagConstraints gbc_btnNaprawa = new GridBagConstraints();
        gbc_btnNaprawa.insets = new Insets(0, 0, 5, 0);
        gbc_btnNaprawa.fill = GridBagConstraints.BOTH;
        gbc_btnNaprawa.gridx = 0;
        gbc_btnNaprawa.gridy = 1;
        add(btnNaprawa, gbc_btnNaprawa);

        btnDoTestw = new JButton("Do testów");
        GridBagConstraints gbc_btnDoTestw = new GridBagConstraints();
        gbc_btnDoTestw.insets = new Insets(0, 0, 5, 0);
        gbc_btnDoTestw.fill = GridBagConstraints.BOTH;
        gbc_btnDoTestw.gridx = 0;
        gbc_btnDoTestw.gridy = 2;
        add(btnDoTestw, gbc_btnDoTestw);

        btnTesty = new JButton("Testy [0 / 16]");
        GridBagConstraints gbc_btnTesty = new GridBagConstraints();
        gbc_btnTesty.fill = GridBagConstraints.BOTH;
        gbc_btnTesty.insets = new Insets(0, 0, 5, 0);
        gbc_btnTesty.gridx = 0;
        gbc_btnTesty.gridy = 3;
        add(btnTesty, gbc_btnTesty);

        btnZakonczenieNaprawy = new JButton("Zakończenie naprawy");
        GridBagConstraints gbc_btnZakonczenieNaprawy = new GridBagConstraints();
        gbc_btnZakonczenieNaprawy.fill = GridBagConstraints.BOTH;
        gbc_btnZakonczenieNaprawy.insets = new Insets(0, 0, 5, 0);
        gbc_btnZakonczenieNaprawy.gridx = 0;
        gbc_btnZakonczenieNaprawy.gridy = 4;
        add(btnZakonczenieNaprawy, gbc_btnZakonczenieNaprawy);

        btnWydanieSprztu = new JButton("Wydanie sprzętu");
        GridBagConstraints gbc_btnWydanieSprztu = new GridBagConstraints();
        gbc_btnWydanieSprztu.fill = GridBagConstraints.BOTH;
        gbc_btnWydanieSprztu.gridx = 0;
        gbc_btnWydanieSprztu.gridy = 5;
        add(btnWydanieSprztu, gbc_btnWydanieSprztu);
    }

    public JButton getBtnDemonta() {
        return btnDemonta;
    }

    public JButton getBtnTesty() {
        return btnTesty;
    }

    public JButton getBtnNaprawa() {
        return btnNaprawa;
    }

    public JButton getBtnZakonczenieNaprawy() {
        return btnZakonczenieNaprawy;
    }

    public JButton getBtnWydanieSprztu() {
        return btnWydanieSprztu;
    }

    public JButton getBtnDoTestw() {
        return btnDoTestw;
    }
}

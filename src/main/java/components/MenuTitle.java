package components;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class MenuTitle extends JPanel {

    private final JLabel title;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public MenuTitle() {
        setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

        GridBagLayout gbl_panelMenuItem = new GridBagLayout();
        gbl_panelMenuItem.columnWidths = new int[]{562, 0};
        gbl_panelMenuItem.rowHeights = new int[]{25, 0};
        gbl_panelMenuItem.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_panelMenuItem.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        setLayout(gbl_panelMenuItem);

        title = new JLabel("PC CENTER Serwis");
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setFont(new Font("Tahoma", Font.BOLD, 20));
        GridBagConstraints gbc_lblNazwaMenu = new GridBagConstraints();
        gbc_lblNazwaMenu.gridx = 0;
        gbc_lblNazwaMenu.gridy = 0;
        add(title, gbc_lblNazwaMenu);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

}

package models;

public class CustomerAddress {

    private final int id;
    private final String city;
    private final String street;
    private final String zipcode;
    private final String phone;
    private final String email;

    public final static CustomerAddress EMPTY_ADDRESS = new AddressBuilder().build();

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static class AddressBuilder {

        private int id;
        private String city;
        private String street;
        private String zipcode;
        private String phone;
        private String email;

        public AddressBuilder() {
            city = "";
            street = "";
            zipcode = "";
            phone = "";
            email = "";
        }

        public AddressBuilder(CustomerAddress adres) {
            this.id = adres.getIdAdres();
            this.city = adres.getCity();
            this.street = adres.getStreet();
            this.zipcode = adres.getZipcode();
            this.phone = adres.getPhone();
            this.email = adres.getEmail();
        }

        public AddressBuilder id(int idAdres) {
            this.id = idAdres;
            return this;
        }

        public AddressBuilder city(String miejscowosc) {
            this.city = miejscowosc;
            return this;
        }

        public AddressBuilder street(String ulica) {
            this.street = ulica;
            return this;
        }

        public AddressBuilder zipcode(String zipcode) {
            this.zipcode = zipcode;
            return this;
        }

        public AddressBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public AddressBuilder email(String email) {
            this.email = email;
            return this;
        }

        public CustomerAddress build() {
            return new CustomerAddress(id, city, street, zipcode, phone, email);
        }
}

    private CustomerAddress(int id, String city, String street, String zipcode, String phone, String mail) {
        this.id = id;
    this.city = city;
    this.street = street;
    this.zipcode = zipcode;
    this.phone = phone;
        this.email = mail;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String[] getAdresAsArray() {
        return new String[]{city, street, zipcode, phone, email};
    }

    public int getIdAdres() {
        return id;
    }

    @Override
    public String toString() {
        return "KlientAdres [id=" + id + ", city="
                + city + ", street=" + street + ", zipcode="
                + zipcode + ", phone=" + phone + ", email=" + email
                + "]";
    }

}

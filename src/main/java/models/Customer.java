package models;

public class Customer implements Cloneable {

    private final int id;
    private final boolean company;
    private final String name;
    private final String NIP;
    private final CustomerAddress address;
    private final String notes;
    private final String dateAdded;
    private final String addedBy;

    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static class CustomerBuilder {

        private int id;
        private boolean company;
        private String name;
        private String nip;
        private CustomerAddress address;
        private String notes;
        private String dateAdded;
        private String addedBy;

        public CustomerBuilder() {
            name = "";
            nip = "";
            address = CustomerAddress.EMPTY_ADDRESS;
            notes = "";
            dateAdded = "";
            addedBy = "";
        }

        public CustomerBuilder(Customer klient) {
            this.nip = "";
            id = klient.getId();
            company = klient.isFirma();
            name = klient.getName();
            nip = klient.getNIP();
            address = klient.getAddress();
            notes = klient.getNotes();
            dateAdded = klient.getDateAdded();
            addedBy = klient.getAddedBy();
        }

        public CustomerBuilder id(int id) {
            this.id = id;
            return this;
        }

        public CustomerBuilder isCompany(boolean isCompany) {
            this.company = isCompany;
            return this;
        }

        public CustomerBuilder name(String clientName) {
            this.name = clientName;
            return this;
        }

        public CustomerBuilder nip(String nip) {
            this.nip = nip;
            return this;
        }

        public CustomerBuilder address(CustomerAddress clientAddress) {
            this.address = clientAddress;
            return this;
        }

        public CustomerBuilder notes(String notes) {
            this.notes = notes;
            return this;
        }

        public CustomerBuilder dateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
            return this;
        }

        public CustomerBuilder addedBy(String addedBy) {
            this.addedBy = addedBy;
            return this;
        }

        public Customer build() {
            return new Customer(id, company, name, nip, address, notes, dateAdded, addedBy);
        }
    }

    public Customer(int id, boolean company, String name, String nip, CustomerAddress address, String notes, String dateAdded, String addedBy) {
        this.id = id;
        this.company = company;
        this.name = name;
        this.NIP = nip;
        this.address = address;
        this.notes = notes;
        this.dateAdded = dateAdded;
        this.addedBy = addedBy;
    }

    public Customer(int id, boolean company, String name, String nip, CustomerAddress address, String notes) {
        this(id, company, name, nip, address, notes, "", "");
    }

    public Customer(boolean company, String name, String nip, CustomerAddress address, String notes) {
        this(0, company, name, nip, address, notes, "", "");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNIP() {
        return NIP;
    }

    public String getNotes() {
        return notes;
    }

    public CustomerAddress getAddress() {
        return address;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean isFirma() {
        return company;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public int getAddressID() {
        return address.getIdAdres();
    }

    public String getEmail() {
        return address.getEmail();
    }

    public String getCity() {
        return address.getCity();
    }

    public String getStreet() {
        return address.getStreet();
    }

    public String getZipcode() {
        return address.getZipcode();
    }

    public String getPhone() {
        return address.getPhone();
    }

    @Override
    public String toString() {
        return "Klient [idKlienta=" + id + ", firma=" + company
                + ", nazwaKlienta=" + name + ", NIP=" + NIP
                + ", adres=" + address + ", notatki="
                + notes + ", dataDodania=" + dateAdded + ", dodanyPrzez="
                + addedBy + "]";
    }

}

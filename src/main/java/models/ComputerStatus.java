package models;

public class ComputerStatus {

    private int id;
    private int hdd;
    private int dvd;
    private int bateria;
    private int zasilacz;
    private int ladowanie;
    private int bluetooth;
    private int glosniki;
    private int sluchawki;
    private int mikrofon;
    private int klawiatura;
    private int kamera;
    private int touchpad;
    private int usb;
    private int lan;
    private int wifi;
    private int monitorZew;

    public ComputerStatus() {
        this.hdd = 4;
        this.dvd = 4;
        this.bateria = 4;
        this.zasilacz = 4;
        this.ladowanie = 4;
        this.bluetooth = 4;
        this.glosniki = 4;
        this.sluchawki = 4;
        this.mikrofon = 4;
        this.klawiatura = 4;
        this.kamera = 4;
        this.touchpad = 4;
        this.usb = 4;
        this.lan = 4;
        this.wifi = 4;
        this.monitorZew = 4;
    }

    public ComputerStatus(int hdd, int dvd, int bat, int zasil, int lad, int blue, int glos, int sluch, int mik, int klaw, int kam, int tpad, int usb, int lan, int wifi, int monZew) {
        setHDD(hdd);
        setDVD(dvd);
        setBateria(bat);
        setZasilacz(zasil);
        setLadowanie(lad);
        setBluetooth(blue);
        setGlosniki(glos);
        setSluchawki(sluch);
        setMikrofon(mik);
        setKlawiatura(klaw);
        setKamera(kam);
        setTouchpad(tpad);
        setUSB(usb);
        setLAN(lan);
        setWiFi(wifi);
        setMonitorZew(monZew);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHDD() {
        return hdd;
    }

    public void setHDD(int hDD) {
        hdd = hDD;
    }

    public int getDVD() {
        return dvd;
    }

    public void setDVD(int dVD) {
        dvd = dVD;
    }

    public int getBateria() {
        return bateria;
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public int getZasilacz() {
        return zasilacz;
    }

    public void setZasilacz(int zasilacz) {
        this.zasilacz = zasilacz;
    }

    public int getLadowanie() {
        return ladowanie;
    }

    public void setLadowanie(int ladowanie) {
        this.ladowanie = ladowanie;
    }

    public int getBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(int bluetooth) {
        this.bluetooth = bluetooth;
    }

    public int getGlosniki() {
        return glosniki;
    }

    public void setGlosniki(int glosniki) {
        this.glosniki = glosniki;
    }

    public int getSluchawki() {
        return sluchawki;
    }

    public void setSluchawki(int sluchawki) {
        this.sluchawki = sluchawki;
    }

    public int getMikrofon() {
        return mikrofon;
    }

    public void setMikrofon(int mikrofon) {
        this.mikrofon = mikrofon;
    }

    public int getKlawiatura() {
        return klawiatura;
    }

    public void setKlawiatura(int klawiatura) {
        this.klawiatura = klawiatura;
    }

    public int getTouchpad() {
        return touchpad;
    }

    public void setTouchpad(int touchpad) {
        this.touchpad = touchpad;
    }

    public int getUSB() {
        return usb;
    }

    public void setUSB(int uSB) {
        usb = uSB;
    }

    public int getLAN() {
        return lan;
    }

    public void setLAN(int lAN) {
        lan = lAN;
    }

    public int getWiFi() {
        return wifi;
    }

    public void setWiFi(int wiFi) {
        wifi = wiFi;
    }

    public int getMonitorZew() {
        return monitorZew;
    }

    public void setMonitorZew(int monitorZew) {
        this.monitorZew = monitorZew;
    }

    public int getKamera() {
        return kamera;
    }

    public void setKamera(int kamera) {
        this.kamera = kamera;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + dvd;
        result = prime * result + hdd;
        result = prime * result + lan;
        result = prime * result + usb;
        result = prime * result + wifi;
        result = prime * result + bateria;
        result = prime * result + bluetooth;
        result = prime * result + glosniki;
        result = prime * result + id;
        result = prime * result + kamera;
        result = prime * result + klawiatura;
        result = prime * result + ladowanie;
        result = prime * result + mikrofon;
        result = prime * result + monitorZew;
        result = prime * result + sluchawki;
        result = prime * result + touchpad;
        result = prime * result + zasilacz;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ComputerStatus other = (ComputerStatus) obj;
        if (dvd != other.dvd) {
            return false;
        }
        if (hdd != other.hdd) {
            return false;
        }
        if (lan != other.lan) {
            return false;
        }
        if (usb != other.usb) {
            return false;
        }
        if (wifi != other.wifi) {
            return false;
        }
        if (bateria != other.bateria) {
            return false;
        }
        if (bluetooth != other.bluetooth) {
            return false;
        }
        if (glosniki != other.glosniki) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        if (kamera != other.kamera) {
            return false;
        }
        if (klawiatura != other.klawiatura) {
            return false;
        }
        if (ladowanie != other.ladowanie) {
            return false;
        }
        if (mikrofon != other.mikrofon) {
            return false;
        }
        if (monitorZew != other.monitorZew) {
            return false;
        }
        if (sluchawki != other.sluchawki) {
            return false;
        }
        if (touchpad != other.touchpad) {
            return false;
        }
        return zasilacz == other.zasilacz;
    }

}

package models;

public class Naprawa implements Cloneable{
	private String kto = "";
	private int idStatus = 0;
	private String data = "";

	public Naprawa(String kt, int idS, String dat){
		setKto(kt);
		setIdStatus(idS);
		setData(dat);		
	}

	public String getKto() {
		return kto;
	}

	public void setKto(String kto) {
		this.kto = kto;
	}

	public int getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public Naprawa clone() throws CloneNotSupportedException {
        return (Naprawa) super.clone();
    }	

}

package models;

import repositories.RepairRepository;


public class Computer implements Cloneable {
	private int idLaptopa = 0;
	private int idKlienta = 0;
	private String model = "";
	//private Model model = new Model();
	private String dataPrzyjecia = "";
	private String dataWysylki = "";
	private String rma = "";
	private String rodzajNaprawy = "";
	private String opisUsterki = "";
	private ComputerStatus stan = new ComputerStatus();
	private Equipment equipment = new Equipment();
	private String uwagi = "";
	private String opisNaprawy = "Naprawa płyty głównej";
	private String statusData = "";

	public Computer() {
	}

	public Computer(Computer sprzet, int idS) {
		setIdLaptopa(idS);
		setIdKlienta(sprzet.getIdKlienta());
		setModel(sprzet.getModel());
		setDataPrzyjecia(sprzet.getDataPrzyjecia());
		setRMA(sprzet.getRMA());
		setEquipment(sprzet.getEquipment());
		setUwagi(sprzet.getUwagi());
	}

	public Computer(int idS, int idK, String mode, String dataPrzyj,
			String nrSer, Equipment aWyposazenie, String uwaga) {
		setIdLaptopa(idS);
		setIdKlienta(idK);
		setModel(mode);
		setDataPrzyjecia(dataPrzyj);
		setRMA(nrSer);
		setEquipment(aWyposazenie);
		setUwagi(uwaga);
	}

	public Computer(int idS, int idK, String mode, String dataPrzyj,
			String nrSer, Equipment aWyposazenie, String opisNapraw,
			String opisUsterk, String uwaga) {
		setIdLaptopa(idS);
		setIdKlienta(idK);
		setModel(mode);
		setDataPrzyjecia(dataPrzyj);
		setRMA(nrSer);
		setEquipment(aWyposazenie);
		setOpisNaprawy(opisNapraw);
		setOpisUsterki(opisUsterk);
		setUwagi(uwaga);
	}
	
	public boolean isInitialized(){
		return idLaptopa!=0;
	}

	public Computer(int idS) {
		setIdLaptopa(idS);
	}

	public int getId() {
		return idLaptopa;
	}

	public void setIdLaptopa(int idLaptopa) {
		this.idLaptopa = idLaptopa;
	}

	public int getIdKlienta() {
		return idKlienta;
	}

	public void setIdKlienta(int idKlienta) {
		this.idKlienta = idKlienta;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDataPrzyjecia() {
		return dataPrzyjecia;
	}

	public void setDataPrzyjecia(String dataPrzyjecia) {
		this.dataPrzyjecia = dataPrzyjecia;
	}

	public String getDataWysylki() {
		return dataWysylki;
	}

	public void setDataWysylki(String dataWysylki) {
		this.dataWysylki = dataWysylki;
	}

	public String getRMA() {
		return rma;
	}

	public void setRMA(String nrSeryjny) {
		this.rma = nrSeryjny;
	}

	public String getOpisUsterki() {
		return opisUsterki;
	}

	public void setOpisUsterki(String opisUsterki) {
		this.opisUsterki = opisUsterki;
	}

	public String getUwagi() {
		return uwagi;
	}

	public void setUwagi(String uwagi) {
		this.uwagi = uwagi;
	}

	public ComputerStatus getStan() {
		return stan;
	}

	public void setStan(ComputerStatus stan) {
		this.stan = stan;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public String getOwner() {
		return new RepairRepository().getOwnerName(this.idKlienta);
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getOpisNaprawy() {
		return opisNaprawy;
	}

	public void setOpisNaprawy(String opisNaprawy) {
		this.opisNaprawy = opisNaprawy;
	}

	public String getRodzajNaprawy() {
		return rodzajNaprawy;
	}

	public void setRodzajNaprawy(String rodzajNaprawy) {
		this.rodzajNaprawy = rodzajNaprawy;
	}
	
	public String getStatusData() {
		return statusData;
	}

	public void setStatusData(String statusData) {
		this.statusData = statusData;
	}

	@Override
	public String toString() {
		return "Sprzet [idLaptopa=" + idLaptopa + ", idKlienta=" + idKlienta
				+ ", model=" + model + ", dataPrzyjecia=" + dataPrzyjecia
				+ ", dataWysylki=" + dataWysylki + ", RMA=" + rma
				+ ", rodzajNaprawy=" + rodzajNaprawy + ", opisUsterki="
				+ opisUsterki + ", stan=" + stan + ", wyposazenie="
				+ equipment + ", uwagi=" + uwagi + ", opisNaprawy="
				+ opisNaprawy + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rma == null) ? 0 : rma.hashCode());
		result = prime * result
				+ ((dataPrzyjecia == null) ? 0 : dataPrzyjecia.hashCode());
		result = prime * result
				+ ((dataWysylki == null) ? 0 : dataWysylki.hashCode());
		result = prime * result + idKlienta;
		result = prime * result + idLaptopa;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result
				+ ((opisNaprawy == null) ? 0 : opisNaprawy.hashCode());
		result = prime * result
				+ ((opisUsterki == null) ? 0 : opisUsterki.hashCode());
		result = prime * result
				+ ((rodzajNaprawy == null) ? 0 : rodzajNaprawy.hashCode());
		result = prime * result + ((stan == null) ? 0 : stan.hashCode());
		result = prime * result
				+ ((statusData == null) ? 0 : statusData.hashCode());
		result = prime * result + ((uwagi == null) ? 0 : uwagi.hashCode());
		result = prime * result
				+ ((equipment == null) ? 0 : equipment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Computer other = (Computer) obj;
		if (rma == null) {
			if (other.rma != null)
				return false;
		} else if (!rma.equals(other.rma))
			return false;
		if (dataPrzyjecia == null) {
			if (other.dataPrzyjecia != null)
				return false;
		} else if (!dataPrzyjecia.equals(other.dataPrzyjecia))
			return false;
		if (dataWysylki == null) {
			if (other.dataWysylki != null)
				return false;
		} else if (!dataWysylki.equals(other.dataWysylki))
			return false;
		if (idKlienta != other.idKlienta)
			return false;
		if (idLaptopa != other.idLaptopa)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (opisNaprawy == null) {
			if (other.opisNaprawy != null)
				return false;
		} else if (!opisNaprawy.equals(other.opisNaprawy))
			return false;
		if (opisUsterki == null) {
			if (other.opisUsterki != null)
				return false;
		} else if (!opisUsterki.equals(other.opisUsterki))
			return false;
		if (rodzajNaprawy == null) {
			if (other.rodzajNaprawy != null)
				return false;
		} else if (!rodzajNaprawy.equals(other.rodzajNaprawy))
			return false;
		if (stan == null) {
			if (other.stan != null)
				return false;
		} else if (!stan.equals(other.stan))
			return false;
		if (statusData == null) {
			if (other.statusData != null)
				return false;
		} else if (!statusData.equals(other.statusData))
			return false;
		if (uwagi == null) {
			if (other.uwagi != null)
				return false;
		} else if (!uwagi.equals(other.uwagi))
			return false;
		if (equipment == null) {
			if (other.equipment != null)
				return false;
		} else if (!equipment.equals(other.equipment))
			return false;
		return true;
	}

}

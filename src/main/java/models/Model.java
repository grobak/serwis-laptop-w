package models;

public class Model implements Cloneable {

    private int id;
    private String manufacturer;
    private String model;
    private int price;

    public Model(String manufacturer, String model, int price) {
        this(0, manufacturer, model, price);
    }

    public Model(int id, String manufacturer, String model, int price) {
        this.id = id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setProducent(String producent) {
        this.manufacturer = producent;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return manufacturer + " " + model;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int getCena() {
        return price;
    }

    public void setCena(int cena) {
        this.price = cena;
    }

}

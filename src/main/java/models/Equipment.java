package models;

import components.KomponentWyposazenie;

public class Equipment {

    private int equipmentId;
    private boolean hdd;
    private boolean dvd;
    private boolean bateria;
    private boolean zasilacz;
    private boolean kabelAC;
    private boolean torba;
    private boolean karton;
    private String wypDodatkowe;
    private String brak;

    public Equipment() {
        this.hdd = false;
        this.dvd = false;
        this.bateria = false;
        this.zasilacz = false;
        this.hdd = false;
        this.kabelAC = false;
        this.torba = false;
        this.karton = false;
        this.wypDodatkowe = "";
        this.brak = "";
    }

    public Equipment(int id) {
        this.equipmentId = id;
    }

    public Equipment(int id, boolean hdd, boolean dvd, boolean bat, boolean zasil, boolean kabel, boolean torb, boolean karto, String wypDodat, String braki) {
        setEquipmentID(id);
        setHDD(hdd);
        setDVD(dvd);
        setBateria(bat);
        setZasilacz(zasil);
        setKabelAC(kabel);
        setTorba(torb);
        setKarton(karto);
        setWypDodatkowe(wypDodat);
        setBrak(braki);
    }

    public Equipment(int id, KomponentWyposazenie komponent, String wypDodat, String braki) {
        setEquipmentID(id);
        setHDD(komponent.getChckbxHdd());
        setDVD(komponent.getChckbxCddvd());
        setBateria(komponent.getChckbxBateria());
        setZasilacz(komponent.getChckbxZasilacz());
        setKabelAC(komponent.getChckbxPrzewdAc());
        setTorba(komponent.getChckbxTorba());
        setKarton(komponent.getChckbxKarton());
        setWypDodatkowe(wypDodat);
        setBrak(braki);
    }

    public Equipment(Equipment wyp, String wypDodat, String braki) {
        setHDD(wyp.isHDD());
        setDVD(wyp.isDVD());
        setBateria(wyp.isBateria());
        setZasilacz(wyp.isZasilacz());
        setKabelAC(wyp.isKabelAC());
        setTorba(wyp.isTorba());
        setKarton(wyp.isKarton());
        setWypDodatkowe(wypDodat);
        setBrak(braki);
    }

    public boolean isHDD() {
        return hdd;
    }

    public void setHDD(boolean hDD) {
        hdd = hDD;
    }

    public boolean isDVD() {
        return dvd;
    }

    public void setDVD(boolean dVD) {
        dvd = dVD;
    }

    public boolean isBateria() {
        return bateria;
    }

    public void setBateria(boolean bateria) {
        this.bateria = bateria;
    }

    public boolean isZasilacz() {
        return zasilacz;
    }

    public void setZasilacz(boolean aZasilacz) {
        zasilacz = aZasilacz;
    }

    public boolean isKabelAC() {
        return kabelAC;
    }

    public void setKabelAC(boolean kabelAC) {
        this.kabelAC = kabelAC;
    }

    public boolean isTorba() {
        return torba;
    }

    public void setTorba(boolean torba) {
        this.torba = torba;
    }

    public String getWypDodatkowe() {
        return wypDodatkowe;
    }

    public void setWypDodatkowe(String wypDodatkowe) {
        this.wypDodatkowe = wypDodatkowe;
    }

    public String getBrak() {
        return brak;
    }

    public void setBrak(String brak) {
        this.brak = brak;
    }

    public boolean isKarton() {
        return karton;
    }

    public void setKarton(boolean karton) {
        this.karton = karton;
    }

    public int getId() {
        return equipmentId;
    }

    public void setEquipmentID(int id) {
        this.equipmentId = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (dvd ? 1231 : 1237);
        result = prime * result + (hdd ? 1231 : 1237);
        result = prime * result + (bateria ? 1231 : 1237);
        result = prime * result + ((brak == null) ? 0 : brak.hashCode());
        result = prime * result + equipmentId;
        result = prime * result + (kabelAC ? 1231 : 1237);
        result = prime * result + (karton ? 1231 : 1237);
        result = prime * result + (torba ? 1231 : 1237);
        result = prime * result
                + ((wypDodatkowe == null) ? 0 : wypDodatkowe.hashCode());
        result = prime * result + (zasilacz ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Equipment other = (Equipment) obj;
        if (dvd != other.dvd) {
            return false;
        }
        if (hdd != other.hdd) {
            return false;
        }
        if (bateria != other.bateria) {
            return false;
        }
        if (brak == null) {
            if (other.brak != null) {
                return false;
            }
        } else if (!brak.equals(other.brak)) {
            return false;
        }
        if (equipmentId != other.equipmentId) {
            return false;
        }
        if (kabelAC != other.kabelAC) {
            return false;
        }
        if (karton != other.karton) {
            return false;
        }
        if (torba != other.torba) {
            return false;
        }
        if (wypDodatkowe == null) {
            if (other.wypDodatkowe != null) {
                return false;
            }
        } else if (!wypDodatkowe.equals(other.wypDodatkowe)) {
            return false;
        }
        return zasilacz == other.zasilacz;
    }

}

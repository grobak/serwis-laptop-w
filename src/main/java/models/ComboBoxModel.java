package models;

import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

public class ComboBoxModel<T> extends DefaultComboBoxModel<T> {

	public ComboBoxModel() {
		super();
	}

	public ComboBoxModel(T[] items) {
		super( items );
	}

	public ComboBoxModel(Vector<T> vector) {
		super( vector );
	}
	
	public ComboBoxModel(List<T> list) {
		this( new Vector<T>(list) );
	}

}

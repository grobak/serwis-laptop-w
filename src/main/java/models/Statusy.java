package models;

public class Statusy {
	
	private int idStatus = 0;
	private String nazwa = "";
	
	public Statusy(int id, String nazw){
		setIdStatus(id);
		setNazwa(nazw);
	}
	
	public int getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

}

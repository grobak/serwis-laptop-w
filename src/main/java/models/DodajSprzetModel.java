package models;

import java.util.concurrent.CopyOnWriteArrayList;
import listeners.Listenable;
import listeners.OwnListener;

public class DodajSprzetModel implements Listenable {
	
	private Customer klient;
	private Computer sprzet;
	
	private CopyOnWriteArrayList<OwnListener> listeners = new CopyOnWriteArrayList<>();
	
	public Customer getKlient() {
		return klient;
	}
	public void setKlient(Customer klient) {
		this.klient = klient;
	}
	public Computer getSprzet() {
		return sprzet;
	}
	public void setSprzet(Computer sprzet) {
		this.sprzet = sprzet;
	}
	@Override
	public void addListener(OwnListener listener) {
		listeners.add(listener);
		
	}
	@Override
	public void removeListener(OwnListener listener) {
		listeners.remove(listener);
	}
	@Override
	public void notifyListeners() {
            listeners.forEach(OwnListener::update);
		
	}

}

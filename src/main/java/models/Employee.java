package models;

public class Employee implements Cloneable {

    private int positionId;
    private String login;
    private String password;
    private String name;
    private String phone;
    private String salt;

    public Employee() {
        this.login = "";
        this.password = "";
        this.name = "";
        this.phone = "";
        this.salt = "";
    }

    public Employee(String login, String password, String name, String phone, int positionId) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.positionId = positionId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return password;
    }

    public void setHaslo(String haslo) {
        this.password = haslo;
    }

    public String getImie() {
        return name;
    }

    public void setImie(String imie) {
        this.name = imie;
    }

    public String getTelefon() {
        return phone;
    }

    public void setTelefon(String notatki) {
        this.phone = notatki;
    }

    @Override
    public Employee clone() throws CloneNotSupportedException {
        return (Employee) super.clone();
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean isAdmin() {
        return positionId == 1;
    }
}

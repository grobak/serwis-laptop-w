package models;

public class DatabaseConfiguration {
	
	private String dbHostname = "localhost";
	private int port = 3306;
	private String dbName = "serwis";
	private String smtpHostname = "mail.pccenter.pl";
	private String email = "status@pccenter.pl";
	private char[] emailPassword = "WaxhLjKW".toCharArray();
	private String lastLoggedUsername = "";
	
	public String getDbHostname() {
		return dbHostname;
	}
	public void setDbHostname(String dbHostname) {
		this.dbHostname = dbHostname;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getSmtpHostname() {
		return smtpHostname;
	}
	public void setSmtpHostname(String smtpHostname) {
		this.smtpHostname = smtpHostname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailPassword() {
		return new String(emailPassword);
	}
	public void setEmailPassword(char[] emailPassword) {
		this.emailPassword = emailPassword;
	}
	public String getLastLoggedUsername() {
		return lastLoggedUsername;
	}
	public void setLastLoggedUsername(String lastLoggedUsername) {
		this.lastLoggedUsername = lastLoggedUsername;
	}
	
	

}
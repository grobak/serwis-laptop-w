package models;

public class Grupy implements Cloneable{
	private String prefix = "";
	private int idGrupy = 0;
	private String nazwa = "";
	
	public Grupy(){
		
	}
	
	public Grupy(String pre, int id, String nazw){
		prefix=pre;
		idGrupy=id;
		nazwa=nazw;
	}
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public int getIdGrupy() {
		return idGrupy;
	}
	public void setIdGrupy(int idGrupy) {
		this.idGrupy = idGrupy;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

package models;

public enum RepairStatus {
	PRZYJETY(1),DEMONTAZ(2),W_NAPRAWIE(3),DO_TESTOW(4),PODCZAS_TESTOW(5),NAPRAWIONY(6),WYDANY(7);
	
	private final int value;

	RepairStatus( int val ){
		this.value = val;
	}
    
	public int getValue(){
		return this.value;
    }
}

package repositories;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.logging.log4j.Logger;

import dataproviders.Database;
import dataproviders.EncryptedPassword;
import models.Employee;

public class EmployeeStatementFactory
{
	private static final Logger LOG = org.apache.logging.log4j.LogManager.getLogger( "src" );

	public PreparedStatement get( String login ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_PRACOWNIK );
		preStmt.setString( 1, login );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement add( Employee pracownik, EncryptedPassword password ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.ADD_PRACOWNIK );
		preStmt.setString( 1, pracownik.getLogin() );
		preStmt.setInt( 2, pracownik.getPositionId() );
		preStmt.setString( 3, password.getHash() );
		preStmt.setString( 4, password.getSalt() );
		preStmt.setString( 5, pracownik.getImie() );
		preStmt.setString( 6, pracownik.getTelefon() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement delete( String login ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.DELETE_PRACOWNIK );
		preStmt.setString( 1, login );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement update( String login, Employee pracownik ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.UPDATE_PRACOWNIK );
		preStmt.setString( 1, pracownik.getLogin() );
		preStmt.setInt( 2, pracownik.getPositionId() );
		preStmt.setString( 3, pracownik.getHaslo() );
		preStmt.setString( 4, pracownik.getSalt() );
		preStmt.setString( 5, pracownik.getImie() );
		preStmt.setString( 6, pracownik.getTelefon() );
		preStmt.setString( 7, login );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getPositionName( int positionID ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_PRACOWNIK_STANOWISKO );
		preStmt.setInt( 1, positionID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getPositionId( String name ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_STANOWISKO_ID );
		preStmt.setString( 1, name );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement findByLoginOrName( String searchCondition ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_PRACOWNICY );
		preStmt.setString( 1, "%" + searchCondition + "%" );
		preStmt.setString( 2, "%" + searchCondition + "%" );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
}

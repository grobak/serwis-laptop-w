package repositories;

public class SQLStatements {
	public static final String ADD_KLIENT = "INSERT INTO klienci (`Firma`, `Nazwa_klienta`, `NIP`, `Notatki`, `Dodany_przez`, id_adres, data_dodania) VALUES (?, ?, ?, ?, ?, ?, NOW())";
	public static final String ADD_KLIENT_ADRES = "INSERT INTO adres (`Miejscowosc`, `Ulica`, `Kod_pocztowy`, `Telefon`, `Email`) VALUES (?, ?, ?, ?, ?)";
	public static final String GET_KLIENT =	"SELECT * FROM klienci WHERE Id_klienta = ?";
	public static final String GET_KLIENT_ADRES = "SELECT * FROM adres WHERE Id_adres=?";
	public static final String GET_KLIENT_NAZWA_BY_ID = "SELECT Nazwa_klienta FROM klienci WHERE Id_klienta = ?";
	public static final String DELETE_KLIENT = "DELETE FROM klienci WHERE Id_klienta =?";
	public static final String DELETE_KLIENT_ADRES = "DELETE FROM adres WHERE Id_adres =?";
	public static final String KLIENT_NAME_CHECK = "SELECT nazwa_klienta FROM klienci WHERE nazwa_klienta like ?";
	public static final String SEARCH_FOR_KLIENT = "SELECT k.`Id_klienta`, `Nazwa_klienta`, `Miejscowosc`, `Telefon` "
								+ "FROM klienci k LEFT JOIN adres ka ON k.Id_adres=ka.Id_adres "
								+ "WHERE `Nazwa_klienta` LIKE ? OR miejscowosc LIKE ? ORDER BY k.`Id_klienta` DESC LIMIT 100";
	public static final String SEARCH_FOR_SPRZET = "SELECT laptopy.`Id_laptopa`, laptopy.Id_klienta, `model`, `RMA`, `Data_przyjecia`, `Nazwa_klienta`, (select nazwa_statusu from status where id_status=max(naprawy.id_status)) as last_status FROM laptopy,klienci, naprawy, status WHERE naprawy.id_laptopa=laptopy.id_laptopa and status.id_status=naprawy.id_status and laptopy.id_klienta=klienci.id_klienta AND (rma LIKE ? OR nazwa_klienta LIKE ? OR model LIKE ?) group by naprawy.id_laptopa ORDER BY id_laptopa desc LIMIT 250";
	public static final String UPDATE_KLIENT = "UPDATE klienci SET `Firma`=?, `Nazwa_klienta`=?, `NIP`=?, `Notatki`=? WHERE `Id_klienta`=?";
	public static final String UPDATE_KLIENT_ADRES = "UPDATE adres SET `Miejscowosc`=?, `Ulica`=?, `Kod_pocztowy`=?, `Telefon`=?, `Email`=? WHERE id_adres=?";
	public static final String KLIENT_HAS_HARDWARE = "SELECT id_laptopa FROM laptopy WHERE id_klienta=?";
	
	public static final String GET_ID_LAPTOPA_BY_STATUS = "SELECT id_laptopa, MAX(id_status) AS stat FROM naprawy where naprawy.id_status=? GROUP BY id_laptopa ORDER BY id_laptopa desc";
	
	public static final String LIST_REPAIRS_BY_STATUS = "SELECT max(data) as dat, l.id_laptopa, `Id_klienta`, `Id_wyposazenie`, `RMA`, `Model`, `Data_przyjecia`, `Data_wysylki`, `Rodzaj_naprawy`, `Opis_usterki`, `Opis_naprawy`, `Uwagi` from laptopy l, naprawy n where l.id_laptopa=n.id_laptopa and data_przyjecia > curDate() - interval '6' month group by l.id_laptopa having max(id_status)=? order by dat desc limit 250";
	public static final String LIST_REPAIRS_BY_STATUS_SEARCH = "SELECT max(data) as dat, l.id_laptopa, l.`Id_klienta`, nazwa_klienta, `Id_wyposazenie`, `RMA`, `Model`, `Data_przyjecia`, `Data_wysylki`, `Rodzaj_naprawy`, `Opis_usterki`, `Opis_naprawy`, `Uwagi` from laptopy l, naprawy n, klienci k where l.id_laptopa=n.id_laptopa AND k.id_klienta=l.id_klienta and (rma LIKE ? OR nazwa_klienta LIKE ? OR model LIKE ?) and data_przyjecia > curDate() - interval '6' month group by l.id_laptopa having max(id_status)=? order by dat desc limit 250";
	public static final String KTO_NAPRAWIAL = "SELECT Imie FROM naprawy, pracownicy WHERE `Id_laptopa`=? AND `Id_status`=3 AND naprawy.login=pracownicy.login";
	public static final String KTO_PRZYJAL = "SELECT imie FROM pracownicy, naprawy WHERE `Id_laptopa`=? AND `Id_status`=1 AND pracownicy.login=naprawy.login";
	
	public static final String ADD_LAPTOP = "INSERT INTO laptopy (`Id_klienta`, `model`, `RMA`, `Uwagi`, `Opis_usterki`, `Id_wyposazenie`, `Rodzaj_naprawy`, Opis_naprawy, data_przyjecia) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW())";
	public static final String ADD_NAPRAWA = "INSERT INTO naprawy (`Id_laptopa`, `login`, `Id_status`) VALUES (?, ?, ?)";
	public static final String ADD_WYPOSAZENIE = "INSERT INTO wyposazenie (`HDD`, `DVD`, `Bateria`, `Zasilacz`, `Kabel_ac`, `Torba`, `Karton`, `Wyp_dodatkowe`, `Brak`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String GET_DATA_WYSYLKI = "SELECT data_wysylki FROM laptopy WHERE Id_laptopa = ?";
	public static final String GET_TESTY = "SELECT * FROM testy t LEFT JOIN laptopynatesty lt ON t.Id_testy = lt.Id_testy WHERE Id_laptopa = ? AND t.id_testy=(SELECT MAX(t2.id_testy) FROM testy t2 WHERE lt.id_laptopa=? AND t2.id_testy = lt.id_testy)";
	public static final String GET_LAPTOP = "SELECT * FROM laptopy WHERE Id_laptopa = ?";
	
	public static final String GET_MOJE_LAPTOPY = "SELECT login, max(data) as data, l.id_laptopa, l.id_klienta, `Id_wyposazenie`, `RMA`, `Model`, `Data_przyjecia`, `Data_wysylki`, `Rodzaj_naprawy`, `Opis_usterki`, `Opis_naprawy`, `Uwagi`, nazwa_klienta from laptopy l, naprawy n, klienci k where l.id_laptopa=n.id_laptopa and l.id_klienta=k.id_klienta and data_przyjecia > curDate() - interval '6' month and n.login =? AND (rma LIKE ? OR nazwa_klienta LIKE ? OR model LIKE ?) group by l.id_laptopa order by data desc limit 250";
	
	public static final String GET_NAPRAWY_BY_LAPTOP_ID = "SELECT id_naprawy, id_laptopa, imie, id_status, data FROM naprawy, pracownicy WHERE naprawy.login=pracownicy.login AND id_laptopa = ? ORDER BY id_naprawy asc";
	public static final String GET_WYPOSAZENIE = "SELECT * FROM wyposazenie WHERE Id_wyposazenie = ?";
	public static final String GET_STAN_LAPTOPA = "SELECT * FROM testy LEFT JOIN laptopynatesty ON testy.Id_testy = laptopynatesty.Id_testy WHERE Id_laptopa = ?";
	public static final String GET_NAZWY_STATUSOW = "SELECT Nazwa_statusu FROM status GROUP BY id_status ASC";
	public static final String GET_STATUS_LAPTOPA_BY_ID = "SELECT Nazwa_statusu FROM status WHERE Id_status=?";
	public static final String GET_STAN_TEKST = "SELECT Stan FROM stantekst WHERE Id_stanTekst=?";
	public static final String GET_NUMBER_OF_TESTS = "SELECT COUNT(distinct CASE WHEN `HDD`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `DVD`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Bateria`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Zasilacz`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Ladowanie`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Bluetooth`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Glosniki`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Sluchawki`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Mikrofon`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Klawiatura`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Kamera`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Touchpad`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `USB`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `LAN`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Wifi`<>4  THEN 1 END) + "
			+ "COUNT(distinct CASE WHEN `Mon_zew`<>4  THEN 1 END) as completed_tests "
			+ "FROM `testy` t, laptopynatesty lt "
			+ "WHERE t.id_testy=lt.id_testy and lt.id_laptopa=? having max(t.id_testy)";
	public static final String DELETE_NAPRAWA = "DELETE FROM naprawy WHERE `Id_laptopa`=?";
	public static final String DELETE_TESTY = "DELETE FROM testy WHERE id_testy=(SELECT laptopynatesty.id_testy FROM laptopynatesty WHERE Id_laptopa=?)";
	public static final String DELETE_LAPTOPYNATESTY = "DELETE FROM laptopynatesty WHERE Id_laptopa=?";
	public static final String DELETE_LAPTOPA = "DELETE FROM laptopy WHERE Id_laptopa=?";
	public static final String DELETE_WYPOSAZENIE = "DELETE FROM wyposazenie WHERE `Id_wyposazenie`=?";
	public static final String SET_DATA_WYSYLKI = "UPDATE laptopy SET data_wysylki=NOW() WHERE id_laptopa=?";
	public static final String UPDATE_TESTY = "INSERT INTO testy (`HDD`, `DVD`, `Bateria`, `Zasilacz`, "
								+ "ladowanie, bluetooth, glosniki, sluchawki, mikrofon, klawiatura, kamera, Touchpad,"
								+ "usb, lan, wifi, mon_zew) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?"
								+ ", ?, ?, ?)";
	public static final String UPDATE_LAPTOPYNATESTY = "INSERT INTO laptopynatesty (`Id_testy`, `Id_laptopa`) "
								+ "VALUES (?, ?)";
	public static final String UPDATE_WYPOSAZENIE = "UPDATE wyposazenie SET `HDD`=?, `DVD`=?, `Bateria`=?, `Zasilacz`=?, `Kabel_ac`=?, `Torba`=?, `Karton`=?, `Wyp_dodatkowe`=?, `Brak`=? WHERE id_wyposazenie=?"; 
	public static final String UPDATE_LAPTOP = "UPDATE laptopy SET `Id_klienta`=?, `model`=?, `RMA`=?, opis_naprawy=?, opis_usterki = ?, uwagi = ? , rodzaj_naprawy = ? WHERE `Id_laptopa`=?";
	
	public static final String ADD_PRACOWNIK = "INSERT INTO pracownicy (`Login`, `Id_stanowiska`, `Haslo`, `salt`, `Imie`, `Telefon`) VALUES (?, ?, ?, ?, ?, ?)";
	public static final String GET_PRACOWNIK = "SELECT * FROM pracownicy WHERE login=?";
	public static final String GET_PRACOWNIK_IMIE = "SELECT imie FROM pracownicy WHERE login=?";
	public static final String GET_PRACOWNIK_SALT = "SELECT salt FROM pracownicy WHERE login=?";
	public static final String GET_PRACOWNIK_STANOWISKO = "SELECT Nazwa_stanowiska FROM stanowisko WHERE Id_stanowiska = ?";
	public static final String GET_PRACOWNICY = "SELECT id_stanowiska, login, haslo, imie, telefon, salt FROM pracownicy WHERE (login LIKE ? OR imie LIKE ?) ORDER BY login ASC";
	public static final String GET_STANOWISKO_ID = "SELECT Id_stanowiska FROM stanowisko WHERE Nazwa_stanowiska = ?";
	public static final String GET_STANOWISKO_NAZWA_ALL = "SELECT Nazwa_stanowiska FROM stanowisko";
	public static final String DELETE_PRACOWNIK = "DELETE FROM pracownicy WHERE login=?";
	public static final String UPDATE_PRACOWNIK = "UPDATE pracownicy SET `Login`=?, `Id_stanowiska`=?, `Haslo`=?, `salt`=?, `Imie`=?, `Telefon`=? WHERE login=?";
	
	public static final String GET_CURRENT_DATE = "SELECT CURDATE();";
	
	public static final String GET_REPAIR_COUNT = "select count(distinct id_laptopa) AS ilosc from naprawy where id_status=? and data between ? and ? and login=?";
	public static final String GET_REPAIR_COUNT_ALL = "select count(distinct id_laptopa) AS ilosc from naprawy where id_status=? and data between ? and ?";
	public static final String GET_STAN_TEKST_ALL = "SELECT id_stantekst, stan FROM stantekst";
	
}

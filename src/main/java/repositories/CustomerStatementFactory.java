package repositories;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.Logger;

import dataproviders.Database;
import dataproviders.LoggedUser;
import models.Customer;

class CustomerStatementFactory
{
	private static final Logger LOG = org.apache.logging.log4j.LogManager.getLogger( "src" );
	
	public PreparedStatement hasHardware(Customer customer) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.KLIENT_HAS_HARDWARE);
		preStmt.setInt(1, customer.getId());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement nameIsAvailable(String name) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.KLIENT_NAME_CHECK);
		preStmt.setString(1, name);
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement add(Customer customer, int addressId) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.ADD_KLIENT, 
				Statement.RETURN_GENERATED_KEYS);
		preStmt.setBoolean(1, customer.isFirma());
		preStmt.setString(2, customer.getName());
		preStmt.setString(3, customer.getNIP());
		preStmt.setString(4, customer.getNotes());
		preStmt.setString(5, LoggedUser.getLogin());
		preStmt.setInt(6, addressId);
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement addAddress(Customer customer) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.ADD_KLIENT_ADRES, Statement.RETURN_GENERATED_KEYS);
		preStmt.setString(1, customer.getAddress().getCity());
		preStmt.setString(2, customer.getAddress().getStreet());
		preStmt.setString(3, customer.getAddress().getZipcode());
		preStmt.setString(4, customer.getAddress().getPhone());
		preStmt.setString(5, customer.getEmail());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement findByNameOrCity(String searchedPhrase) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.SEARCH_FOR_KLIENT);
		preStmt.setString(1, "%" + searchedPhrase + "%");
		preStmt.setString(2, "%" + searchedPhrase + "%");
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getById(int id) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.GET_KLIENT);
		preStmt.setInt(1, id);
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getAddress(Customer customer) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.GET_KLIENT_ADRES);
		preStmt.setInt(1, customer.getAddressID());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement update(Customer customer) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.UPDATE_KLIENT);
		preStmt.setBoolean(1, customer.isFirma());
		preStmt.setString(2, customer.getName());
		preStmt.setString(3, customer.getNIP());
		preStmt.setString(4, customer.getNotes());
		preStmt.setInt(5, customer.getId());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement updateAddress(Customer customer) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.UPDATE_KLIENT_ADRES);		
		preStmt.setString(1, customer.getAddress().getCity());
		preStmt.setString(2, customer.getAddress().getStreet());
		preStmt.setString(3, customer.getAddress().getZipcode());
		preStmt.setString(4, customer.getAddress().getPhone());
		preStmt.setString(5, customer.getAddress().getEmail());
		preStmt.setInt(6, customer.getAddressID());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement delete(Customer customer) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.DELETE_KLIENT);
		preStmt.setInt(1, customer.getId());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement deleteAddress(int id) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement(SQLStatements.DELETE_KLIENT_ADRES);
		preStmt.setInt(1, id);
		LOG.trace( preStmt.toString() );
		return preStmt;
	}

}

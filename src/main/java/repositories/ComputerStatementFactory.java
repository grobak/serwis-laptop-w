package repositories;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import components.TestComponent;
import dataproviders.LoggedUser;
import dataproviders.SQLConsumer;
import models.Computer;
import models.Equipment;
import models.RepairStatus;

public class ComputerStatementFactory extends StatementFactory{
	
	public PreparedStatement numberOfCompletedTests(Computer computer) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement(SQLStatements.GET_NUMBER_OF_TESTS);
		preStmt.setInt(1, computer.getId());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getComputerStatus(Computer computer) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.GET_STAN_LAPTOPA );
		preStmt.setInt( 1, computer.getId() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement addedBy(int computerID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement(SQLStatements.KTO_PRZYJAL);
		preStmt.setInt(1, computerID);
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement updateTests(TestComponent testy, int id) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement(SQLStatements.UPDATE_TESTY, Statement.RETURN_GENERATED_KEYS);
		preStmt.setInt(1, testy.getHdd().getSelected());
		preStmt.setInt(2, testy.getDvd().getSelected());
		preStmt.setInt(3, testy.getBateria().getSelected());
		preStmt.setInt(4, testy.getZasilacz().getSelected());
		preStmt.setInt(5, testy.getLadowanie().getSelected());
		preStmt.setInt(6, testy.getBluetooth().getSelected());
		preStmt.setInt(7, testy.getGlosniki().getSelected());
		preStmt.setInt(8, testy.getSluchawki().getSelected());
		preStmt.setInt(9, testy.getMikrofon().getSelected());
		preStmt.setInt(10, testy.getKlawiatura().getSelected());
		preStmt.setInt(11, testy.getKamera().getSelected());
		preStmt.setInt(12, testy.getTouchpad().getSelected());
		preStmt.setInt(13, testy.getUsb().getSelected());
		preStmt.setInt(14, testy.getLan().getSelected());
		preStmt.setInt(15, testy.getWifi().getSelected());
		preStmt.setInt(16, testy.getMonitorZew().getSelected());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement applyTest(int testID, int id) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement(SQLStatements.UPDATE_LAPTOPYNATESTY);
		preStmt.setInt(1, testID);
		preStmt.setInt(2, id);
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement updateEquipment(Equipment equipment) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement(SQLStatements.UPDATE_WYPOSAZENIE);
		preStmt.setBoolean(1, equipment.isHDD());
		preStmt.setBoolean(2, equipment.isDVD());
		preStmt.setBoolean(3, equipment.isBateria());
		preStmt.setBoolean(4, equipment.isZasilacz());
		preStmt.setBoolean(5, equipment.isKabelAC());
		preStmt.setBoolean(6, equipment.isTorba());
		preStmt.setBoolean(7, equipment.isKarton());
		preStmt.setString(8, equipment.getWypDodatkowe());
		preStmt.setString(9, equipment.getBrak());
		preStmt.setInt(10, equipment.getId());
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement update(Computer computer) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.UPDATE_LAPTOP );
		preStmt.setInt( 1, computer.getIdKlienta() );
		preStmt.setString( 2, computer.getModel() );
		preStmt.setString( 3, computer.getRMA() );
		preStmt.setString( 4, computer.getOpisNaprawy() );
		preStmt.setString( 5, computer.getOpisUsterki() );
		preStmt.setString( 6, computer.getUwagi() );
		preStmt.setString( 7, computer.getRodzajNaprawy() );
		preStmt.setInt( 8, computer.getId() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement add(Computer computer, int equipmentID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.ADD_LAPTOP, Statement.RETURN_GENERATED_KEYS );
		preStmt.setInt( 1, computer.getIdKlienta() );
		preStmt.setString( 2, computer.getModel() );
		preStmt.setString( 3, computer.getRMA() );
		preStmt.setString( 4, computer.getUwagi() );
		preStmt.setString( 5, computer.getOpisUsterki() );
		preStmt.setInt( 6, equipmentID );
		preStmt.setString( 7, computer.getRodzajNaprawy() );
		preStmt.setString( 8, computer.getOpisNaprawy() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement addRepair(Computer computer, RepairStatus status) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.ADD_NAPRAWA );
		preStmt.setInt( 1, computer.getId() );
		preStmt.setString( 2, LoggedUser.getLogin() );
		preStmt.setInt( 3, status.getValue() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement setShipmentDate(Computer computer) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.SET_DATA_WYSYLKI );
		preStmt.setInt( 1, computer.getId() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement myComputers( String searchText ) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.GET_MOJE_LAPTOPY );
		preStmt.setString( 1, LoggedUser.getLogin() );
		preStmt.setString( 2, "%" + searchText + "%" );
		preStmt.setString( 3, "%" + searchText + "%" );
		preStmt.setString( 4, "%" + searchText + "%" );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement listByStatus( int idStatus, String searchCondition ) throws SQLException
	{
		PreparedStatement preStmt;
		if( searchCondition.trim().isEmpty() )
		{
			preStmt = prepareStatement( SQLStatements.LIST_REPAIRS_BY_STATUS );
			preStmt.setInt( 1, idStatus );
		}
		else
		{
			preStmt = prepareStatement( SQLStatements.LIST_REPAIRS_BY_STATUS_SEARCH );
			preStmt.setString( 1, "%" + searchCondition + "%" );
			preStmt.setString( 2, "%" + searchCondition + "%" );
			preStmt.setString( 3, "%" + searchCondition + "%" );
			preStmt.setInt( 4, idStatus );
		}		
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement addEquipment(Equipment equipment) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.ADD_WYPOSAZENIE, Statement.RETURN_GENERATED_KEYS );
		preStmt.setBoolean( 1, equipment.isHDD() );
		preStmt.setBoolean( 2, equipment.isDVD() );
		preStmt.setBoolean( 3, equipment.isBateria() );
		preStmt.setBoolean( 4, equipment.isZasilacz() );
		preStmt.setBoolean( 5, equipment.isKabelAC() );
		preStmt.setBoolean( 6, equipment.isTorba() );
		preStmt.setBoolean( 7, equipment.isKarton() );
		preStmt.setString( 8, equipment.getWypDodatkowe() );
		preStmt.setString( 9, equipment.getBrak() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement find( String searchCondition ) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.SEARCH_FOR_SPRZET );
		preStmt.setString( 1, "%" + searchCondition + "%" );
		preStmt.setString( 2, "%" + searchCondition + "%" );
		preStmt.setString( 3, "%" + searchCondition + "%" );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getShipmentDate(int computerID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.GET_DATA_WYSYLKI );
		preStmt.setInt( 1, computerID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getTests(int computerID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.GET_TESTY );
		preStmt.setInt( 1, computerID );
		preStmt.setInt( 2, computerID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getComputerById(int computerID) throws SQLException
	{
            PreparedStatement preStmt = prepareStatement(SQLStatements.GET_LAPTOP);
            preStmt.setInt(1, computerID);
            LOG.trace( preStmt.toString() );
            return preStmt;
	}
	
	public PreparedStatement getEquipment(Computer computer) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.GET_WYPOSAZENIE );
		preStmt.setInt( 1, computer.getEquipment().getId() );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement deleteRepair(int equipmentID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.DELETE_NAPRAWA );
		preStmt.setInt( 1, equipmentID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public List<PreparedStatement> delete(int equipmentID) throws SQLException
	{
		String[] stmts = new String[] 
		{
			 SQLStatements.DELETE_NAPRAWA,
			 SQLStatements.DELETE_TESTY,
			 SQLStatements.DELETE_LAPTOPYNATESTY,
			 SQLStatements.DELETE_LAPTOPA,
			 SQLStatements.DELETE_WYPOSAZENIE
		};
		List<PreparedStatement> list = Arrays.asList(stmts)
				.stream()
				.map( (SQLFunction<String,PreparedStatement>) this::prepareStatement)
				.collect(Collectors.toList());
		list.forEach( (SQLConsumer<PreparedStatement>) stmt -> stmt.setInt( 1, equipmentID ) );
		
		return list;
	}
	
	@FunctionalInterface
	private interface SQLFunction<T,R> extends Function<T,R> {

		default R apply(T t){
			try
			{
				return acceptWithThrow(t);
			}
			catch( SQLException e )
			{
				org.apache.logging.log4j.LogManager.getLogger( "src" ).error( e, e );
				throw new RuntimeException(e);
			}
		};
		R acceptWithThrow(T t) throws SQLException;
	}
		
	
	public PreparedStatement deleteComputerTests(int equipmentID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.DELETE_TESTY );
		preStmt.setInt( 1, equipmentID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement deleteComputerTestsData(int equipmentID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.DELETE_LAPTOPYNATESTY );
		preStmt.setInt( 1, equipmentID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement deleteComputer(int equipmentID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.DELETE_LAPTOPA );
		preStmt.setInt( 1, equipmentID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement deleteEquipment(int equipmentID) throws SQLException
	{
		PreparedStatement preStmt = prepareStatement( SQLStatements.DELETE_WYPOSAZENIE );
		preStmt.setInt( 1, equipmentID );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
		
}

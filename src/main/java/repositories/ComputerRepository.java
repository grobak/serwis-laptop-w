package repositories;

import components.TestComponent;
import dataproviders.Database;
import dataproviders.SQLConsumer;
import java.awt.Toolkit;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import models.Computer;
import models.ComputerStatus;
import models.Equipment;
import models.RepairStatus;

public class ComputerRepository extends Repository {

    private final ComputerStatementFactory statements = new ComputerStatementFactory();

    public int numberOfCompletedTests(Computer computer) {
        int numberOfTests = 0;
        try (PreparedStatement preStmt = statements.numberOfCompletedTests(computer);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                numberOfTests = result.getInt("completed_tests");
            }
        } catch (SQLException e) {
            showError(e);
        }
        return numberOfTests;
    }

    public ComputerStatus getComputerStatus(Computer computer) {
        ComputerStatus status = new ComputerStatus();
        try (PreparedStatement preStmt = statements.getComputerStatus(computer);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                status.setId(result.getInt("Id_testy"));
                status.setHDD(result.getInt("HDD"));
                status.setDVD(result.getInt("DVD"));
                status.setBateria(result.getInt("Bateria"));
                status.setZasilacz(result.getInt("Zasilacz"));
                status.setLadowanie(result.getInt("Ladowanie"));
                status.setBluetooth(result.getInt("Bluetooth"));
                status.setGlosniki(result.getInt("Glosniki"));
                status.setSluchawki(result.getInt("Sluchawki"));
                status.setMikrofon(result.getInt("Mikrofon"));
                status.setKlawiatura(result.getInt("Klawiatura"));
                status.setKamera(result.getInt("Kamera"));
                status.setTouchpad(result.getInt("Touchpad"));
                status.setUSB(result.getInt("USB"));
                status.setLAN(result.getInt("LAN"));
                status.setWiFi(result.getInt("Wifi"));
                status.setMonitorZew(result.getInt("Mon_zew"));
            }
        } catch (SQLException e) {
            showError(e);
        }
        return status;
    }

    public String addedBy(int computerID) {
        String addedByUser = "Brak danych";

        try (PreparedStatement preStmt = statements.addedBy(computerID);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                addedByUser = result.getString(1);
            }
        } catch (SQLException e) {
            showError(e);
        }
        return addedByUser;
    }

    public void updateTests(TestComponent testy, int id) {
        int idTest = 0;
        try {
            Database.INSTANCE.setAutoCommit(false);

            try (PreparedStatement preStmt = statements.updateTests(testy, idTest)) {
                preStmt.executeUpdate();

                try (ResultSet result = preStmt.getGeneratedKeys()) {
                    while (result.next()) {
                        idTest = result.getInt(1);
                    }
                }
            }
            try (PreparedStatement preStmt = statements.applyTest(idTest, id)) {
                preStmt.executeUpdate();
                Database.INSTANCE.commit();
                Database.INSTANCE.setAutoCommit(true);
            }

        } catch (SQLException e) {
            showError(e);
            Database.INSTANCE.rollback();
        }
    }

    private void updateEquipment(Equipment equipment) {
        try (PreparedStatement preStmt = statements.updateEquipment(equipment)) {
            preStmt.executeUpdate();
        } catch (SQLException e) {
            showError(e);
        }
    }

    public void update(Computer computer) {
        updateEquipment(computer.getEquipment());
        try (PreparedStatement preStmt = statements.update(computer)) {
            preStmt.executeUpdate();
            Toolkit.getDefaultToolkit().beep();
        } catch (SQLException e) {
            showError(e);
        }
    }

    public int add(Computer computer) {
        int equipmentID = addEquipment(computer.getEquipment());
        int computerID = 0;
        try (PreparedStatement preStmt = statements.add(computer, equipmentID)) {
            preStmt.executeUpdate();
            try (ResultSet result = preStmt.getGeneratedKeys()) {
                while (result.next()) {
                    computerID = result.getInt(1);
                }
            }
        } catch (SQLException e) {
            showError(e);
        }
        setStatus(new Computer(computer, equipmentID), RepairStatus.PRZYJETY);
        computer.setIdLaptopa(computerID);

        Toolkit.getDefaultToolkit().beep();
        JOptionPane.showMessageDialog(null, "Dodano nowy sprzęt", "Powodzenie", JOptionPane.INFORMATION_MESSAGE);
        return computerID;
    }

    public void setStatus(Computer computer, RepairStatus status) {
        try {
            Database.INSTANCE.setAutoCommit(false);

            try (PreparedStatement preStmt = statements.addRepair(computer, status)) {
                preStmt.executeUpdate();
            }
            if (status == RepairStatus.WYDANY) {
                try (PreparedStatement preStmt = statements.setShipmentDate(computer)) {
                    preStmt.executeUpdate();
                }
            }
            Database.INSTANCE.commit();
            Database.INSTANCE.setAutoCommit(true);
        } catch (SQLException e) {
            Database.INSTANCE.rollback();
            showError(e);
        }
    }

    public List<Computer> myComputers(String searchText) {
        List<Computer> computers = new ArrayList<>();
        try (PreparedStatement preStmt = statements.myComputers(searchText);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                Computer sprzetSQL = new Computer();
                sprzetSQL.setIdLaptopa(result.getInt("Id_laptopa"));
                sprzetSQL.setIdKlienta(result.getInt("Id_klienta"));
                sprzetSQL.setModel(result.getString("model"));
                sprzetSQL.setRMA(result.getString("rma"));
                String data = result.getString("Data_przyjecia");
                sprzetSQL.setDataPrzyjecia(data != null ? data.substring(0, 16) : data);
                data = result.getString("Data_wysylki");
                sprzetSQL.setDataWysylki(data != null ? data.substring(0, 16) : data);
                sprzetSQL.setOpisUsterki(result.getString("Opis_usterki"));
                sprzetSQL.setUwagi(result.getString("Uwagi"));
                data = result.getString("data");
                sprzetSQL.setStatusData(data != null ? data.substring(0, 16) : data);

                computers.add(0, sprzetSQL);
            }
        } catch (SQLException e) {
            showError(e);
        }
        return computers;
    }

    public List<Computer> listByStatus(int idStatus, String searchCondition) {
        List<Computer> computers = new ArrayList<>();

        try (PreparedStatement preStmt = statements.listByStatus(idStatus, searchCondition);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                Computer sprzetSQL = new Computer();
                sprzetSQL.setIdLaptopa(result.getInt("Id_laptopa"));
                sprzetSQL.setIdKlienta(result.getInt("Id_klienta"));
                sprzetSQL.setModel(result.getString("model"));
                sprzetSQL.setRMA(result.getString("rma"));
                String data = result.getString("Data_przyjecia");
                sprzetSQL.setDataPrzyjecia(data != null ? data.substring(0, 16) : data); // .substring(0,
                // 16));
                data = result.getString("Data_wysylki");
                sprzetSQL.setDataWysylki(data != null ? data.substring(0, 16) : data);
                sprzetSQL.setOpisUsterki(result.getString("Opis_usterki"));
                sprzetSQL.setUwagi(result.getString("Uwagi"));
                data = result.getString("dat");
                sprzetSQL.setStatusData(data != null ? data.substring(0, 16) : data);

                computers.add(0, sprzetSQL);
            }
        } catch (SQLException e) {
            showError(e);
        }

        return computers;
    }

    public int addEquipment(Equipment equipment) {
        int id = 0;
        try (PreparedStatement preStmt = statements.addEquipment(equipment)) {
            preStmt.executeUpdate();

            try (ResultSet result = preStmt.getGeneratedKeys()) {
                while (result.next()) {
                    id = result.getInt(1);
                }
            }

        } catch (SQLException e) {
            showError(e);
        }
        return id;
    }

    public List<Computer> find(String searchCondition) {
        List<Computer> computers = new ArrayList<>();

        try (PreparedStatement preStmt = statements.find(searchCondition);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                Computer sprzetSQL = new Computer();
                sprzetSQL.setIdLaptopa(result.getInt("Id_laptopa"));
                sprzetSQL.setIdKlienta(result.getInt("Id_klienta"));
                sprzetSQL.setModel(result.getString("model"));
                sprzetSQL.setRMA(result.getString("RMA"));
                sprzetSQL.setDataPrzyjecia(result.getString("Data_przyjecia") != null
                        ? result.getString("Data_przyjecia").substring(0, 16)
                        : result.getString("Data_przyjecia"));// .substring(0,
                // 16));
                sprzetSQL.setStatusData(result.getString("last_status"));

                computers.add(0, sprzetSQL);
            }

        } catch (SQLException e) {
            showError(e);
        }

        return computers;
    }

    public String getShipmentDate(int computerID) {
        try (PreparedStatement preStmt = statements.getShipmentDate(computerID);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                return result.getString("Data_wysylki") != null
                        ? result.getString("Data_wysylki").substring(0, 16)
                        : result.getString("Data_wysylki");
            }
        } catch (SQLException e) {
            showError(e);
        }

        return "";
    }

    public TestComponent getTests(int computerID) {
        TestComponent testy = new TestComponent();
        try (PreparedStatement preStmt = statements.getTests(computerID);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                testy.setIdLaptopa(result.getInt("Id_testy"));
                testy.getHdd().select(result.getInt("HDD"));
                testy.getDvd().select(result.getInt("DVD"));
                testy.getBateria().select(result.getInt("Bateria"));
                testy.getZasilacz().select(result.getInt("Zasilacz"));
                testy.getLadowanie().select(result.getInt("Ladowanie"));
                testy.getBluetooth().select(result.getInt("Bluetooth"));
                testy.getGlosniki().select(result.getInt("Glosniki"));
                testy.getSluchawki().select(result.getInt("Sluchawki"));
                testy.getMikrofon().select(result.getInt("Mikrofon"));
                testy.getKlawiatura().select(result.getInt("Klawiatura"));
                testy.getKamera().select(result.getInt("Kamera"));
                testy.getTouchpad().select(result.getInt("Touchpad"));
                testy.getUsb().select(result.getInt("USB"));
                testy.getLan().select(result.getInt("LAN"));
                testy.getWifi().select(result.getInt("Wifi"));
                testy.getMonitorZew().select(result.getInt("Mon_zew"));
            }
        } catch (SQLException e) {
            showError(e);
        }
        return testy;
    }

    public Computer getComputerByID(int computerID) {
        Computer sprzetSQL = new Computer();
        try (PreparedStatement preStmt = statements.getComputerById(computerID);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                sprzetSQL.setIdLaptopa(result.getInt("Id_laptopa"));
                sprzetSQL.setIdKlienta(result.getInt("Id_klienta"));
                sprzetSQL.setEquipment(new Equipment(result.getInt("Id_wyposazenie")));
                sprzetSQL.setModel(result.getString("model"));
                sprzetSQL.setRMA(result.getString("RMA"));
                sprzetSQL.setDataPrzyjecia(result.getString("Data_przyjecia").substring(0, 16));
                sprzetSQL.setDataWysylki(result.getString("Data_wysylki") != null
                        ? result.getString("Data_wysylki").substring(0, 16)
                        : result.getString("Data_wysylki"));
                sprzetSQL.setOpisNaprawy(result.getString("opis_naprawy"));
                sprzetSQL.setOpisUsterki(result.getString("Opis_usterki"));
                sprzetSQL.setUwagi(result.getString("Uwagi"));
                sprzetSQL.setRodzajNaprawy(result.getString("Rodzaj_naprawy"));
            }
        } catch (SQLException e) {
            showError(e);
        }
        sprzetSQL.setEquipment(getEquipment(sprzetSQL));
        sprzetSQL.setModel(sprzetSQL.getModel());
        return sprzetSQL;
    }

    public Equipment getEquipment(Computer computer) {
        Equipment equipment = new Equipment();
        try (PreparedStatement preStmt = statements.getEquipment(computer);
                ResultSet result = preStmt.executeQuery();) {
            while (result.next()) {
                equipment.setEquipmentID(result.getInt("Id_wyposazenie"));
                equipment.setHDD(result.getBoolean("HDD"));
                equipment.setDVD(result.getBoolean("DVD"));
                equipment.setBateria(result.getBoolean("Bateria"));
                equipment.setZasilacz(result.getBoolean("Zasilacz"));
                equipment.setKabelAC(result.getBoolean("Kabel_AC"));
                equipment.setTorba(result.getBoolean("Torba"));
                equipment.setKarton(result.getBoolean("Karton"));
                equipment.setWypDodatkowe(result.getString("Wyp_dodatkowe"));
                equipment.setBrak(result.getString("Brak"));
            }
        } catch (SQLException e) {
            showError(e);
        }
        return equipment;
    }

    public void delete(int equipmentID) {
        try {
            Database.INSTANCE.setAutoCommit(false);
            List<PreparedStatement> preStmts = statements.delete(equipmentID);
            preStmts.forEach((SQLConsumer<PreparedStatement>) stmt -> {
                stmt.executeUpdate();
                stmt.close();
            });
            Database.INSTANCE.commit();
            Database.INSTANCE.setAutoCommit(true);
        } catch (RuntimeException | SQLException e) {
            Database.INSTANCE.rollback();
            showError(e);
        }

        Toolkit.getDefaultToolkit().beep();
        JOptionPane.showMessageDialog(null, "Usunięto sprzęt", "Powodzenie", JOptionPane.INFORMATION_MESSAGE);
    }

}

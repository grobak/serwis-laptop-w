package repositories;

import javax.swing.JOptionPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Repository {

    protected static final Logger LOG = LogManager.getLogger("src");

    protected Repository() {
        //class designed to be uninstantable
    }

    protected void showError(Exception e) {
        JOptionPane.showMessageDialog(null, e, "Błąd", JOptionPane.ERROR_MESSAGE);
        LOG.error(e, e);
    }

}

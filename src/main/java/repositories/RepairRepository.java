package repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Naprawa;

public class RepairRepository extends Repository
{

	private final RepairStatementFactory statements = new RepairStatementFactory(); 
	
	public List<String> getRepairStatusNames()
	{
		List<String> repairStatuses = new ArrayList<>();
		try( PreparedStatement preStmt = statements.getRepairStatusNames();
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
				repairStatuses.add( result.getString( 1 ) );

			repairStatuses.add( 0, "" );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return repairStatuses;
	}

	public String getRepairStatus( int id )
	{
		String status = "";
		try( PreparedStatement preStmt = statements.getRepairStatus( id );
			 ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
				status = result.getString( 1 );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return status;
	}

	public String getStatusName( int id )
	{
		String stanTekst = "";
		try( PreparedStatement preStmt = statements.getStatusName( id );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
				stanTekst = result.getString( 1 );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return stanTekst;
	}

	public List<Naprawa> getRepairs( int computerId )
	{
		List<Naprawa> statusy = new ArrayList<>();
		try( PreparedStatement preStmt = statements.getRepairs( computerId );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
			{
				Naprawa naprawa = new Naprawa( result.getString( "imie" ), result.getInt( "Id_status" ),
						result.getString( "Data" ) );
				statusy.add( naprawa );
			}
		}
		catch( Exception e )
		{
			showError( e );
		}
		return statusy;
	}

	public String getOwnerName( int ownerId )
	{
		String ownerName = "";
		try( PreparedStatement preStmt = statements.getOwnerName( ownerId );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
				ownerName += result.getString( 1 );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return ownerName;
	}

	public String getNumberOfRepairs( int idStatus, String dataOd, String dataDo, String pracownik )
	{
		String ilosc = "";
		try( PreparedStatement preStmt = statements.getNumberOfRepairs( idStatus, dataOd, dataDo, pracownik );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
				ilosc = result.getString( "ilosc" );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return ilosc;
	}
	
	public Map<Integer, String> getAllStatusNames()
	{
		Map<Integer, String> resultMap = new HashMap<>();
		try( PreparedStatement preStmt = statements.getAllStatusNames();
			 ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
				resultMap.put( result.getInt( "id_stantekst" ), result.getString( "stan" ) );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return resultMap;
	}
}

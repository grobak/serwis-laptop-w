package repositories;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.logging.log4j.Logger;

import dataproviders.Database;

public class RepairStatementFactory
{
	private static final Logger LOG = org.apache.logging.log4j.LogManager.getLogger( "src" );
	
	public PreparedStatement getRepairStatusNames() throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_NAZWY_STATUSOW );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getAllStatusNames() throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_STAN_TEKST_ALL );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getRepairStatus( int id ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_STATUS_LAPTOPA_BY_ID );
		preStmt.setInt( 1, id );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getRepairs( int computerId ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_NAPRAWY_BY_LAPTOP_ID );
		preStmt.setInt( 1, computerId );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	
	public PreparedStatement getStatusName( int id ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_STAN_TEKST );
		preStmt.setInt( 1, id );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	
	public PreparedStatement getOwnerName( int ownerId ) throws SQLException
	{
		PreparedStatement preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_KLIENT_NAZWA_BY_ID );
		preStmt.setInt( 1, ownerId );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}
	public PreparedStatement getNumberOfRepairs( int idStatus, String dataOd, String dataDo, String pracownik ) throws SQLException
	{
		PreparedStatement preStmt; 
		if( pracownik.equals( "Wszyscy" ) )
			preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_REPAIR_COUNT_ALL );
		else
			preStmt = Database.INSTANCE.getConnection().prepareStatement( SQLStatements.GET_REPAIR_COUNT );
		preStmt.setInt( 1, idStatus );
		preStmt.setString( 2, dataOd );
		preStmt.setString( 3, dataDo );
		if( !pracownik.equals( "Wszyscy" ) )
			preStmt.setString( 4, pracownik );
		LOG.trace( preStmt.toString() );
		return preStmt;
	}

}

package repositories;

import dataproviders.EncryptedPassword;
import dataproviders.PasswordGenerator;
import java.awt.Toolkit;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import models.Employee;

public class EmployeeRepository extends Repository
{
	EmployeeStatementFactory statements = new EmployeeStatementFactory();
	
	public Employee get( String login )
	{
		Employee pracownikSQL = new Employee();

		try( PreparedStatement preStmt = statements.get( login );
			 ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
			{
				pracownikSQL.setPositionId( result.getInt( "Id_stanowiska" ) );
				pracownikSQL.setLogin( result.getString( "Login" ) );
				pracownikSQL.setHaslo( result.getString( "Haslo" ) );
				pracownikSQL.setSalt( result.getString( "salt" ) );
				pracownikSQL.setImie( result.getString( "Imie" ) );
				pracownikSQL.setTelefon( result.getString( "Telefon" ) );
			}
		}
		catch( SQLException e )
		{
			showError( e );
		}
		return pracownikSQL;
	}

	public void add( Employee employee )
	{
		
		PasswordGenerator generator = new PasswordGenerator();
            EncryptedPassword password = null;
            try {
                password = generator.encrypt(employee.getHaslo());
            } catch (NoSuchAlgorithmException ex) {
                showError(ex);
            }

		try( PreparedStatement preStmt = statements.add( employee, password ) )
		{	
			preStmt.executeUpdate();

			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog( null, "Dodano nowego pracownika", "Powodzenie",
					JOptionPane.INFORMATION_MESSAGE );

		}
		catch( SQLException e )
		{
			if( e.getMessage().contains( "Duplicate entry" ) )
				JOptionPane.showMessageDialog( null,
						"Pracownik o tej nazwie użytkownika już istnieje.\nProszę wybrać inną nazwę.", "Błąd",
						JOptionPane.ERROR_MESSAGE );
			else
				showError( e );
		}
	}

	public void delete( String login )
	{
		try( PreparedStatement preStmt = statements.delete( login ))
		{
			preStmt.executeUpdate();

			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog( null, "Usunięto pracownika", "Powodzenie",
					JOptionPane.INFORMATION_MESSAGE );
		}
		catch( SQLException e )
		{
			if( e.getMessage().contains( "Cannot delete" ) )
				JOptionPane.showMessageDialog( null,
						"Do pracownika przypisane są naprawy.\nNie można usunąć pracownika.", "Błąd",
						JOptionPane.ERROR_MESSAGE );
			else
				showError( e );
		}
	}

	public void update( String login, Employee pracownik ) 
	{

		try( PreparedStatement preStmt = statements.update( login, pracownik ) )
		{
			preStmt.executeUpdate();

			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog( null, "Zmodyfikowano pracownika", "Powodzenie",
					JOptionPane.INFORMATION_MESSAGE );
		}
		catch( SQLException e )
		{
			showError( e );
		}
	}

	public String getPositionName( int positionID )
	{
		String stanowiska = "";
		try( PreparedStatement preStmt = statements.getPositionName( positionID );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
				stanowiska = result.getString( "Nazwa_stanowiska" );
		}
		catch( SQLException e )
		{
			showError( e );
		}

		return stanowiska;
	}

	public int getPositionId( String name )
	{
		int positionID = 0;
		try( PreparedStatement preStmt = statements.getPositionId( name );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
				positionID = result.getInt( "Id_stanowiska" );
		}
		catch( SQLException e )
		{
			showError( e );
		}
		
		return positionID;
	}

	public List<Employee> findByLoginOrName( String searchCondition )
	{
		List<Employee> employees = new ArrayList<>();
		
		try( PreparedStatement preStmt = statements.findByLoginOrName( searchCondition );
			 ResultSet result = preStmt.executeQuery();)
		{
			while( result.next() )
			{
				Employee employee = new Employee();
				employee.setPositionId( result.getInt( "Id_stanowiska" ) );
				employee.setLogin( result.getString( "Login" ) );
				employee.setHaslo( result.getString( "Haslo" ) );
				employee.setSalt( result.getString( "salt" ) );
				employee.setImie( result.getString( "Imie" ) );
				employee.setTelefon( result.getString( "Telefon" ) );

				employees.add( employee );
			}
		}
		catch( SQLException e )
		{
			showError( e );
		}
		return employees;
	}
}

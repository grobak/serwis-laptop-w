package repositories;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.logging.log4j.Logger;

import dataproviders.Database;

public abstract class StatementFactory
{
	protected static final Logger LOG = org.apache.logging.log4j.LogManager.getLogger( "src" );

	protected PreparedStatement prepareStatement(String sqlStatement) throws SQLException
	{
		return Database.INSTANCE.getConnection().prepareStatement( sqlStatement );
	}
	
	protected PreparedStatement prepareStatement(String sqlStatement, int option) throws SQLException
	{
		return Database.INSTANCE.getConnection().prepareStatement( sqlStatement, option );
	}
}

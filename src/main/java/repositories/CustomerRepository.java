package repositories;

import java.awt.Toolkit;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import dataproviders.Database;
import models.Customer;
import models.Customer.CustomerBuilder;
import models.CustomerAddress;
import models.CustomerAddress.AddressBuilder;

public class CustomerRepository extends Repository
{
	private final CustomerStatementFactory statements = new CustomerStatementFactory();

	public boolean hasHardware( Customer klient )
	{
		try( PreparedStatement preStmt = statements.hasHardware( klient ) )
		{
			return preStmt.executeQuery().next();
		}
		catch( SQLException e )
		{
			LOG.trace( e, e );
		}
		return false;
	}

	public boolean nameIsAvailable( String name )
	{
		try( PreparedStatement preStmt = statements.nameIsAvailable( name ) )
		{
			return !preStmt.executeQuery().next();
		}
		catch( SQLException e )
		{
			Database.INSTANCE.rollback();
			showError( e );
		}
		return false;
	}

	public void add( Customer customer )
	{
		int addressId;
		try
		{
			Database.INSTANCE.setAutoCommit( false );

			addressId = addAddress( customer );

			try( PreparedStatement preStmt = statements.add( customer, addressId ) )
			{
				preStmt.executeUpdate();

				Database.INSTANCE.commit();
				Database.INSTANCE.setAutoCommit( true );
			}
		}
		catch( SQLException e )
		{
			Database.INSTANCE.rollback();
			showError( e );
		}

		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog( null, "Dodano nowego klienta", "Powodzenie", JOptionPane.INFORMATION_MESSAGE );
	}

	private int addAddress( Customer customer ) throws SQLException
	{
		try( PreparedStatement preStmt = statements.addAddress( customer ) )
		{
			preStmt.executeUpdate();
			try( ResultSet result = preStmt.getGeneratedKeys() )
			{
				result.next();
				return result.getInt( 1 );
			}
		}
	}

	public List<Customer> findByNameOrCity( String searchedPhrase )
	{
		List<Customer> customers = new ArrayList<>();

		try( PreparedStatement preStmt = statements.findByNameOrCity( searchedPhrase );
				ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
			{
				CustomerAddress address = new AddressBuilder().city( result.getString( "Miejscowosc" ) )
						.phone( result.getString( "Telefon" ) ).build();

				Customer sqlcustomer = new CustomerBuilder().id( result.getInt( "Id_klienta" ) )
						.name( result.getString( "Nazwa_klienta" ) ).address( address ).build();

				customers.add( 0, sqlcustomer );
			}
		}
		catch( SQLException e )
		{
			showError( e );
		}
		return customers;
	}

	public Customer getById( int id )
	{
		try( PreparedStatement preStmt = statements.getById( id ); ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
			{
				CustomerAddress adres = new AddressBuilder().id( result.getInt( "id_adres" ) ).build();

				Customer customer = new CustomerBuilder().id( result.getInt( "Id_klienta" ) )
						.isCompany( result.getBoolean( "Firma" ) ).name( result.getString( "Nazwa_klienta" ) )
						.nip( result.getString( "NIP" ) ).address( adres ).notes( result.getString( "Notatki" ) )
						.build();
				return new CustomerBuilder( customer ).address( getAddress( customer ) ).build();
			}

		}
		catch( SQLException e )
		{
			showError( e );
		}
		return new CustomerBuilder().build();
	}

	private CustomerAddress getAddress( Customer customer )
	{
		CustomerAddress address = CustomerAddress.EMPTY_ADDRESS;

		try( PreparedStatement preStmt = statements.getAddress( customer ); ResultSet result = preStmt.executeQuery(); )
		{
			while( result.next() )
			{
				address = new AddressBuilder().id( result.getInt( "Id_adres" ) )
						.city( result.getString( "Miejscowosc" ) ).street( result.getString( "Ulica" ) )
						.zipcode( result.getString( "Kod_pocztowy" ) ).phone( result.getString( "Telefon" ) )
						.email( result.getString( "Email" ) ).build();
			}

		}
		catch( SQLException e )
		{
			showError( e );
		}

		return address;
	}

	public void update( Customer customer )
	{
		updateAddress( customer );
		try( PreparedStatement preStmt = statements.update( customer ) )
		{
			preStmt.executeUpdate();

			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog( null, "Zmodyfikowano klienta", "Powodzenie",
					JOptionPane.INFORMATION_MESSAGE );
		}
		catch( SQLException e )
		{
			showError( e );
		}
	}

	private void updateAddress( Customer customer )
	{
		try( PreparedStatement preStmt = statements.updateAddress( customer ) )
		{
			preStmt.executeUpdate();
		}
		catch( SQLException e )
		{
			showError( e );
		}
	}

	public void delete( Customer customer )
	{
		deleteAddress( customer.getAddressID() );
		try( PreparedStatement preStmt = statements.delete( customer ) )
		{
			preStmt.executeUpdate();
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog( null, "Usunięto klienta", "Powodzenie", JOptionPane.INFORMATION_MESSAGE );
		}
		catch( SQLException e )
		{
			showError( e );
		}
	}

	private void deleteAddress( int id )
	{
		try( PreparedStatement preStmt = statements.deleteAddress( id ) )
		{
			preStmt.executeUpdate();
		}
		catch( SQLException e )
		{
			showError( e );
		}
	}

}

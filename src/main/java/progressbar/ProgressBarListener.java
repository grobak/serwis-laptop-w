package progressbar;

/**
 *
 * @author Grzesiek
 */
public interface ProgressBarListener {

    void start(boolean flag);
}

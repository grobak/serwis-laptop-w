package progressbar;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Grzesiek
 */
public interface ProgressController {

    CopyOnWriteArrayList<ProgressBarListener> OBSERVERS = new CopyOnWriteArrayList<>();

    default void addObserver(ProgressBarListener obs) {
        OBSERVERS.clear();
        OBSERVERS.add(obs);
    }

    default void removeObserver(ProgressBarListener obs) {
        OBSERVERS.remove(obs);
    }

    default void startProgressBar() {
        OBSERVERS.forEach(obs -> obs.start(true));
    }

    default void stopProgressBar() {
        OBSERVERS.forEach(obs -> obs.start(false));
    }

}

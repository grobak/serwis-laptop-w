package listeners;

import javax.swing.JPanel;

/**
 *
 * @author Grzesiek
 */
public interface PanelChooserListener {
    void setPanel(String title, JPanel panel);
}

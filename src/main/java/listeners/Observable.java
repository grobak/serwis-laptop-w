package listeners;

import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class Observable<T> implements IObservable<T> {

    CopyOnWriteArrayList<Listener<T>> observers = new CopyOnWriteArrayList<>();
    private final T frame;

    public Observable(T frame) {
        this.frame = frame;
    }

    /* (non-Javadoc)
     * @see interfaces.IObservable#addObserver(interfaces.Listener)
     */
    @Override
    public void addObserver(Listener<T> obs) {
        observers.add(obs);
    }

    /* (non-Javadoc)
     * @see interfaces.IObservable#removeObserver(interfaces.Listener)
     */
    @Override
    public void removeObserver(Listener<T> obs) {
        observers.remove(obs);
    }

    /* (non-Javadoc)
     * @see interfaces.IObservable#notifyObservers(java.util.Optional)
     */
    @Override
    public void notifyObservers(Optional<Object> arg) {
        observers.forEach(obs -> obs.update(frame, arg));
    }

    /* (non-Javadoc)
     * @see interfaces.IObservable#notifyObservers()
     */
    @Override
    public void notifyObservers() {
        this.notifyObservers(Optional.empty());
    }

}

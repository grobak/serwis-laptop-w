package listeners;

import java.util.Optional;

public interface Listener<T>
{
	void update(T listenable, Optional<Object> arg);
}

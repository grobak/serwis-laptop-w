package listeners;

import java.util.Optional;

public interface IObservable<T>
{

	void addObserver( Listener<T> obs );

	void removeObserver( Listener<T> obs );

	void notifyObservers( Optional<Object> arg );

	void notifyObservers();

}
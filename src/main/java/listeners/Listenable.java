package listeners;

public interface Listenable {
	void addListener( OwnListener listener );
	void removeListener( OwnListener listener );
	void notifyListeners();
}

package listeners;

import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.JPanel;

/**
 *
 * @author Grzesiek
 */
public interface PanelChooser {

    CopyOnWriteArrayList<PanelChooserListener> OBSERVERS = new CopyOnWriteArrayList<>();

    default void addObserver(PanelChooserListener obs) {
        OBSERVERS.clear();
        OBSERVERS.add(obs);
    }

    default void removeObserver(PanelChooserListener obs) {
        OBSERVERS.remove(obs);
    }

    default void setPanel(String title, JPanel panel) {
        OBSERVERS.forEach(e -> e.setPanel(title, panel));
    }
}

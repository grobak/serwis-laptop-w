package controllers;

import view.forms.PracownikForm;

public class PracownikFormValidator {
	
	private String errorMessage = "Nieznany błąd";
	
	public PracownikFormValidator(){
		
	}
	
	public boolean isFormValidUpdate(PracownikForm form){
		return isLoginValid(form.getLogin()) && isNameValid(form.getName()) &&
				doPasswordsMatch(form) && isTelefonValid(form.getPhone());
		
	}
	
	public boolean isFormValid(PracownikForm form){
		return isLoginValid(form.getLogin()) && isNameValid(form.getName()) &&
				isPasswordValid(form.getPassword()) && isPasswordConfirmValid(form.getPasswordConfirm()) &&
				doPasswordsMatch(form) && isTelefonValid(form.getPhone());
		
	}

	public boolean isLoginValid(String login) {
		if(login.length()==0){
			errorMessage = "Musisz wypełnić pole \"Nazwa użytkownika\"";
			return false;
		}		
		return true;
	}
	
	public boolean isNameValid(String name){
		if(name.length()==0){
			errorMessage = "Musisz wypełnić pole \"Imię i nazwisko\"";
			return false;
		}
		return true;
	}
	
	public boolean isPasswordValid(String password){
		if(password.length()==0){
			errorMessage = "Musisz wypełnić pole \"Hasło\"";
			return false;
		}
		return true;
	}
	
	public boolean isPasswordConfirmValid(String password){
		if(password.length()==0){
			errorMessage = "Musisz wypełnić pole \"Powtórz hasło\"";
			return false;
		}
		return true;
	}
	
	public boolean doPasswordsMatch(PracownikForm form){
		errorMessage = "Hasła nie pasują do siebie";
		return form.getPassword().equals(form.getPasswordConfirm());
	}
	
	public boolean isTelefonValid(String telefon){
		if(telefon.length()==0)
			return true;
		errorMessage = "Podany numer telefonu jest nieprawidłowy";
		return telefon.trim().length()>0 && isNumber(telefon);
	}
	
	public boolean isNumber(String text){
		text=text.replace("-", "");
		text=text.replace(" ", "");
		try{
			Integer.parseInt(text);
		}
		catch(NumberFormatException e){
			return false;
		}
		return true;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

}

package controllers;

import repositories.CustomerRepository;
import view.forms.KlientForm;

public class KlientFormValidator {

    private String errorMessage = "Nieznany błąd";
    private static final int NUMBER_OF_NIP_DIGITS = 10;
    private final KlientForm form;

    public KlientFormValidator(KlientForm form) {
        this.form = form;
    }

    public boolean formIsValid() {

        return requiredFieldsAreValid() && additionalFieldsAreValid();
    }

    public boolean requiredFieldsAreValid() {
        return isNazwaKlientaValid() && isTelefonValid();
    }

    public boolean additionalFieldsAreValid() {
        return isEmailValid() && isNIPValid();
    }

    public boolean nameIsAvailable() {
        errorMessage = "Podana nazwa klienta jest już zajęta.";
        return new CustomerRepository().nameIsAvailable(form.getNazwaKlienta());
    }

    public boolean isNazwaKlientaValid() {
        errorMessage = "Nazwa klienta ma nieodpowiednią ilość znaków. (min 3, max 100)";
        return form.getTextNazwaKlienta().getText().length() < 101 && form.getTextNazwaKlienta().getText().length() > 2;
    }

    public boolean isTelefonValid() {
        errorMessage = "Podany numer telefonu jest nieprawidłowy";
        return form.getTextTelefon().getText().trim().length() > 0 && isNumber(form.getTextTelefon().getText());
    }

    public boolean isEmailValid() {
        String email = form.getTextEmail().getText().trim();
        if (email.isEmpty()) {
            return true;
        }
        errorMessage = "Podany email jest nieprawidłowy";
        String emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(emailRegex);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches() && email.trim().length() < 50 && email.trim().length() > 5;
    }

    public boolean isNIPValid() {
        errorMessage = "Podany NIP jest nieprawidłowy";
        String nip = form.getTextNIP().getText().trim();
        if (nip.isEmpty()) {
            return true;
        }
        System.out.println(nip);

        long numberOfDigits = nip.chars()
                .filter(Character::isDigit)
                .count();
        return numberOfDigits == NUMBER_OF_NIP_DIGITS && nip.chars().noneMatch(Character::isAlphabetic);
    }

    public boolean isNumber(String text) {
        return text.chars()
                .filter(i -> Character.isDigit(i) || Character.isSpaceChar(i))
                .count() > 0;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}

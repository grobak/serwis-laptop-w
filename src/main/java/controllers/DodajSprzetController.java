package controllers;

import dialogs.DialogWyszukajKlienta;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import models.DodajSprzetModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pdf.PdfTemplateType;
import pdf.PlikPDF;
import repositories.ComputerRepository;
import view.PanelDodajSprzet;

public class DodajSprzetController {

    private final DodajSprzetModel model;
    private final PanelDodajSprzet view;

    private static final Logger LOG = LogManager.getLogger("src");

    public DodajSprzetController() {
        this.model = new DodajSprzetModel();
        this.view = new PanelDodajSprzet(this);
    }

    private void confirmPrint() {
        int numberOfCopiesToPrint = showConfirmPrintDialog();
        PlikPDF confirmationPdf = new PlikPDF(PdfTemplateType.PRZYJECIE, model.getSprzet(), model.getKlient());
        try {
            for (int i = 0; i < numberOfCopiesToPrint; i++) {
                confirmationPdf.print();
            }
        } catch (IOException | IllegalArgumentException ex) {
            LOG.error(ex, ex);
            JOptionPane.showMessageDialog(null, "Błąd wydruku: Drukowanie nie jest możliwe.",
                    "Błąd", JOptionPane.ERROR_MESSAGE);
        }
    }

    private int showConfirmPrintDialog() {
        Object options[] = {"1 raz", "2 razy", "Nie"};
        return JOptionPane.showOptionDialog(null, "Czy wydrukować potwierdzenie przyjęcia sprzętu?", "Potwierdzenie przyjęcia sprzętu", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
    }

    public void chooseClient() {
        DialogWyszukajKlienta dialog = new DialogWyszukajKlienta();
        dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dialog.setVisible(true);
        model.setKlient(dialog.closeDialog());
        if (model.getKlient() != null) {
            view.setNazwaKlienta(model.getKlient().getName());
        }
    }

    public void saveSprzet() {
        model.getSprzet().setIdLaptopa(new ComputerRepository().add(model.getSprzet()));
        view.clearForm();
        //confirmPrint(form.generateSprzet());
        confirmPrint();
    }
}

package controllers;

import components.TopSearchPanel;
import interfaces.BackgroundTask;
import listeners.PanelChooser;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import models.Customer;
import repositories.CustomerRepository;
import view.ContentPanel;
import view.PanelSzczegolyKlient;
import view.forms.SearchForm;

public class WyszukajKlient implements BackgroundTask, PanelChooser {

    private TopSearchPanel buttons = new TopSearchPanel();

    private JTable searchResultTable = new JTable();

    private final ContentPanel content;

    private int lastClickedRow;
    private final Object[] nazwyKolumn;

    private Customer klient;
    private List<Customer> klienci;

    public WyszukajKlient(ContentPanel content) {
        this.content = content;
        SearchForm form = (SearchForm) content.getContent();

        searchResultTable = form.getSearchResultTable();

        buttons.getTxtWyszukaj().requestFocusInWindow();

        buttons.getBtnWyszukaj().addActionListener(e -> {
            buttons.getBtnWyszukaj().setEnabled(false);
            setUpTable();
        });

        buttons.getTxtWyszukaj().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    buttons.getBtnWyszukaj().doClick();
                }
            }
        });

        searchResultTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                lastClickedRow = table.rowAtPoint(p);
                if (me.getClickCount() == 2) {
                    doubleClickAction();
                }
            }
        });

        content.setButtons(buttons);

        nazwyKolumn = new Object[]{"Dane klienta", "Telefon", "Miejscowość"};
        setUpTable();

    }

    public DefaultTableModel generateTableModel() {
        klienci = new CustomerRepository().findByNameOrCity(buttons.getSearchInput());

        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        model.setColumnIdentifiers(nazwyKolumn);

        for (Customer aKlienci : klienci) {
            model.insertRow(0, new Object[]{aKlienci.getName(), aKlienci.getPhone(), aKlienci.getCity()});
        }
        return model;
    }

    public void doubleClickAction() {
        klient = klienci.get(klienci.size() - lastClickedRow - 1);

        backgroundTask(() -> {
            klient = new CustomerRepository().getById(klient.getId());
            setPanel("Szczegóły klienta", new PanelSzczegolyKlient(klient, content));
        });

    }

    public void setUpTable() {
        backgroundTask(() -> searchResultTable.setModel(generateTableModel()),
                () -> buttons.getBtnWyszukaj().setEnabled(true));
    }

}

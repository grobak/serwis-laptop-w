package controllers;

import java.util.HashMap;
import java.util.Map;
import models.Computer;
import models.Customer;
import repositories.ComputerRepository;
import repositories.RepairRepository;

public class PlaceholderReplacer {
	
    private final Map<Placeholder, String> replacementValues = new HashMap<>();
    private final Map<Integer, String> stanTekst = new RepairRepository().getAllStatusNames();
    private final static String CHECKED_CHECKBOX = "checked=\"checked\"";

	public PlaceholderReplacer(Computer sprzet, Customer klient){
		fillValuesMap(sprzet, klient);
	}

	private void fillValuesMap(Computer sprzet, Customer klient) {
		replacementValues.put(Placeholder.RMA, sprzet.getRMA());
		replacementValues.put(Placeholder.KLIENT_NAME, klient.getName());
		replacementValues.put(Placeholder.TELEFON, klient.getAddress().getPhone()!=null ? klient.getAddress().getPhone() : "brak danych");
		replacementValues.put(Placeholder.MODEL, sprzet.getModel());
		replacementValues.put(Placeholder.DATE_ADDED, sprzet.getDataPrzyjecia());
		replacementValues.put(Placeholder.DATE_SENT, sprzet.getDataWysylki()!=null ? sprzet.getDataWysylki() : "---------------");

		replacementValues.put(Placeholder.HDD_CHK, sprzet.getEquipment().isHDD() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.CD_CHK, sprzet.getEquipment().isDVD() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.BATERIA_CHK, sprzet.getEquipment().isBateria() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.ZASILACZ_CHK, sprzet.getEquipment().isZasilacz() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.AC_CABLE_CHK, sprzet.getEquipment().isKabelAC() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.TORBA, sprzet.getEquipment().isTorba() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.KARTON, sprzet.getEquipment().isKarton() ? CHECKED_CHECKBOX : "");
		replacementValues.put(Placeholder.DODATKOWE_WYP, sprzet.getEquipment().getWypDodatkowe()!=null ? sprzet.getEquipment().getWypDodatkowe() : "");
		replacementValues.put(Placeholder.BRAK, sprzet.getEquipment().getBrak()!=null ? sprzet.getEquipment().getBrak() : "");
		replacementValues.put(Placeholder.UWAGI, sprzet.getUwagi()!=null ? sprzet.getUwagi() : "");
		replacementValues.put(Placeholder.HDD, stanTekst.get(sprzet.getStan().getHDD()));
		replacementValues.put(Placeholder.GLOSNIKI, stanTekst.get(sprzet.getStan().getGlosniki()));
		replacementValues.put(Placeholder.CD, stanTekst.get(sprzet.getStan().getDVD()));
		replacementValues.put(Placeholder.SLUCHAWKI, stanTekst.get(sprzet.getStan().getSluchawki()));
		replacementValues.put(Placeholder.MIC, stanTekst.get(sprzet.getStan().getMikrofon()));
		replacementValues.put(Placeholder.BATERIA, stanTekst.get(sprzet.getStan().getBateria()));
		replacementValues.put(Placeholder.ZASILACZ, stanTekst.get(sprzet.getStan().getZasilacz()));
		replacementValues.put(Placeholder.KLAWIATURA, stanTekst.get(sprzet.getStan().getKlawiatura()));
		replacementValues.put(Placeholder.LADOWANIE, stanTekst.get(sprzet.getStan().getLadowanie()));
		replacementValues.put(Placeholder.CAM, stanTekst.get(sprzet.getStan().getKamera()));
		replacementValues.put(Placeholder.BT, stanTekst.get(sprzet.getStan().getBluetooth()));
		replacementValues.put(Placeholder.TOUCHPAD, stanTekst.get(sprzet.getStan().getTouchpad()));
		replacementValues.put(Placeholder.LAN, stanTekst.get(sprzet.getStan().getLAN()));
		replacementValues.put(Placeholder.USB, stanTekst.get(sprzet.getStan().getUSB()));
		replacementValues.put(Placeholder.WIFI, stanTekst.get(sprzet.getStan().getWiFi()));
		replacementValues.put(Placeholder.MONITOR, stanTekst.get(sprzet.getStan().getMonitorZew()));
		replacementValues.put(Placeholder.OPIS_NAPRAWY, sprzet.getRodzajNaprawy());
		replacementValues.put(Placeholder.GWARANCJA, "Na naprawę nie udzielono gwarancji");
		replacementValues.put(Placeholder.OBJAWY, sprzet.getOpisUsterki());
		replacementValues.put(Placeholder.ADDED_BY, new ComputerRepository().addedBy(sprzet.getId()));
		replacementValues.put(Placeholder.RODZAJ_NAPRAWY, sprzet.getRodzajNaprawy());
		replacementValues.put(Placeholder.EKSPERTYZA, sprzet.getOpisNaprawy());
	}
	
	public String replace(String line){
		for (Placeholder placeholder : Placeholder.values()) {
			if(line.contains(placeholder.templateTextValue))
				line=line.replace(placeholder.templateTextValue, replacementValues.get(placeholder));
		}
		return line;
		
	}
	

	public void setWarranty(String warrantyText)
	{
		replacementValues.put(Placeholder.GWARANCJA, "Gwarancja na wykonaną naprawę wynosi " + warrantyText);
	}
	
	private enum Placeholder{
		RMA("xrmax"), 
		KLIENT_NAME("xklientx"),
		TELEFON("xTelefonx"),
		MODEL("xmodelx"),
		DATE_ADDED("xdataPrzyjeciax"),
		DATE_SENT("xdataWysylkix"),
		HDD_CHK("xhddCheckx"),
		CD_CHK("xCDCheckx"),
		BATERIA_CHK("xBatCheckx"),
		ZASILACZ_CHK("xZasCheckx"),
		AC_CABLE_CHK("xACCheckx"),
		TORBA("xTorbaCheckx"),
		KARTON("xKartonCheckx"),
		DODATKOWE_WYP("xDodatkoweWyposazeniex"),
		BRAK("xBrakx"),
		UWAGI("xUwagix"),
		HDD("xHDDx"),
		GLOSNIKI("xGlosnikix"),
		CD("xDVDx"),
		SLUCHAWKI("xSluchawkix"),
		MIC("xMikrofonx"),
		BATERIA("xBateriax"),
		ZASILACZ("xZasilaczx"),
		KLAWIATURA("xKlawiaturax"),
		LADOWANIE("xLadowaniex"),
		CAM("xKamerax"),
		BT("xBluetoothx"),
		TOUCHPAD("xTouchpadx"),
		LAN("xLANx"),
		USB("xUSBx"),
		WIFI("xWiFix"),
		MONITOR("xMonitorx"),
		OPIS_NAPRAWY("xOpisNaprawyx"),
		GWARANCJA("xGwarancjax"),
		OBJAWY("xObjawyx"),
		ADDED_BY("xPrzyjetyPrzezx"),
		RODZAJ_NAPRAWY("xRodzajNaprawyx"),
		EKSPERTYZA("xEkspertyzax");
		
		private String templateTextValue;
		
		Placeholder( String text ){
			this.templateTextValue=text;
		}
	}

}

package controllers;

import components.Buttons;
import interfaces.BackgroundTask;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import repositories.CustomerRepository;
import view.ContentPanel;
import view.forms.KlientForm;

public class DodajKlient implements BackgroundTask {

    private final KlientForm form;
    private final Buttons buttons = new Buttons();

    public DodajKlient(ContentPanel content) {
        this.form = (KlientForm) content.getContent();

        buttons.setButtonsAdd();

        buttons.getBtnCofnij().addActionListener(e -> form.clear());
        buttons.getBtnZapisz().setEnabled(false);
        KlientFormValidator validator = new KlientFormValidator(form);
        buttons.getBtnZapisz().addActionListener(e -> {
            if (validator.formIsValid() && validator.nameIsAvailable()) {

                backgroundTask(
                        () -> new CustomerRepository().add(form.generate()),
                        form::clear);
            } else {
                JOptionPane.showMessageDialog(null, validator.getErrorMessage(), "Błąd", JOptionPane.ERROR_MESSAGE);
            }
        });

        checkFieldsRequirements();
        form.clear();

        form.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER && buttons.getBtnZapisz().isEnabled()) {
                    buttons.getBtnZapisz().doClick();
                }
            }
        });
        content.setButtons(buttons);

    }

    private void checkFieldsRequirements() {
        DocumentListener pozwolenieNaZapis = new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                unlockAddButton();
            }

            public void removeUpdate(DocumentEvent e) {
                unlockAddButton();
            }

            public void insertUpdate(DocumentEvent e) {
                unlockAddButton();
            }
        };

        form.getTextNazwaKlienta().getDocument().addDocumentListener(pozwolenieNaZapis);
        form.getTextTelefon().getDocument().addDocumentListener(pozwolenieNaZapis);
    }

    private void unlockAddButton() {
        buttons.getBtnZapisz().setEnabled(new KlientFormValidator(form).requiredFieldsAreValid());
    }

}

package serwis.window;

import components.MenuTitle;
import components.TreeMenu;
import components.UserInfo;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

class View extends JFrame {

    private final JPanel panelContent;
    private final TreeMenu treeMenu;
    private final JProgressBar progressBar;

    private final UserInfo userInfo;
    private final MenuTitle menuTitle;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public View() {
        setTitle("Serwis " + SerwisMainWindow.VERSION);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        setMinimumSize(new Dimension(800, 600));
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{160, 0, 0};
        gbl_contentPane.rowHeights = new int[]{30, 60, 0, 0};
        gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        progressBar = new JProgressBar();
        GridBagConstraints gbc_progressBar = new GridBagConstraints();
        gbc_progressBar.fill = GridBagConstraints.HORIZONTAL;
        gbc_progressBar.insets = new Insets(0, 0, 5, 5);
        gbc_progressBar.gridx = 0;
        gbc_progressBar.gridy = 0;
        contentPane.add(progressBar, gbc_progressBar);

        userInfo = new UserInfo();
        GridBagConstraints gbc_userInfo_1 = new GridBagConstraints();
        gbc_userInfo_1.anchor = GridBagConstraints.EAST;
        gbc_userInfo_1.insets = new Insets(0, 0, 5, 0);
        gbc_userInfo_1.fill = GridBagConstraints.VERTICAL;
        gbc_userInfo_1.gridx = 1;
        gbc_userInfo_1.gridy = 0;
        contentPane.add(userInfo, gbc_userInfo_1);

        menuTitle = new MenuTitle();
        menuTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        GridBagConstraints gbc_panelMenuItem = new GridBagConstraints();
        gbc_panelMenuItem.gridwidth = 2;
        gbc_panelMenuItem.insets = new Insets(0, 0, 5, 0);
        gbc_panelMenuItem.fill = GridBagConstraints.BOTH;
        gbc_panelMenuItem.gridx = 0;
        gbc_panelMenuItem.gridy = 1;
        contentPane.add(menuTitle, gbc_panelMenuItem);
        GridBagLayout gbl_panelMenuItem = new GridBagLayout();
        gbl_panelMenuItem.columnWidths = new int[]{562, 0};
        gbl_panelMenuItem.rowHeights = new int[]{25, 0};
        gbl_panelMenuItem.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_panelMenuItem.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        menuTitle.setLayout(gbl_panelMenuItem);

        JPanel panelMenu = new JPanel();
        panelMenu.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        GridBagConstraints gbc_panelMenu = new GridBagConstraints();
        gbc_panelMenu.insets = new Insets(0, 0, 0, 5);
        gbc_panelMenu.fill = GridBagConstraints.BOTH;
        gbc_panelMenu.gridx = 0;
        gbc_panelMenu.gridy = 2;
        contentPane.add(panelMenu, gbc_panelMenu);
        panelMenu.setLayout(new GridLayout(1, 0, 0, 0));

        treeMenu = new TreeMenu();
        panelMenu.add(treeMenu);

        panelContent = new JPanel();
        panelContent.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

        GridBagLayout gbl_panelContent = new GridBagLayout();
        gbl_panelContent.columnWidths = new int[]{0};
        gbl_panelContent.rowHeights = new int[]{0};
        gbl_panelContent.columnWeights = new double[]{Double.MIN_VALUE};
        gbl_panelContent.rowWeights = new double[]{Double.MIN_VALUE};
        panelContent.setLayout(gbl_panelContent);

        GridBagConstraints gbc_panelContent = new GridBagConstraints();
        gbc_panelContent.fill = GridBagConstraints.BOTH;
        gbc_panelContent.gridx = 1;
        gbc_panelContent.gridy = 2;
        contentPane.add(panelContent, gbc_panelContent);
    }

    public JPanel getPanelContent() {
        return panelContent;
    }

    public TreeMenu getTreeMenu() {
        return treeMenu;
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public MenuTitle getMenuTitle() {
        return menuTitle;
    }

}

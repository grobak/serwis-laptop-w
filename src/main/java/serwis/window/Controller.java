package serwis.window;

import dataproviders.Database;
import dialogs.login.LoginDialog;
import interfaces.BackgroundTask;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import listeners.PanelChooser;
import listeners.PanelChooserListener;
import progressbar.ProgressController;

/**
 *
 * @author Grzesiek
 */
class Controller implements PanelChooserListener {

    private final View view;
    private final ProgressBarControl progressBarControl;

    public Controller(View view) {
        this.view = view;
        this.progressBarControl = new ProgressBarControl(view.getProgressBar());
    }

    public void init() {
        view.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        view.setLocationRelativeTo(null);
        view.setVisible(true);
        
        view.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Database.INSTANCE.close();
                e.getWindow().setVisible(false);
                e.getWindow().dispose();
            }
        });

        view.getUserInfo().addLogoutListener(e -> {
            Database.INSTANCE.close();
            new LoginDialog();
            view.setVisible(false);
            view.dispose();
        });

        view.getTreeMenu().addTreeSelectionListener(
                e -> setPanel(view.getTreeMenu().getSelectedMenuName(),
                        view.getTreeMenu().getSelectedPanel()));
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    @Override
    public void setPanel(String title, JPanel panel) {
        if (panel instanceof BackgroundTask) {
            ((ProgressController) panel).addObserver(progressBarControl);
        }
        if (panel instanceof PanelChooser) {
            ((PanelChooser) panel).addObserver(this);
        }
        EventQueue.invokeLater(() -> {
            view.getPanelContent().removeAll();
            view.getMenuTitle().setTitle(title);
            GridBagConstraints gbc_panel = new GridBagConstraints();
            gbc_panel.insets = new Insets(0, 0, 0, 5);
            gbc_panel.fill = GridBagConstraints.BOTH;
            gbc_panel.gridx = 0;
            gbc_panel.gridy = 0;
            view.getPanelContent().add(panel, gbc_panel);
            panel.setVisible(true);
            view.getPanelContent().repaint();
            view.getTreeMenu().transferFocus();
        });
    }


}

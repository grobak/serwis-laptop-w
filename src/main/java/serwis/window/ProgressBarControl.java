package serwis.window;

import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import progressbar.ProgressBarListener;

class ProgressBarControl implements ProgressBarListener {

    private final JProgressBar progressBar;

    public ProgressBarControl(JProgressBar progressBar) {
        super();
        this.progressBar = progressBar;
    }

    public void start() {
        getWorker(true).execute();
    }

    public void stop() {
        getWorker(false).execute();
    }

    private SwingWorker getWorker(boolean startStopFlag) {
        return new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                progressBar.setIndeterminate(startStopFlag);
                return null;
            }
        };
    }

    @Override
    public void start(boolean flag) {
        stop();
        if (flag) {
            start();
        };
    }

    public enum ProgressBar {

        START, STOP;
    }

}

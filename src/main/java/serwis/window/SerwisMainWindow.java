package serwis.window;

public class SerwisMainWindow {

    public final static String VERSION = "1.14";

    public SerwisMainWindow() {
        Controller controller = new Controller(new View());
        controller.init();
    }
}

package serwis;

import dialogs.login.LoginDialog;
import java.awt.EventQueue;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Serwis {
    
    private static final Logger LOG = LogManager.getLogger("src");

    private Serwis() {
    };
    
    public static void main(String... args) {
        setLookAndFeel();
        EventQueue.invokeLater(() -> new LoginDialog());
    }

    private static void setLookAndFeel() {
        try {
            if (System.getProperty("os.name").equals("Linux")) {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            } else {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
        } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOG.error(e, e);
        }
    }

}

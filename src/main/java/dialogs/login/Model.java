package dialogs.login;

import java.util.Observable;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dataproviders.ConnectionValidator;
import dataproviders.DatabaseURI;
import dataproviders.Database;

class Model extends Observable{
		
	private static final Logger LOG = LogManager.getLogger("src");
	
	private final Database DB;
	private final DatabaseURI settings;
	
	public Model() {
		this.DB = Database.INSTANCE;
		settings = DB.getDatabaseInfo();
	}
	
	public boolean login(String username, char... password){
		DB.saveLastLoggedUser(username);
		notifyObservers( Events.SEARCH );
		try {
			if(new ConnectionValidator().isServerUp(settings.getAdres(), Integer.valueOf( settings.getPort() )))
			{
				return isUserAuthenticated(username, password);
			}
		}
		catch (Exception e1) {
			LOG.debug(e1,e1);
			JOptionPane.showMessageDialog(null, "Nie można ustanowić połączenia z serwerem bazy danych. \nSprawdź ustawienia połączenia.", "Błąd", JOptionPane.ERROR_MESSAGE);
		}   
		notifyObservers( Events.NONE );
		return false;
	}
	
	public boolean isUserAuthenticated(String username, char... password){
		return DB.login(username, String.valueOf(password));
	}
	
	public String getLastLoggedUserName() {
		return DB.readLastUser();
	}
	
	@Override
	public void notifyObservers() {
		notifyObservers( null );
	}
	
	@Override
	public void notifyObservers(Object arg) {
		setChanged();
		super.notifyObservers( arg );
	}

}

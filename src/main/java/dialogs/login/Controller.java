package dialogs.login;

import dialogs.options.OptionsDialog;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import serwis.window.SerwisMainWindow;

class Controller implements Observer {

    private final View view;
    private static final Logger LOG = LogManager.getLogger("src");

    private final Model model;

    public Controller(View view) {
        this.view = view;
        this.model = new Model();
    }

    public void init() {
        model.addObserver(this);

        view.addLoginListener(e -> authorize());
        view.addOptionsListener(e -> openOptionsDialog());
        fillLastLoggedUserName();

        view.addCancelListener(e -> {
            view.setVisible(false);
            view.dispose();
        });

        view.setLocationRelativeTo(null);
        view.setVisible(true);
    }

    private void openOptionsDialog() {
        OptionsDialog dialogOpcje = new OptionsDialog();
        dialogOpcje.setVisible(true);
    }

    private void fillLastLoggedUserName() {
        view.setLogin(model.getLastLoggedUserName());
        moveFocusToPassword();
    }

    private void moveFocusToPassword() {
        if (!view.getLogin().isEmpty()) {
            EventQueue.invokeLater(() -> view.passwordRequestFocus());
        }
    }

    private void authorize() {
        if (model.login(view.getLogin(), view.getPassword())) {
            LOG.debug("Login attempt - success");
            openMainWindow();
        } else {
            loginFailed();
        }

    }

    private void openMainWindow() {
        view.hideAndStopProgressBar();
        view.close();
        new SerwisMainWindow();    
    }

    private void loginFailed() {
        Toolkit.getDefaultToolkit().beep();
        JOptionPane.showMessageDialog(null,
                "Nieprawidłowy login lub hasło",
                "Błąd",
                JOptionPane.ERROR_MESSAGE);
        view.hideAndStopProgressBar();
        view.passwordRequestFocus();
        LOG.debug("Login attempt - failed");
    }

    @Override
    public void update(Observable observable, Object event) {
        view.hideAndStopProgressBar();
        if (event == Events.SEARCH) {
            view.showAndStartProgressBar();
        }

    }

}

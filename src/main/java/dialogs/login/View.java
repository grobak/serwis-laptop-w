package dialogs.login;

import components.JTextFieldWthPopup;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

class View extends JFrame {

    private final JPanel contentPanel = new JPanel();
    private final JTextFieldWthPopup poleLogin;
    private final JPasswordField poleHaslo;
    private final JProgressBar progressBar;
    private final JButton btnOptions;
    private final JButton btnLogin;
    private final JButton btnCancel;
    private final JLabel lblPrzesyanieDanych;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public View() {
        setTitle("Serwis - logowanie");
        setBounds(100, 100, 450, 300);
        setResizable(false);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{112, 113, 113, 112, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 14, 42, 20, 0, 0, 20, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0,
            Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);

        Component verticalStrut = Box.createVerticalStrut(20);
        GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
        gbc_verticalStrut.insets = new Insets(0, 0, 5, 0);
        gbc_verticalStrut.gridx = 3;
        gbc_verticalStrut.gridy = 0;
        contentPanel.add(verticalStrut, gbc_verticalStrut);

        JLabel lblInfo = new JLabel("Proszę podać nazwę użytkownika i hasło:");
        GridBagConstraints gbc_lblInfo = new GridBagConstraints();
        gbc_lblInfo.gridwidth = 4;
        gbc_lblInfo.anchor = GridBagConstraints.NORTH;
        gbc_lblInfo.insets = new Insets(0, 0, 5, 0);
        gbc_lblInfo.gridx = 0;
        gbc_lblInfo.gridy = 1;
        contentPanel.add(lblInfo, gbc_lblInfo);

        JLabel lblLogin = new JLabel("Nazwa użytkownika: ");
        GridBagConstraints gbc_lblLogin = new GridBagConstraints();
        gbc_lblLogin.insets = new Insets(0, 0, 5, 5);
        gbc_lblLogin.gridx = 1;
        gbc_lblLogin.gridy = 3;
        contentPanel.add(lblLogin, gbc_lblLogin);

        poleLogin = new JTextFieldWthPopup();
        GridBagConstraints gbc_poleLogin = new GridBagConstraints();
        gbc_poleLogin.fill = GridBagConstraints.HORIZONTAL;
        gbc_poleLogin.anchor = GridBagConstraints.NORTH;
        gbc_poleLogin.insets = new Insets(0, 0, 5, 5);
        gbc_poleLogin.gridx = 2;
        gbc_poleLogin.gridy = 3;
        contentPanel.add(poleLogin, gbc_poleLogin);
        poleLogin.setColumns(10);

        JLabel lblHaso = new JLabel("Hasło: ");
        GridBagConstraints gbc_lblHaso = new GridBagConstraints();
        gbc_lblHaso.insets = new Insets(0, 0, 5, 5);
        gbc_lblHaso.gridx = 1;
        gbc_lblHaso.gridy = 4;
        contentPanel.add(lblHaso, gbc_lblHaso);

        poleHaslo = new JPasswordField();
        GridBagConstraints gbc_poleHaslo = new GridBagConstraints();
        gbc_poleHaslo.fill = GridBagConstraints.HORIZONTAL;
        gbc_poleHaslo.insets = new Insets(0, 0, 5, 5);
        gbc_poleHaslo.anchor = GridBagConstraints.NORTH;
        gbc_poleHaslo.gridx = 2;
        gbc_poleHaslo.gridy = 4;
        contentPanel.add(poleHaslo, gbc_poleHaslo);
        poleHaslo.setColumns(10);

        Component verticalStrut_1 = Box.createVerticalStrut(20);
        GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
        gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
        gbc_verticalStrut_1.gridx = 1;
        gbc_verticalStrut_1.gridy = 5;
        contentPanel.add(verticalStrut_1, gbc_verticalStrut_1);

        progressBar = new JProgressBar();
        GridBagConstraints gbc_progressBar = new GridBagConstraints();
        gbc_progressBar.fill = GridBagConstraints.HORIZONTAL;
        gbc_progressBar.insets = new Insets(0, 0, 5, 5);
        gbc_progressBar.gridwidth = 2;
        gbc_progressBar.gridx = 1;
        gbc_progressBar.gridy = 6;
        contentPanel.add(progressBar, gbc_progressBar);

        lblPrzesyanieDanych = new JLabel("Przesyłanie danych...");
        lblPrzesyanieDanych.setVisible(false);
        GridBagConstraints gbc_lblPrzesyanieDanych = new GridBagConstraints();
        gbc_lblPrzesyanieDanych.gridwidth = 4;
        gbc_lblPrzesyanieDanych.gridx = 0;
        gbc_lblPrzesyanieDanych.gridy = 7;
        contentPanel.add(lblPrzesyanieDanych, gbc_lblPrzesyanieDanych);
        progressBar.setVisible(false);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        btnOptions = new JButton("Ustawienia połączenia");
        buttonPane.add(btnOptions);

        btnLogin = new JButton("Zaloguj");
        btnLogin.setDefaultCapable(true);
        buttonPane.add(btnLogin);
        getRootPane().setDefaultButton(btnLogin);

        btnCancel = new JButton("Zamknij");
        btnCancel.setActionCommand("Cancel");
        buttonPane.add(btnCancel);
    }

    public void passwordRequestFocus() {
        poleHaslo.requestFocusInWindow();
        poleHaslo.selectAll();
    }

    public void showAndStartProgressBar() {
        EventQueue.invokeLater(() -> {
            disableInputs();
            progressBar.setVisible(true);
            lblPrzesyanieDanych.setVisible(true);
            progressBar.setIndeterminate(true);
        });
    }

    public void hideAndStopProgressBar() {
        EventQueue.invokeLater(() -> {
            enableInputs();
            progressBar.setVisible(false);
            lblPrzesyanieDanych.setVisible(false);
            progressBar.setIndeterminate(false);
        });
    }

    public void disableInputs() {
        poleLogin.setEnabled(false);
        poleHaslo.setEnabled(false);
        btnLogin.setEnabled(false);
        btnOptions.setEnabled(false);
        btnCancel.setEnabled(false);
    }

    public void enableInputs() {
        poleLogin.setEnabled(true);
        poleHaslo.setEnabled(true);
        btnLogin.setEnabled(true);
        btnOptions.setEnabled(true);
        btnCancel.setEnabled(true);
    }

    public void close() {
        this.setVisible(false);
        this.dispose();
    }

    public String getLogin() {
        return poleLogin.getText();
    }

    public void setLogin(final String login) {
        poleLogin.setText(login);
    }

    public char[] getPassword() {
        return poleHaslo.getPassword();
    }

    public void resetPassword() {
        poleHaslo.setText("");
    }

    public void addLoginListener(final ActionListener listener) {
        btnLogin.addActionListener(listener);
    }

    public void addOptionsListener(final ActionListener listener) {
        btnOptions.addActionListener(listener);
    }

    public void addCancelListener(final ActionListener listener) {
        btnCancel.addActionListener(listener);
    }

}

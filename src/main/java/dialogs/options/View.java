package dialogs.options;

import components.JTextFieldWthPopup;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

class View extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private final JTextFieldWthPopup JTextDbHostname;
    private final JTextFieldWthPopup JTextDbPort;
    private final JTextFieldWthPopup JTextDbName;
    private final JTextFieldWthPopup jTextEmail;
    private final JPasswordField JTextEmailPassword;
    private final JButton okButton;
    private final JButton cancelButton;
    private final JTextFieldWthPopup jTextSmtpHostname;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public View() {
        this.setModalityType(ModalityType.APPLICATION_MODAL);
        setTitle("Opcje");
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{92, 0};
        gbl_contentPanel.rowHeights = new int[]{14, 0, 14, 14, 0, 0, 0, 14, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);

        JPanel dbConfigurationPanel = new JPanel();
        dbConfigurationPanel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Ustawienia bazy danych", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        GridBagConstraints gbc_DbConfigurationPanel = new GridBagConstraints();
        gbc_DbConfigurationPanel.gridheight = 4;
        gbc_DbConfigurationPanel.insets = new Insets(0, 0, 5, 0);
        gbc_DbConfigurationPanel.fill = GridBagConstraints.BOTH;
        gbc_DbConfigurationPanel.gridx = 0;
        gbc_DbConfigurationPanel.gridy = 0;
        contentPanel.add(dbConfigurationPanel, gbc_DbConfigurationPanel);
        GridBagLayout gbl_DbConfigurationPanel = new GridBagLayout();
        gbl_DbConfigurationPanel.columnWidths = new int[]{0, 0, 0};
        gbl_DbConfigurationPanel.rowHeights = new int[]{0, 0, 0, 0};
        gbl_DbConfigurationPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        gbl_DbConfigurationPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        dbConfigurationPanel.setLayout(gbl_DbConfigurationPanel);

        JLabel lblDbHostname = new JLabel("Adres bazy");
        GridBagConstraints gbc_lblDbHostname = new GridBagConstraints();
        gbc_lblDbHostname.insets = new Insets(0, 0, 5, 5);
        gbc_lblDbHostname.gridx = 0;
        gbc_lblDbHostname.gridy = 0;
        dbConfigurationPanel.add(lblDbHostname, gbc_lblDbHostname);

        JTextDbHostname = new JTextFieldWthPopup();
        GridBagConstraints gbc_JTextDbHostname = new GridBagConstraints();
        gbc_JTextDbHostname.insets = new Insets(0, 0, 5, 0);
        gbc_JTextDbHostname.fill = GridBagConstraints.HORIZONTAL;
        gbc_JTextDbHostname.gridx = 1;
        gbc_JTextDbHostname.gridy = 0;
        dbConfigurationPanel.add(JTextDbHostname, gbc_JTextDbHostname);

        JLabel lblDbPort = new JLabel("Port bazy");
        GridBagConstraints gbc_lblDbPort = new GridBagConstraints();
        gbc_lblDbPort.insets = new Insets(0, 0, 5, 5);
        gbc_lblDbPort.gridx = 0;
        gbc_lblDbPort.gridy = 1;
        dbConfigurationPanel.add(lblDbPort, gbc_lblDbPort);

        JTextDbPort = new JTextFieldWthPopup();
        GridBagConstraints gbc_JTextDbPort = new GridBagConstraints();
        gbc_JTextDbPort.fill = GridBagConstraints.HORIZONTAL;
        gbc_JTextDbPort.insets = new Insets(0, 0, 5, 0);
        gbc_JTextDbPort.gridx = 1;
        gbc_JTextDbPort.gridy = 1;
        dbConfigurationPanel.add(JTextDbPort, gbc_JTextDbPort);

        JLabel lblDbName = new JLabel("Nazwa bazy");
        GridBagConstraints gbc_lblDbName = new GridBagConstraints();
        gbc_lblDbName.insets = new Insets(0, 0, 0, 5);
        gbc_lblDbName.gridx = 0;
        gbc_lblDbName.gridy = 2;
        dbConfigurationPanel.add(lblDbName, gbc_lblDbName);

        JTextDbName = new JTextFieldWthPopup();
        GridBagConstraints gbc_JTextDbName = new GridBagConstraints();
        gbc_JTextDbName.fill = GridBagConstraints.HORIZONTAL;
        gbc_JTextDbName.gridx = 1;
        gbc_JTextDbName.gridy = 2;
        dbConfigurationPanel.add(JTextDbName, gbc_JTextDbName);

        JPanel smtpConfigurationPanel = new JPanel();
        smtpConfigurationPanel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Ustawienia email", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        GridBagConstraints gbc_SmtpConfigurationPanel = new GridBagConstraints();
        gbc_SmtpConfigurationPanel.gridheight = 4;
        gbc_SmtpConfigurationPanel.fill = GridBagConstraints.BOTH;
        gbc_SmtpConfigurationPanel.gridx = 0;
        gbc_SmtpConfigurationPanel.gridy = 4;
        contentPanel.add(smtpConfigurationPanel, gbc_SmtpConfigurationPanel);
        GridBagLayout gbl_SmtpConfigurationPanel = new GridBagLayout();
        gbl_SmtpConfigurationPanel.columnWidths = new int[]{214, 210, 0};
        gbl_SmtpConfigurationPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_SmtpConfigurationPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        gbl_SmtpConfigurationPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        smtpConfigurationPanel.setLayout(gbl_SmtpConfigurationPanel);

        JLabel lblAdresSerweraSmtp = new JLabel("Adres serwera SMTP");
        GridBagConstraints gbc_lblAdresSerweraSmtp = new GridBagConstraints();
        gbc_lblAdresSerweraSmtp.insets = new Insets(0, 0, 5, 5);
        gbc_lblAdresSerweraSmtp.gridx = 0;
        gbc_lblAdresSerweraSmtp.gridy = 0;
        smtpConfigurationPanel.add(lblAdresSerweraSmtp, gbc_lblAdresSerweraSmtp);

        jTextSmtpHostname = new JTextFieldWthPopup();
        GridBagConstraints gbc_jTextSmtpHostname = new GridBagConstraints();
        gbc_jTextSmtpHostname.insets = new Insets(0, 0, 5, 0);
        gbc_jTextSmtpHostname.fill = GridBagConstraints.HORIZONTAL;
        gbc_jTextSmtpHostname.gridx = 1;
        gbc_jTextSmtpHostname.gridy = 0;
        smtpConfigurationPanel.add(jTextSmtpHostname, gbc_jTextSmtpHostname);
        jTextSmtpHostname.setColumns(10);

        JLabel lblNewLabel_2 = new JLabel("Adres email");
        GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
        gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel_2.gridx = 0;
        gbc_lblNewLabel_2.gridy = 1;
        smtpConfigurationPanel.add(lblNewLabel_2, gbc_lblNewLabel_2);

        jTextEmail = new JTextFieldWthPopup();
        GridBagConstraints gbc_JTextLogin = new GridBagConstraints();
        gbc_JTextLogin.insets = new Insets(0, 0, 5, 0);
        gbc_JTextLogin.fill = GridBagConstraints.HORIZONTAL;
        gbc_JTextLogin.gridx = 1;
        gbc_JTextLogin.gridy = 1;
        smtpConfigurationPanel.add(jTextEmail, gbc_JTextLogin);

        JLabel lblEmailPassword = new JLabel("Hasło");
        GridBagConstraints gbc_lblEmailPassword = new GridBagConstraints();
        gbc_lblEmailPassword.insets = new Insets(0, 0, 5, 5);
        gbc_lblEmailPassword.gridx = 0;
        gbc_lblEmailPassword.gridy = 2;
        smtpConfigurationPanel.add(lblEmailPassword, gbc_lblEmailPassword);

        JTextEmailPassword = new JPasswordField();
        GridBagConstraints gbc_JTextEmailPassword = new GridBagConstraints();
        gbc_JTextEmailPassword.insets = new Insets(0, 0, 5, 0);
        gbc_JTextEmailPassword.fill = GridBagConstraints.HORIZONTAL;
        gbc_JTextEmailPassword.gridx = 1;
        gbc_JTextEmailPassword.gridy = 2;
        smtpConfigurationPanel.add(JTextEmailPassword, gbc_JTextEmailPassword);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        okButton = new JButton("Zapisz");
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        cancelButton = new JButton("Anuluj");
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
    }

    JTextFieldWthPopup getDbHostname() {
        return JTextDbHostname;
    }

    JTextFieldWthPopup getDbPort() {
        return JTextDbPort;
    }

    JTextFieldWthPopup getDbName() {
        return JTextDbName;
    }

    JTextFieldWthPopup getEmail() {
        return jTextEmail;
    }

    JPasswordField getEmailPassword() {
        return JTextEmailPassword;
    }

    JTextFieldWthPopup getSmtpHostname() {
        return jTextSmtpHostname;
    }

    JButton getOkButton() {
        return okButton;
    }

    JButton getCancelButton() {
        return cancelButton;
    }

}

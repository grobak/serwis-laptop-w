package dialogs.options;

import javax.swing.JDialog;
import models.DatabaseConfiguration;

class Controller {

    private final View view;
    private final Model model;

    public Controller(View view) {
        this.view = view;
        this.model = new Model();
    }

    public static Controller getDefaultInstance(View view) {
        Controller ctrl = new Controller(view);
        ctrl.init();
        return ctrl;
    }

    public void init() {
        view.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        view.getDbHostname().setText(model.getDbHostname());
        view.getDbPort().setText(model.getDbPort());
        view.getDbName().setText(model.getDbName());
        view.getSmtpHostname().setText(model.getSmtpHostname());
        view.getEmail().setText(model.getEmail());
        view.getEmailPassword().setText(model.getEmailPassword());

        view.getOkButton().addActionListener(e -> {
            DatabaseConfiguration dbConfig = new DatabaseConfiguration();
            dbConfig.setDbHostname(view.getDbHostname().getText());
            dbConfig.setPort(Integer.parseInt(view.getDbPort().getText()));
            dbConfig.setDbName(view.getDbName().getText());
            dbConfig.setSmtpHostname(view.getSmtpHostname().getText());
            dbConfig.setEmail(view.getEmail().getText());
            dbConfig.setEmailPassword(view.getEmailPassword().getPassword());
            dbConfig.setLastLoggedUsername("");
            model.updateSettings(dbConfig);
            view.setVisible(false);
            view.dispose();
        });

        view.getCancelButton().addActionListener(e -> {
            view.setVisible(false);
            view.dispose();
        });

    }

}

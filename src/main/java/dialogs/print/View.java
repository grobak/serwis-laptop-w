package dialogs.print;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

class View extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private final JComboBox<String> comboGwarancja;
    private final JToggleButton tglbtnPrzyjcieSprztu;
    private final JToggleButton tglbtnKartaTowaru;
    private final JToggleButton tglbtnEkspertyza;
    private final ButtonGroup grupa = new ButtonGroup();
    private final JLabel lblGwarancja;
    private final JButton cancelButton;
    private final JButton okButton;
    private final JButton btnPodgld;

    @SuppressWarnings("PMD.VariableNamingConventions")
    public View() {
        this.setModalityType(ModalityType.APPLICATION_MODAL);
        setTitle("Drukuj...");
        setBounds(100, 100, 385, 213);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);

        tglbtnPrzyjcieSprztu = new JToggleButton("Przyjęcie sprzętu");
        GridBagConstraints gbc_tglbtnPrzyjcieSprztu = new GridBagConstraints();
        gbc_tglbtnPrzyjcieSprztu.gridwidth = 2;
        gbc_tglbtnPrzyjcieSprztu.insets = new Insets(0, 0, 5, 5);
        gbc_tglbtnPrzyjcieSprztu.gridx = 0;
        gbc_tglbtnPrzyjcieSprztu.gridy = 0;
        contentPanel.add(tglbtnPrzyjcieSprztu, gbc_tglbtnPrzyjcieSprztu);

        tglbtnKartaTowaru = new JToggleButton("Karta Towaru");
        GridBagConstraints gbc_tglbtnKartaTowaru = new GridBagConstraints();
        gbc_tglbtnKartaTowaru.insets = new Insets(0, 0, 5, 5);
        gbc_tglbtnKartaTowaru.gridx = 2;
        gbc_tglbtnKartaTowaru.gridy = 0;
        contentPanel.add(tglbtnKartaTowaru, gbc_tglbtnKartaTowaru);

        tglbtnEkspertyza = new JToggleButton("Ekspertyza");
        GridBagConstraints gbc_tglbtnEkspertyza = new GridBagConstraints();
        gbc_tglbtnEkspertyza.insets = new Insets(0, 0, 5, 0);
        gbc_tglbtnEkspertyza.gridx = 3;
        gbc_tglbtnEkspertyza.gridy = 0;
        contentPanel.add(tglbtnEkspertyza, gbc_tglbtnEkspertyza);

        grupa.add(tglbtnEkspertyza);
        grupa.add(tglbtnKartaTowaru);
        grupa.add(tglbtnPrzyjcieSprztu);
        grupa.setSelected(tglbtnKartaTowaru.getModel(), true);

        lblGwarancja = new JLabel("Gwarancja:");
        GridBagConstraints gbc_lblGwarancja = new GridBagConstraints();
        gbc_lblGwarancja.anchor = GridBagConstraints.EAST;
        gbc_lblGwarancja.insets = new Insets(0, 0, 5, 5);
        gbc_lblGwarancja.gridx = 0;
        gbc_lblGwarancja.gridy = 2;
        contentPanel.add(lblGwarancja, gbc_lblGwarancja);

        comboGwarancja = new JComboBox<>();
        comboGwarancja.setModel(new DefaultComboBoxModel<>());
        comboGwarancja.setEditable(true);
        GridBagConstraints gbc_comboBox1 = new GridBagConstraints();
        gbc_comboBox1.gridwidth = 3;
        gbc_comboBox1.insets = new Insets(0, 0, 5, 0);
        gbc_comboBox1.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox1.gridx = 1;
        gbc_comboBox1.gridy = 2;
        contentPanel.add(comboGwarancja, gbc_comboBox1);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        btnPodgld = new JButton("Podgląd");
        buttonPane.add(btnPodgld);

        okButton = new JButton("Drukuj");
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        cancelButton = new JButton("Anuluj");
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
    }

    public JComboBox<String> getComboGwarancja() {
        return comboGwarancja;
    }

    public JToggleButton getTglbtnPrzyjcieSprztu() {
        return tglbtnPrzyjcieSprztu;
    }

    public JToggleButton getTglbtnKartaTowaru() {
        return tglbtnKartaTowaru;
    }

    public JToggleButton getTglbtnEkspertyza() {
        return tglbtnEkspertyza;
    }

    public ButtonGroup getGrupa() {
        return grupa;
    }

    public JLabel getLblGwarancja() {
        return lblGwarancja;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getBtnPodgld() {
        return btnPodgld;
    }

}

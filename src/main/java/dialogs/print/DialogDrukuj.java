package dialogs.print;


import models.Computer;
import models.Customer;

public class DialogDrukuj extends View {

    public DialogDrukuj(Computer computer, Customer customer) {
        super();
        new Controller(this, computer, customer).init();
    }
}

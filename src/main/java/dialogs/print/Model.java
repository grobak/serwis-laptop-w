package dialogs.print;

import java.io.IOException;
import java.util.Arrays;
import java.util.Observable;
import models.Computer;
import models.Customer;
import pdf.PdfTemplateType;
import pdf.PlikPDF;
import repositories.ComputerRepository;

/**
 *
 * @author Grzesiek
 */
class Model extends Observable{
    
    private final String[] warrantyValues;
    private String warranty;
    private PdfTemplateType typWydruku;
    
    private final Customer customer;
    private final Computer computer;

    public Model(Computer computer, Customer customer) {
        this.customer = customer;
        this.computer = computer;
        this.warrantyValues = new String[] {"12 miesięcy", "6 miesięcy", "3 miesiące", "1 miesiąc", "7 dni", "Brak"};
        this.typWydruku = PdfTemplateType.KARTA;
    }

    public String[] getWarrantyValues() {
        return Arrays.copyOf(warrantyValues, warrantyValues.length);
    }

    @Override
    public void notifyObservers(Object arg) {
        setChanged();
        super.notifyObservers(arg);  
    }

    @Override
    public void notifyObservers() {
        notifyObservers( null );
    }

    boolean isWarrantyVisible() {
        return typWydruku == PdfTemplateType.KARTA || typWydruku == PdfTemplateType.PRZYJECIE;
    }
    
    public void setPrintType(PdfTemplateType type)
    {
        this.typWydruku = type;
        notifyObservers();
    }    

    void setWarranty(String itemAt) {
        this.warranty = itemAt;
        notifyObservers();
    }

    public void printPDF() throws IOException {
        switch (typWydruku) {
            case KARTA:
                new PlikPDF(typWydruku, computer, customer, warranty).print();
                break;
            case EKSPERTYZA:
                new PlikPDF(typWydruku, computer, customer, "brak").print();
                break;
            case PRZYJECIE:
                new PlikPDF(typWydruku, computer, customer).print();
                break;
        }
    }

    public void openPDF() throws IOException {
        switch (typWydruku) {
            case KARTA:
                new PlikPDF(typWydruku, computer, customer, warranty).open();
                break;
            case EKSPERTYZA:
                new PlikPDF(typWydruku, computer, customer, "brak").open();
                break;
            case PRZYJECIE:
                new PlikPDF(typWydruku, computer, customer).open();
                break;
        }
    }

    void setComputerShipmentDate() {
        computer.setDataWysylki(new ComputerRepository().getShipmentDate(computer.getId()));
    }

}

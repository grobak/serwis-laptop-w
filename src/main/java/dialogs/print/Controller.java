package dialogs.print;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import models.Computer;
import models.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pdf.PdfTemplateType;

class Controller implements Observer {

    private static final Logger LOG = LogManager.getLogger("src");

    private final View view;
    private final Model model;

    private boolean wcisnietoOk;

    public Controller(View view, Computer computer, Customer customer) {
        this.view = view;
        this.model = new Model(computer, customer);
    }

    public void init() {
        model.addObserver(this);
        model.notifyObservers();

        view.getTglbtnEkspertyza().addActionListener(e -> model.setPrintType(PdfTemplateType.EKSPERTYZA));
        view.getTglbtnKartaTowaru().addActionListener(e -> model.setPrintType(PdfTemplateType.KARTA));
        view.getTglbtnPrzyjcieSprztu().addActionListener(e -> model.setPrintType(PdfTemplateType.PRZYJECIE));
        view.getComboGwarancja().addActionListener(e -> model.setWarranty(getSelectedWarranty()));

        view.getOkButton().addActionListener(e -> {
            wcisnietoOk = true;
            model.setComputerShipmentDate();
            printPDF();

            zamknijDialog();
        });

        view.getBtnPodgld().addActionListener(e -> openPDF());

        view.getCancelButton().addActionListener(e -> zamknijDialog());
    }

    private String getSelectedWarranty() {
        return view.getComboGwarancja().getItemAt(view.getComboGwarancja().getSelectedIndex());
    }

    private void openPDF() {
        try {
            model.openPDF();
        } catch (Exception e) {
            LOG.error(e, e);
            JOptionPane.showMessageDialog(null, "Błąd: Nie można otworzyć pliku.",
                    "Błąd", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void printPDF() {     
        try {
            model.printPDF();
        } catch (IOException ex) {
            LOG.error(ex, ex);
            JOptionPane.showMessageDialog(null, "Błąd wydruku: Drukowanie nie jest możliwe.",
                    "Błąd", JOptionPane.ERROR_MESSAGE);
        }
    }

    private Result zamknijDialog() {
        view.setVisible(false);
        view.dispose();
        if (wcisnietoOk) {           
            return Result.OK;
        } else {
            return Result.CANCEL;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Model) {
            Model model = (Model) o;
            view.getComboGwarancja().setModel(new DefaultComboBoxModel<>(model.getWarrantyValues()));
            view.getComboGwarancja().setVisible(model.isWarrantyVisible());
            view.getLblGwarancja().setVisible(model.isWarrantyVisible());
        }
    }

    public enum Result {

        OK, CANCEL;
    }

}

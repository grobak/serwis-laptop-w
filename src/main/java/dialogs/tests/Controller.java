package dialogs.tests;

import interfaces.BackgroundTask;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import models.Computer;
import models.RepairStatus;
import repositories.ComputerRepository;

/**
 *
 * @author Grzesiek
 */
class Controller implements BackgroundTask {

    private final View view;
    private Computer computer;

    public Controller(View view) {
        this.view = view;
    }

    public void init(Computer computer) {
        this.computer = computer;
        view.setTitle("Testy " + computer.getRMA());
        view.setCachedTests(new ComputerRepository().getTests(computer.getId()));
        view.setTests(new ComputerRepository().getTests(computer.getId()));       

        view.getOkButton().addActionListener(e -> {
            if (hasTestBeenModified()) {
                backgroundTask(
                        this::saveTests,
                        view::closeDialog);
            } else {
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, "Nie dokonano żadnych zmian", "Błąd", JOptionPane.ERROR_MESSAGE);
            }
        });

        view.getCancelButton().addActionListener(e -> view.closeDialog());
    }

    private boolean hasTestBeenModified() {
        return !view.getTests().equals(view.getCachedTests());
    }

    private void saveTests() {
        ComputerRepository computers = new ComputerRepository();
        computers.updateTests(view.getTests(), computer.getId());
        computers.setStatus(computer, RepairStatus.PODCZAS_TESTOW);
    }
}

package dialogs.tests;

import components.TestComponent;
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Grzesiek
 */
class View extends JDialog {

    private final JButton cancelButton;
    private final JButton okButton;
    private TestComponent tests;
    private TestComponent cachedTests;

    @SuppressWarnings("PMD.VariableNamingConventions")
    View() {
        this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setBounds(100, 100, 493, 378);
        setPreferredSize(new Dimension(500, 400));
        setMinimumSize(getPreferredSize());
        getContentPane().setLayout(new BorderLayout());
        JPanel contentPanel = new JPanel();
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0};
        gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);

        GridBagConstraints gbc_komponentTesty = new GridBagConstraints();
        gbc_komponentTesty.fill = GridBagConstraints.BOTH;
        gbc_komponentTesty.gridx = 0;
        gbc_komponentTesty.gridy = 0;
        contentPanel.add(tests, gbc_komponentTesty);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        this.okButton = new JButton("Zapisz");
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        this.cancelButton = new JButton("Anuluj");
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
    }

    JButton getCancelButton() {
        return cancelButton;
    }

    JButton getOkButton() {
        return okButton;
    }

    public TestComponent getTests() {
        return tests;
    }

    public TestComponent getCachedTests() {
        return cachedTests;
    }

    void setTests(TestComponent tests) {
        this.tests = tests;
    }

    void setCachedTests(TestComponent cachedTests) {
        this.cachedTests = cachedTests;
    }

    public void closeDialog() {
        setVisible(false);
        dispose();
    }

}

package dialogs;

import components.SearchPanel;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import models.Customer;
import repositories.CustomerRepository;

public class DialogWyszukajKlienta extends JDialog {

    private Customer klient;
    private List<Customer> klienci;
    private JButton btnWybierz;
    private JButton btnAnuluj;
    private SearchPanel searchPanel;

    public DialogWyszukajKlienta() {
        setLayout();

        addWindowFocusListener(new WindowAdapter() {
            public void windowGainedFocus(WindowEvent e) {
                getRootPane().setDefaultButton(searchPanel.getBtnWyszukaj());
            }
        });

        klienci = new CustomerRepository().findByNameOrCity("");
        searchPanel.getSearchResultTable().setModel(stworzModelTabeli());

        btnWybierz.addActionListener(e -> {
            if (searchPanel.getSearchResultTable().getSelectedRow() >= 0) {
                int row = searchPanel.getSearchResultTable().getSelectedRow();
                klient = klienci.get(klienci.size() - row - 1);
                klient = new CustomerRepository().getById(klient.getId());
                closeDialog();
            }
        });

        btnAnuluj.addActionListener(e -> {
            setVisible(false);
            dispose();
        });

        searchPanel.getBtnWyszukaj().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    searchPanel.getBtnWyszukaj().doClick();
                }
            }
        });

        searchPanel.getBtnWyszukaj().addActionListener(e -> searchPanel.getSearchResultTable().setModel(stworzModelTabeli()));

        searchPanel.getSearchResultTable().addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent klikniecie) {
                JTable table = (JTable) klikniecie.getSource();
                Point p = klikniecie.getPoint();
                int row = table.rowAtPoint(p);
                if (klikniecie.getClickCount() == 2) {
                    klient = klienci.get(klienci.size() - row - 1);
                    klient = new CustomerRepository().getById(klient.getId());
                    closeDialog();
                }
            }
        });
        getRootPane().setDefaultButton(btnWybierz);
    }

    @SuppressWarnings("PMD.VariableNamingConventions")
    private void setLayout() {
        this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        setBounds(100, 100, 450, 300);
        setTitle("Wyszukaj klienta");

        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        Object[] nazwyKolumn = {"Dane klienta", "Telefon", "Miejscowość"};
        model.setColumnIdentifiers(nazwyKolumn);

        JPanel panelPrzyciskow = new JPanel();
        panelPrzyciskow.setLayout(new FlowLayout(FlowLayout.RIGHT));

        btnWybierz = new JButton("Wybierz");
        btnWybierz.setActionCommand("OK");
        panelPrzyciskow.add(btnWybierz);

        btnAnuluj = new JButton("Anuluj");
        btnAnuluj.setActionCommand("Cancel");
        panelPrzyciskow.add(btnAnuluj);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        getContentPane().setLayout(gridBagLayout);

        searchPanel = new SearchPanel();
        GridBagConstraints gbc_searchPanel = new GridBagConstraints();
        gbc_searchPanel.gridwidth = 2;
        gbc_searchPanel.insets = new Insets(0, 0, 5, 0);
        gbc_searchPanel.fill = GridBagConstraints.BOTH;
        gbc_searchPanel.gridx = 0;
        gbc_searchPanel.gridy = 0;
        getContentPane().add(searchPanel, gbc_searchPanel);
        searchPanel.getTxtWyszukaj().requestFocus();

        GridBagConstraints gbc_panelPrzyciskow = new GridBagConstraints();
        gbc_panelPrzyciskow.fill = GridBagConstraints.BOTH;
        gbc_panelPrzyciskow.gridx = 1;
        gbc_panelPrzyciskow.gridy = 1;
        getContentPane().add(panelPrzyciskow, gbc_panelPrzyciskow);
    }

    public Customer closeDialog() {
        setVisible(false);
        dispose();
        return klient;
    }

    private DefaultTableModel stworzModelTabeli() {
        klienci = new CustomerRepository().findByNameOrCity(searchPanel.getTxtWyszukaj().getText());
        DefaultTableModel modelTabeli = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        Object[] nazwyKolumn = {"Dane klienta", "Telefon", "Miejscowość"};

        modelTabeli.setColumnIdentifiers(nazwyKolumn);
        for (Customer aKlient : klienci) {
            modelTabeli.insertRow(0, new Object[]{aKlient.getName(), aKlient.getAddress().getPhone(), aKlient.getAddress().getCity()});
        }
        return modelTabeli;
    }

}

package controllers;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import models.Computer;
import models.Customer;
import repositories.ComputerRepository;
import repositories.CustomerRepository;

public class PlaceholderReplacerTest {
	
	private Computer sprzet =  new ComputerRepository().getComputerByID(22);
	private Customer klient = new CustomerRepository().getById(sprzet.getIdKlienta());
	
	@Test
	public void testPlaceholderReplacer() {
		PlaceholderReplacer replacer = new PlaceholderReplacer(sprzet, klient);
		String replacedLine;
		replacedLine = replacer.replace("<p id=\"nazwa_klienta\"><strong>xklientx</strong></p>");
		assertTrue(replacedLine.equals("<p id=\"nazwa_klienta\"><strong>Firma Testowa Grzegorz Robak</strong></p>"));
	}


}

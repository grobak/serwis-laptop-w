package controllers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import view.forms.PracownikForm;

public class PracownikFormValidatorTest
{
	
	private PracownikFormValidator valid;

	@Before
	public void setUp() {
		valid = new PracownikFormValidator();
	}

	@After
	public void tearDown() {
		valid = null;
	}

	@Test
	public void testIsLoginValid(){
		assertFalse("Login jest pusty", valid.isLoginValid(""));
		assertTrue("OK", valid.isLoginValid("a"));
	}
	
	@Test
	public void testIsNameValid(){
		assertFalse("Nazwa jest pusta", valid.isNameValid(""));
		assertTrue("OK", valid.isNameValid("a"));
	}
	
	@Test
	public void testIsPasswordValid(){
		assertFalse("Hasło jest puste", valid.isPasswordValid(""));
		assertTrue("OK", valid.isPasswordValid("a"));
	}
	
	@Test
	public void testIsPasswordConfirmValid(){
		assertFalse("Hasło confirm jest puste", valid.isPasswordConfirmValid(""));
		assertTrue("OK", valid.isPasswordConfirmValid("a"));
	}
	
	@Test
	public void testDoPasswordsMatch(){
		PracownikForm form = new PracownikForm();
		form.setPassword("a");
		form.setPasswordConfirm("");
		assertFalse("Hasła są różne", valid.doPasswordsMatch(form));
		form.setPassword("");
		form.setPasswordConfirm("a");
		assertFalse("Hasła są różne", valid.doPasswordsMatch(form));
		form.setPassword("a");
		assertTrue("Hasła są takie same", valid.doPasswordsMatch(form));
	}
	
	@Test
	public void testIsTelefonValid(){
		assertFalse("Numer jest tekstem", valid.isTelefonValid("a"));
		assertTrue("Numer jest pusty", valid.isTelefonValid(""));
		assertTrue("OK", valid.isTelefonValid("1"));
	}
	
	@Test
	public void testIsFormValid(){
		PracownikForm form = new PracownikForm();
		form.setTextLogin("a");
		form.setTextName("a");
		form.setTextPhone("1");
		form.setPassword("a");
		form.setPasswordConfirm("a");
		form.setComboJobPosition("Administrator");
		assertTrue("OK", valid.isFormValid(form));
		
	}

}

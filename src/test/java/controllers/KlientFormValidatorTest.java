package controllers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import view.forms.KlientForm;


public class KlientFormValidatorTest{

	private KlientForm form;
	private KlientFormValidator validator;
	
	@Before
	public void setUp() {
		form = new KlientForm();
		validator = new KlientFormValidator(form);
	}

	@Test
	public void testNameIsAvailable() {
		KlientFormValidator validatorMock = mock(KlientFormValidator.class);
		when(validatorMock.nameIsAvailable()).thenReturn(false);
		form.getTextNazwaKlienta().setText("Jan Kowalski");
		assertFalse("Podano zajętą nazwę klienta", validatorMock.nameIsAvailable());
		when(validatorMock.nameIsAvailable()).thenReturn(true);
		form.getTextNazwaKlienta().setText("abcdefghijklmnop");
		assertTrue("Podano wolną nazwę klienta", validatorMock.nameIsAvailable());
	}

	@Test
	public void testIsNazwaKlientaValid() {

		form.getTextNazwaKlienta().setText("");
		assertFalse("Pusta nazwa klienta", validator.isNazwaKlientaValid());
		String test = "a";
		for(int i=0;i<101;i++)
			test=test + "a";
		form.getTextNazwaKlienta().setText(test);
		assertFalse("Nazwa klienta za długa (>100)", validator.isNazwaKlientaValid());
		form.getTextNazwaKlienta().setText("aa");
		assertFalse("Nazwa klienta za krótka", validator.isNazwaKlientaValid());
		form.getTextNazwaKlienta().setText("Kowalski");
		assertTrue("Nazwa klienta ok", validator.isNazwaKlientaValid());
	}

	@Test
	public void testIsTelefonValid() {
		form.getTextTelefon().setText("");
		assertFalse("Pole telefon jest puste", validator.isTelefonValid());
		form.getTextTelefon().setText("a");
		assertFalse("Telefon to nie liczba", validator.isTelefonValid());
		form.getTextTelefon().setText("12");
		assertTrue("Telefon jest liczbą", validator.isTelefonValid());
		form.getTextTelefon().setText("123456");
		assertTrue("Telefon jest poprawny", validator.isTelefonValid());
	}

	@Test
	public void testIsEmailValid() {
		form.getTextEmail().setText("");
		assertTrue("Email jest pusty", validator.isEmailValid());
		form.getTextEmail().setText("aaaa");
		assertFalse("Email za krótki", validator.isEmailValid());
		form.getTextEmail().setText("aaaa@");
		assertFalse("Email za krótki", validator.isEmailValid());
		form.getTextEmail().setText("aaa@aaa");
		assertFalse("Email za krótki", validator.isEmailValid());
		form.getTextEmail().setText("aaa@aaa.aa");
		assertTrue("Email ok", validator.isEmailValid());
	}

	@Test
	public void testIsNIPValid() {
		form.getTextNIP().setText("");
		assertTrue("Pusty NIP", validator.isNIPValid());
		form.getTextNIP().setText("123");
		assertFalse("NIP za krótki", validator.isNIPValid());
		form.getTextNIP().setText("a");
		assertFalse("NIP nie jest liczbą", validator.isNIPValid());
		form.getTextNIP().setText("12345678901");
		assertFalse("NIP za długi", validator.isNIPValid());
		form.getTextNIP().setText("1234567890");
		assertTrue("NIP długość ok", validator.isNIPValid());
            form.getTextNIP().setText("123-456-78-90");
		assertTrue("NIP z kreskami ok", validator.isNIPValid());
		form.getTextNIP().setText("123-456-78");
		assertFalse("NIP z kreskami za krótki", validator.isNIPValid());
	}

	@Test
	public void testIsNumber() {
		assertFalse("Podano litery", validator.isNumber("aaaa"));
		assertFalse("Podano nic", validator.isNumber(""));
		assertTrue("Podano liczby", validator.isNumber("123456"));
		assertFalse("Podano znaki inne niż - i spacja", validator.isNumber("!@#$%^&*()") );
		assertTrue("Podano liczby i dozwolone znaki", validator.isNumber("123-456 789"));
	}

}

package database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;

import org.junit.Before;
import org.junit.Test;

import dataproviders.EncryptedPassword;
import dataproviders.PasswordGenerator;

public class PasswordGeneratorTest {
	
	private PasswordGenerator gen;
	
	@Before
	public void setUp(){
		gen = new PasswordGenerator();
	}

	@Test
	public void testPasswordGenerator() {
		try {
			EncryptedPassword pass = gen.encrypt("admin", "[B@5197848c");
			assertEquals("is generated password good?", "4ef3824f2fa4f45f5262cf7fc8e18e7f609b82eae4a1376daf33485d577e061b", pass.getHash() );
			assertTrue("is getSalt() generating strings", pass.getSalt() != null );
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}

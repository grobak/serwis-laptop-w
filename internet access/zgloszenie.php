<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Informacja nt. napraw - PC Center</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
   <?php

    $data=date("Y-m-d");
    $ip=$_SERVER['REMOTE_ADDR'];
    $czas=date("G:i:s");

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $imie=$_POST['imie']; 
        $ulica=$_POST['ulica'];
        $kod=$_POST['kod']; 
        $miasto=$_POST['miasto']; 
        $telefon=$_POST['telefon'];
        $email=$_POST['email']; 
        $producent=$_POST['producent'];
        $model=$_POST['model'];
        $seryjny=$_POST['seryjny'];
        $problem=wordwrap($_POST['problem'], 20, "\n", true);
        $torba=$_POST['torba'];
        $zasilacz=$_POST['zasilacz'];
        $przewod=$_POST['przewod'];
        $dysk=$_POST['dysk'];
        $regulamin=$_POST['regulamin']; 
        $blad=false; 
        $nazwa_firmy=$_POST['nazwa_firmy'];
        $nip=$_POST['nip'];
    }
    ?>

    <div class="srodtytul">
       <p>Podane dane zostaną użyte do odesłania sprzętu po naprawie.</p>
    </div>

    <form action="insert.php" method="POST" class="form" name="form">
        <p>Pola oznaczone * są wymagane.</p>
        <div class="form_inputs" id="dane_klienta">
            <table>
                <tr>
                    <td>Nazwa firmy: </td>
                    <td>
                        <input type="text" name="nazwa_firmy" value="" size="80" maxlength="80">
                     </td>
                </tr>
                <tr>
                    <td>NIP: </td>
                    <td>
                        <input type="text" name="nip" value="" size="80" maxlength="80">
                    </td>
                </tr>

                <tr>
                    <td id="imie">Imię i nazwisko: </td>
                    <td>
                        <input type="text" name="imie" value="" size="80" maxlength="80" required>*
                    </td>
                </tr>

                <tr>
                    <td id="ulica">Ulica i nr domu: </td>
                    <td>
                        <input type="text" name="ulica" value="" size="80"  maxlength="80" required>*
                    </td>
                </tr>

                <tr>
                    <td id="kod">Kod pocztowy: </td>
                    <td>
                        <input type="text" name="kod" value="" size="10"  maxlength="10" required>*
                    </td>
                </tr>

                <tr>
                    <td id="miasto">Miejscowość: </td>
                    <td>
                        <input type="text" name="miasto" value="" size="30"  maxlength="30" required>*
                    </td>
                </tr>

                <tr>
                    <td id="telefon">Telefon: </td>
                    <td>
                        <input type="tel" name="telefon" value="" size="30"  maxlength="30" required>*
                    </td>
                </tr>

                <tr>
                    <td id="email">Email: </td>
                    <td>
                        <input type="email" name="email" value="" size="30"  maxlength="30" required>*
                    </td>
                </tr>
            </table>
        </div>

        <div class="srodtytul">
            <p>Informacje o wysyłanym sprzęcie</p>
        </div>

        <div class="form_inputs" id="dane_laptopa">
            <table>
                <tr>
                    <td id="producent">Producent: </td>
                    <td>
                        <input type="text" name="producent" value="" size="80" maxlength="80" required>*
                    </td>
                </tr>
                <tr>
                    <td id="model">Model: </td>
                    <td>
                        <input type="text" name="model" value="" size="40"  maxlength="40" required>*
                    </td>
                </tr>
                <tr>
                    <td>Dodatkowe <br/>wyposażenie:</td>
                    <td>
                        <input type="hidden" name="torba" value="0" />
                        <label>
                            <input type="checkbox" name="torba" value="1">Torba
                        </label>
                        <input type="hidden" name="zasilacz" value="0" />
                        <label>
                            <input name="zasilacz" type="checkbox" value="1">Zasilacz
                        </label>
                        <input type="hidden" name="przewod" value="0" />
                        <label>
                            <input name="przewod" type="checkbox" value="1">Przewód do zasilacza
                        </label>
                        <input type="hidden" name="dysk" value="0" />
                        <label>
                            <input name="dysk" type="checkbox" value="1">Dysk twardy   
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>Uwagi: </td>
                    <td>
                        <input type="text" name="uwagi" value="" size="80"  maxlength="80" />
                    </td>
                </tr>
                <tr>
                    <td id="problem">Opis usterki: </td>
                    <td>
                        <textarea required name="problem" cols="50" rows="5"></textarea>*
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="form_center">
                        <input type="hidden" name="regulamin" value="0" />
                        <label id="regulamin">
                            <input id="reg" name="regulamin" type="checkbox" value="1" >* 
                            Oświadczam, że zapoznałem się z <a href="regulamin.html" target="_blank"><u>regulaminem serwisu</u></a> i akceptuję jego warunki
                        </label>
                    </td>
                </tr>
                <tr id="blad"><td colspan="2"><strong>Formularz zawiera błędy lub niewypełnione pola.</strong></td></tr>
                <tr>
                    <td colspan="2" class="form_center">
                        <input type="button" value="Anuluj" onClick="parent.location='index.php'">
                        <input type="submit" name="wyslij" value="Wyślij zgłoszenie" onclick="return validateForm()">
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
<script>
function validateForm() {
    var result = true;
    
    validate("imie");
    validate("ulica");
    validate("kod");
    validate("miasto");
    validate("telefon");

    var x = document.forms["form"]["email"].value;
    validate(x, "email");
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        document.getElementById('email').style.color = 'red';
        result = false;
    }
    else
        document.getElementById('email').style.color = 'black';

    validate("producent");
    validate("model");
    validate("problem");

    var x = document.getElementById("reg").checked;
    if (x === false) {
        document.getElementById('regulamin').style.color = 'red';
        result = false;
    }
    else
        document.getElementById('regulamin').style.color = 'black';

    if(result==false)
        document.getElementById('blad').style.visibility = 'visible';

    return result;
}

function validate(id){
    var x = document.forms["form"][id].value;
    if (x == null || x == "") {
        document.getElementById(id).style.color = 'red';
        result = false;
    }
    else
        document.getElementById(id).style.color = 'black';
}
</script>
</html>
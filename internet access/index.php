<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Informacja nt. napraw - PC Center</title>
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript">
		function drukuj(){
			if (!window.print){
 				alert("Twoja przeglądarka nie obsługuje drukowania!")
 				return 0;
 			}
 			window.print(); // jeśli wszystko ok &#8211; drukuj
		}
	</script>
</head>
<body>
	<p>Witamy!</p>
	<p>Oferujemy możliwość zgłoszenia naprawy poprzez forumlarz internetowy lub sprawdzenie statusu naprawy.</p>
	<p>Proszę wybrać jedną z opcji:</p>
	<table>
		<tr>
			<td>
				<a href="zgloszenie.php">Zgłoszenie naprawy</a>
			</td>
			<td>
				<a href="status.php">Status naprawy</a>
			</td>
		</tr>
	</table>
</body>
</html>
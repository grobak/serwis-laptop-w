<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Informacja nt. napraw - PC Center</title>
    <link href="insert.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <?php
        $imie=$ulica=$kod=$miasto=$telefon=$email=$producent=$model="";
        $uwagi=$problem=$torba=$zasilacz=$przewod=$dysk=$regulamin="";
        $rma=$nazwa_firmy=$nip="";

        $data=date("Y-m-d");
        $ip=$_SERVER['REMOTE_ADDR'];
        $czas=date("G:i:s");

        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $imie=$_POST['imie']; 
            $ulica=$_POST['ulica'];
            $kod=$_POST['kod']; 
            $miasto=$_POST['miasto']; 
            $telefon=$_POST['telefon'];
            $email=$_POST['email']; 
            $producent=$_POST['producent'];
            $model=$_POST['model'];
            $uwagi=$_POST['uwagi'];
            $problem=wordwrap($_POST['problem'], 20, "\n", true);
            $torba=$_POST['torba'];
            $zasilacz=$_POST['zasilacz'];
            $przewod=$_POST['przewod'];
            $dysk=$_POST['dysk'];
            $regulamin=$_POST['regulamin']; 
            $nazwa_firmy=$_POST['nazwa_firmy'];
            $nip=$_POST['nip'];
            $firma = 1;
            $nazwa_klienta = $nazwa_firmy." ".$imie;

            if($nazwa_firmy==""){
                $nazwa_klienta=$imie;
                $firma=0;
            }
        }

        $con = @mysqli_connect('localhost', 'root', '', 'pccenter_robak')
                or die('Błąd: Brak połączenia z serwerem mysql.');

        mysqli_query($con, "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

        $result = mysqli_query($con, "SELECT mid(rma, 2, 6) as rma_short FROM `laptopy` WHERE rma like 'r%' having length(rma_short)=5 order by rma desc limit 1");
   
    
        while($row = mysqli_fetch_array($result))
        $rma=$row['rma_short'];
    
        $rma= $rma +1;

        if ($rma <=99) $rma = "R00" . $rma;
        if ($rma >=100) $rma = "R0" . $rma;
        if ($rma >=1000) $rma = "R" . $rma;
        
        $result = mysqli_query($con, "INSERT INTO klienci (`Firma`, `Nazwa_klienta`, `NIP`, `Dodany_przez`) VALUES ($firma, '$nazwa_klienta', '$nip', 'Formularz')");
        $id_klienta = mysqli_insert_id($con);
        $result = mysqli_query($con, "INSERT INTO klientadres (`Id_klienta`, `Miejscowosc`, `Ulica`, `Kod_pocztowy`, `Telefon`, `Email`) VALUES ($id_klienta, '$miasto', '$ulica', '$kod', '$telefon', '$email')");
        $result = mysqli_query($con, "INSERT INTO wyposazenie (`HDD`, `DVD`, `Bateria`, `Zasilacz`, `Kabel_ac`, `Torba`, `Karton`, `Wyp_dodatkowe`, `Brak`) VALUES ('$dysk', '0', '0', '$zasilacz', '$przewod', '$torba', '0', '', '')");
        $id_wyposazenie = mysqli_insert_id($con);
        $result = mysqli_query($con, "INSERT INTO laptopy (`Id_klienta`, `model`, `Data_przyjecia`, `RMA`, `Uwagi`, `Opis_usterki`, `Id_wyposazenie`, `Rodzaj_naprawy`, Opis_naprawy) VALUES ($id_klienta, '$model', now(), '$rma', '$uwagi', '$problem', $id_wyposazenie, 'Naprawa', '')");
        $id_laptopa = mysqli_insert_id($con);
        $result = mysqli_query($con, "INSERT INTO naprawy (`Id_laptopa`, `Kto`, `Id_status`) VALUES ($id_laptopa, 'Formularz', 1)");

        mysqli_close($con);
    ?>

    <div class="wazne">
        <p>Prosimy o dołączenie tej częsci zgłoszenia do wysyłanego sprzętu.</p>
        <p>Pozwoli to na sprawne przeprowadzenie naprawy i odesłanie do Państwa naprawionego sprzetu.</p>
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" class="wydruk_tabela">
            <tr>
                <td>Numer naprawy RMA: </td>
                <td><?php echo $rma; ?></td>
            </tr>
            <?php
            if($nazwa_firmy!=''){
                echo "<tr>";
                echo    "<td>Nazwa firmy:  </td>";
                echo    "<td> ".$nazwa_firmy." </td>";
                echo "</tr>";
            }
            if($nip!=''){
                echo "<tr>";
                echo    "<td>NIP:  </td>";
                echo    "<td> ".$nip." </td>";
                echo "</tr>";
            }
            ?>
            <tr>
                <td>Imie i nazwisko:  </td>
                <td><?php echo $imie; ?></td>
            </tr>
            <tr>
                <td>Ulica:  </td>
                <td><?php echo $ulica; ?></td>
            </tr>
            <tr>
                <td>Kod:  </td>
                <td><?php echo $kod; ?></td>
            </tr>
            <tr>
                <td>Miasto:  </td>
                <td><?php echo $miasto; ?></td>
            </tr>
            <tr>
                <td>Telefon:  </td>
                <td><?php echo $telefon; ?></td>
            </tr>
            <tr>
                <td>email:  </td>
                <td><?php echo $email; ?></td>
            </tr>
        </table>
    </div>

    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="600px"  class="wydruk_tabela">
            <tr>
                <td>Producent:</td>
                <td><?php echo $producent; ?></td>
            </tr>
            <tr>
                <td>Model:</td>
                <td><?php echo $model; ?></td>
            </tr>
            <tr>
                <td>Dodatkowe wyposażenie:</td>
                <td>
                    <?php if($torba== '1') echo '<input type="checkbox" checked />Torba |'; ?>
                    <?php if($zasilacz== '1') echo '<input type="checkbox" checked />Zasilacz |'; ?>
                    <?php if($przewod== '1') echo '<input type="checkbox" checked />Kabel do zasilacza |'; ?>
                    <?php if($dysk== '1') echo '<input type="checkbox" checked />Dysk Twardy'; ?>   
                </td>
            </tr>
            <tr>
                <td>Uwagi:</td>
                <td><?php echo $uwagi; ?></td>
            </tr>
            <tr>
                <td>Opis usterki:</td>
                <td><?php echo $problem ?></td>
            </tr>
        </table>
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" class="wydruk_tabela">
            <tr>
                <td>Formularz wysłany z adresu: <?php echo $ip . "<br>"; ?></td>
            </tr>
            <tr>
                <td>Data wypełnienia: <?php echo $data . "<br>"; ?></td>
            </tr>
            <tr>
                <td>Godzina wypełnienia: <?php echo $czas; ?></td>
            </tr>
        </table>
    </div>
    <div id="nr_RMA">
        Państwa numer zgłoszenia naprawy (RMA) to: <b><?php echo $rma; ?></b>
        <br />Prosimy o zatrzymanie tej częsci zgłoszenia.<br />
        W przypadku kontaktu z naszą firmą prosimy o powoływanie się na powyższy numer RMA.<br /><br />
        <b>Prosimy pamiętać o prawidłowym i bezpiecznym opakowaniu przesyłki.</b>
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" class="wydruk_tabela">
            <tr>
                <td>Numer naprawy RMA: </td>
                <td><?php echo $rma; ?></td>
            </tr>
            <tr>
                <td>Imię i nazwisko:</td>
                <td><?php echo $imie; ?></td>
            </tr>
        </table>
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="600px"  class="wydruk_tabela">
            <tr>
                <td>Producent:</td>
                <td><?php echo $producent; ?></td>
            </tr>
            <tr>
                <td>Model:</td>
                <td><?php echo $model; ?></td>
            </tr>
            <tr>
                <td>Dodatkowe wyposażenie:</td>
                <td>
                    <?php if($torba== '1') echo '<input type="checkbox" checked />Torba |'; ?>
                    <?php if($zasilacz== '1') echo '<input type="checkbox" checked />Zasilacz |'; ?>
                    <?php if($przewod== '1') echo '<input type="checkbox" checked />Kabel do zasilacza |'; ?>
                    <?php if($dysk== '1') echo '<input type="checkbox" checked />Dysk Twardy'; ?>    
                </td>
            </tr>
            <tr>
                <td>Uwagi:</td>
                <td><?php echo $uwagi; ?></td>
            </tr>
            <tr>
                <td>Opis usterki:</td>
                <td><?php echo $problem ?></td>
            </tr>
        </table>
    </div>
    <div id="footer">
        <p>
            <form id="back" action="index.php" method="POST">
                <input type="submit" value="Wróć">
                <input name="Drukuj" type="button" onClick="drukuj()" value="Drukuj zgłoszenie"/>
            </form>
        </p>
    </div>
</body>
<script>
    function drukuj(){
        if (!window.print){
            alert("Twoja przeglądarka nie obsługuje drukowania!")
            return 0;
        }
        window.print();
    }
</script>
</html>